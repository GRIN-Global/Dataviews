SELECT
  g.geography_id AS value_member,
  LTRIM(RTRIM(COALESCE(cvl.title, g.country_code) || 
               CASE COALESCE( g.adm1, '') WHEN '' THEN '' ELSE ', ' || g.adm1 END ||
               CASE COALESCE( g.adm2, '') WHEN '' THEN '' ELSE ', ' || g.adm2 END)) AS display_member
FROM
    geography g
    LEFT JOIN code_value cv
      ON  g.country_code = cv.value 
          AND  cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' 
    LEFT JOIN code_value_lang cvl
      ON  cv.code_value_id = cvl.code_value_id 
          AND  cvl.sys_lang_id = __LANGUAGEID__ 

WHERE
  ((g.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (g.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (g.geography_id IN (:valuemember))
    OR (g.geography_id BETWEEN :startpkey AND :stoppkey) 
    OR (cv.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (cv.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (cvl.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (cvl.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
  )
