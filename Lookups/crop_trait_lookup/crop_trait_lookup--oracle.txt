SELECT
  ct.crop_trait_id AS value_member
  ,COALESCE(ctl.title, ct.coded_name) AS display_member
  ,ct.crop_id AS crop_id
FROM
  crop_trait ct 
  LEFT JOIN crop_trait_lang ctl ON ct.crop_trait_id = ctl.crop_trait_id AND ctl.sys_lang_id = __LANGUAGEID__
WHERE
  ((ct.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (ct.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (ct.crop_trait_id IN (:valuemember))
    OR (ct.crop_trait_id BETWEEN :startpkey AND :stoppkey)
    OR (ctl.created_date > COALESCE(:createddate, to_date('1753-01-01','YYYY-MM-DD')))
    OR (ctl.modified_date > COALESCE(:modifieddate, to_date('1753-01-01','YYYY-MM-DD')))
  )
