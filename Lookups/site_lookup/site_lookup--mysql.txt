SELECT
  s.site_id as value_member,
  s.site_short_name AS display_member
FROM
  site AS s

WHERE
  ((s.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (s.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (s.site_short_name IN (:valuemember))
   OR (s.site_id BETWEEN :startpkey AND :stoppkey))

ORDER by s.site_short_name