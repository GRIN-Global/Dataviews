SELECT
  DISTINCT wc.web_cooperator_id as value_member,
  LTRIM(RTRIM(nvl(wc.last_name, '') || ', ' || nvl(wc.first_name, '') || ', ' || nvl(wc.organization, ''))) as display_member,
  NVL(wc.is_active, 'N') AS is_active
FROM
  web_cooperator wc
WHERE
  ((to_char(wc.created_date,'YYYY-MM-DD') > NVL(to_char(:createddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (to_char(wc.modified_date,'YYYY-MM-DD') > NVL(to_char(:modifieddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (wc.web_cooperator_id IN (:valuemember))
   OR (wc.web_cooperator_id BETWEEN :startpkey AND :stoppkey)
  )
