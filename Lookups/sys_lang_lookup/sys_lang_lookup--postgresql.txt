SELECT
  sl.sys_lang_id AS value_member
  ,sl.title AS display_member
FROM
  sys_lang sl
WHERE
  ((sl.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (sl.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (sl.sys_lang_id IN (:valuemember))
   OR (sl.sys_lang_id BETWEEN :startpkey AND :stoppkey)
/*   OR (sl.name like :displaymember) */ )