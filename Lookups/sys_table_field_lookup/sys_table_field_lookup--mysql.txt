SELECT
  stf.sys_table_field_id AS value_member,
  stf.field_name AS display_member,
  stf.sys_table_id
FROM
    sys_table_field stf
WHERE
  ((stf.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (stf.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (stf.sys_table_field_id IN (:valuemember))
   OR (stf.sys_table_field_id BETWEEN :startpkey AND :stoppkey))
