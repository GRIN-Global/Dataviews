SELECT
  st.sys_table_id AS value_member,
  st.table_name AS display_member
FROM
    sys_table st
WHERE
  ((st.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (st.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (st.sys_table_id IN (:valuemember))
   OR (st.sys_table_id BETWEEN :startpkey AND :stoppkey))
ORDER by st.table_name
