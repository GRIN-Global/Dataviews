SELECT
  ga.genetic_annotation_id AS value_member,
  gm.name + ', ' + m.name AS display_member,
  gm.crop_id
FROM
  genetic_annotation ga
  LEFT JOIN genetic_marker gm ON ga.genetic_marker_id = gm.genetic_marker_id
  LEFT JOIN method m ON ga.method_id = m.method_id
WHERE
  ((ga.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (ga.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (ga.genetic_annotation_id IN (:valuemember))
    OR (ga.genetic_annotation_id BETWEEN :startpkey AND :stoppkey)
    OR (gm.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (gm.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (m.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (m.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  )