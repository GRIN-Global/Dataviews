SELECT
  r.region_id AS value_member,
  CONCAT(r.continent,
    CASE WHEN r.subcontinent IS NULL THEN '' ELSE CONCAT(', ', r.subcontinent) END
    ) AS display_member
FROM
  region r
WHERE
  ((to_char(r.created_date,'YYYY-MM-DD') > NVL(to_char(:createddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (to_char(r.modified_date,'YYYY-MM-DD') > NVL(to_char(:modifieddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (r.region_id IN (:valuemember))
   OR (r.region_id BETWEEN :startpkey AND :stoppkey))
ORDER BY 2
