SELECT
  crop.crop_id AS value_member
  ,crop.name AS display_member
FROM
  crop
WHERE
  ((crop.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (crop.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (crop.crop_id IN (:valuemember))
   OR (crop.crop_id BETWEEN :startpkey AND :stoppkey)
/*   OR (crop.name like :displaymember) */ )
