SELECT
  ng.name_group_id AS value_member
  ,ng.group_name AS display_member
FROM
  name_group ng
WHERE
  ((ng.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (ng.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (ng.name_group_id IN (:valuemember))
   OR (ng.name_group_id BETWEEN :startpkey AND :stoppkey)
/*   OR (ng.group_name like :displaymember) */ )