SELECT
  g.geography_id AS value_member,
  COALESCE(g.adm1 || ', ', '') || COALESCE(cvl.title, g.country_code) AS display_member
FROM
  geography g
  LEFT JOIN code_value cv ON g.country_code = cv.value AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
  LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id AND cvl.sys_lang_id = __LANGUAGEID__
WHERE
  g.adm2 IS NULL AND g.adm3 IS NULL AND g.adm4 IS NULL AND
  ((g.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (g.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (g.geography_id IN (:valuemember))
    OR (g.geography_id BETWEEN :startpkey AND :stoppkey)
    OR (cv.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (cv.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (cvl.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (cvl.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  )