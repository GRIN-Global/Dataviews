SELECT
  a.accession_id AS value_member
  ,LTRIM(RTRIM(LTRIM(COALESCE(a.accession_number_part1, '') + ' ') + LTRIM(COALESCE(convert(varchar, a.accession_number_part2), '') + ' ') + COALESCE(a.accession_number_part3, ''))) AS display_member
  ,a.accession_number_part1 AS accession_prefix
  ,a.accession_number_part2 AS accession_number
  ,a.accession_number_part3 AS accession_suffix
FROM
  accession a
WHERE
  ((a.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (a.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (a.accession_id IN (:valuemember))
   OR (a.accession_id BETWEEN :startpkey AND :stoppkey)
  )