SELECT
  l.literature_id AS value_member,
  l.standard_abbreviation AS display_member
FROM
    literature AS l
WHERE
  ((l.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (l.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR l.literature_id IN (:valuemember))
   OR l.literature_id BETWEEN :startpkey AND :stoppkey
