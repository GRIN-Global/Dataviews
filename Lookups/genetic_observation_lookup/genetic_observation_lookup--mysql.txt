SELECT
  gob.genetic_observation_id AS value_member,
  LTRIM(RTRIM(COALESCE(i.inventory_number_part1, '') || ' ' || COALESCE(CONVERT(i.inventory_number_part2,CHAR),'') || ' ' || COALESCE(i.inventory_number_part3,''))) || ' : '  || CONVERT(gob.mean_value,CHAR) AS display_member
  ,gob.genetic_annotation_id
FROM
  genetic_observation gob 
  INNER JOIN inventory i ON gob.inventory_id = i.inventory_id
WHERE
  ((gob.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (gob.modified_date > COALESCE(:modifieddate, '1753-01-01'))
    OR (gob.genetic_observation_id IN (:valuemember))
    OR (gob.genetic_observation_id BETWEEN :startpkey AND :stoppkey)
    OR (i.created_date > COALESCE(:createddate, '1753-01-01'))
    OR (i.modified_date > COALESCE(:modifieddate, '1753-01-01'))
  ) 