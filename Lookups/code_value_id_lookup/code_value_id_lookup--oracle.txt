SELECT
  cv.code_value_id value_member,
  LTRIM(RTRIM(nvl(cv.group_name, '') || '.' || nvl(cv.value, ''))) display_member
FROM
  code_value cv
WHERE
  ((to_char(cv.created_date,'YYYY-MM-DD') > nvl(to_char(:createddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (to_char(cv.modified_date,'YYYY-MM-DD') > nvl(to_char(:modifieddate,'YYYY-MM-DD'), '1753-01-01'))
   OR (cv.value IN (:valuemember))
   OR (cv.code_value_id BETWEEN :startpkey AND :stoppkey)
  )