SELECT 
       LTRIM(RTRIM(COALESCE(c.last_name, '') + ', ' + 
        COALESCE(c.first_name, '') + ', ' + COALESCE(c.organization, ''))) AS cooperator,
       c.cooperator_id,s.site_short_name,asr.source_type_code, YEAR(asr.source_date) as srcdate,
       count(*) as acc_count,
concat('<nobr><a onclick="javascript:return true;" href="../cooperator.aspx?id=', 
         convert(nvarchar, c.cooperator_id), '">', 'link', 
         '</a></nobr>') as cooperator_number
FROM cooperator c 
	   Left JOIN accession_source_map asm
	    on asm.cooperator_id = c.cooperator_id
	   LEFT JOIN accession_source asr
	    on asr.accession_source_id = asm.accession_source_id
	   LEFT JOIN accession a
	    on asr.accession_id = a.accession_id
	   LEFT JOIN cooperator c2
	    on c2.cooperator_id = a.owned_by
	   LEFT JOIN taxonomy_species ts
	    on a.taxonomy_species_id = ts.taxonomy_species_id 
	   LEFT JOIN geography g
	    on g.geography_id = c.geography_id
	   LEFT JOIN site s
	    on c2.site_id = s.site_id
WHERE c.cooperator_id in (select current_cooperator_id from cooperator c3 where c3.last_name like :last_name + '%')
	and asr.source_type_code is not null
GROUP BY  LTRIM(RTRIM(COALESCE(c.last_name, '') + ', ' + 
        COALESCE(c.first_name, '') + ', ' + COALESCE(c.organization, ''))),
        c.cooperator_id,s.site_short_name,
	asr.source_type_code,YEAR(asr.source_date)
ORDER BY LTRIM(RTRIM(COALESCE(c.last_name, '') + ', ' + 
        COALESCE(c.first_name, '') + ', ' + COALESCE(c.organization, ''))),
    	c.cooperator_id,s.site_short_name,
	asr.source_type_code,YEAR(asr.source_date)
       