SELECT accession_number_part1 as prefix,accession_number_part2 as number,accession_number_part3 as suffix,name,
       (SELECT site_short_name from site s,cooperator c where c.site_id=s.site_id and a.owned_by=c.cooperator_id) as site,
       status_code 
from accession a, taxonomy_species t where a.taxonomy_species_id=t.taxonomy_species_id and t.name like upper(substring(:taxon,1,1)) + lower(substring(:taxon,2,len(:taxon)-1)) and not exists
(select * from  accession a2, inventory i, crop cr, crop_trait tr,
              crop_trait_observation o where
                 a2.accession_id = i.accession_id and
                 i.inventory_id = o.inventory_id and
                 o.crop_trait_id = tr.crop_trait_id and
                 tr.crop_id = cr.crop_id and
                 cr.name = upper(:crop) and coded_name = upper(:trait) and a.accession_id=a2.accession_id
)

 