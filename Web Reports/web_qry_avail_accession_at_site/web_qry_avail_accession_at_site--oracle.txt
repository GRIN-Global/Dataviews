select  distinct
	concat(coalesce(a.accession_number_part1,''), ' ', coalesce(cast(a.accession_number_part2) as varchar2(50),''), ' ', coalesce(a.accession_number_part3,'')) as 'Plant ID'
from accession a 
	join inventory i on a.accession_id = i.accession_id 
	join cooperator c on a.owned_by = c.cooperator_id
	join site s on c.site_id = s.site_id
where
	i.is_available = 'Y' 
	and i.is_distributable = 'Y' 
	and i.form_type_code != '**'
	and s.site_short_name != 'NSSB' 
	and s.site_short_name = :site_acronym
	and a.is_web_visible = 'Y'