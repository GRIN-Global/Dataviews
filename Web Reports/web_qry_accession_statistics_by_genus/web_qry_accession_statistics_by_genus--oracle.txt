SELECT
  tg.genus_name as genus, s.site_short_name, COUNT(distinct a.accession_id) AS accession_count
FROM
    accession_source asr,accession a,taxonomy_species ts,taxonomy_genus tg,
    cooperator c,site s
WHERE a.taxonomy_species_id = ts.taxonomy_species_id and ts.taxonomy_genus_id = tg.taxonomy_genus_id
        and a.accession_id = asr.accession_id and a.owned_by = c.cooperator_id and c.site_id = s.site_id
        and is_origin = 'Y' and s.site_short_name like :site_short_name  
  GROUP BY tg.genus_name, s.site_short_name
  ORDER BY tg.genus_name, s.site_short_name
