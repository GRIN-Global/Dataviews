SELECT
  oi.order_request_item_id
  ,oi.order_request_id
  ,oi.sequence_number
  ,Row_number() Over (ORDER BY (select name from taxonomy_species ts WHERE ts.taxonomy_species_id = a.taxonomy_species_id), inventory_number_part1, inventory_number_part2) AS office_use
  ,(select min(plant_name) from accession_inv_name an, inventory i where 
    i.inventory_id = an.inventory_id and i.accession_id = a.accession_id and
    plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2, inventory i2 where an2.inventory_id = i2.inventory_id and i2.accession_id = a.accession_id)) AS plant_name
  ,i.accession_id
  ,i.inventory_number_part1
  ,i.inventory_number_part2
  ,i.inventory_number_part3
  ,o.requestor_cooperator_id
  ,o.final_recipient_cooperator_id
  ,o.ship_to_cooperator_id
  ,imp.distribution_default_quantity
  ,i.inventory_maint_policy_id
  ,oi.inventory_id
  ,(SELECT c.site_id FROM cooperator c WHERE c.cooperator_id = i.owned_by) AS site_id
  ,oi.name
  ,oi.external_taxonomy
  ,(SELECT taxonomy_species_id FROM accession a WHERE i.accession_id = a.accession_id) AS taxonomy_species_id
  ,(SELECT s.geography_id FROM accession_source s WHERE i.accession_id = s.accession_id AND accession_source_id = (SELECT MIN(accession_source_id) FROM accession_source s2 WHERE i.accession_id = s2.accession_id AND is_origin = 'Y')) AS geography_id
  ,i.storage_location_part1
  ,i.storage_location_part2
  ,i.storage_location_part3
  ,i.storage_location_part4
  ,(SELECT COALESCE(i.storage_location_part1, '') + '_' + COALESCE(i.storage_location_part2, '') + '_' + COALESCE(i.storage_location_part3, '') + '_' + COALESCE(i.storage_location_part4, '')) AS storage_location
  ,i.quantity_on_hand
  ,i.quantity_on_hand_unit_code
  ,i.availability_status_code
  ,oi.quantity_shipped
  ,oi.quantity_shipped_unit_code
  ,oi.distribution_form_code
  ,oi.status_code
  ,oi.status_date
  ,oi.note
  ,oi.source_cooperator_id
  ,i.hundred_seed_weight
  ,(SELECT i.hundred_seed_weight * oi.quantity_shipped / 100) AS order_weight
  ,(SELECT TOP 1 percent_viable  FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS percent_viable
  ,(SELECT TOP 1 percent_normal  FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS percent_normal
  ,(SELECT TOP 1 percent_dormant FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS percent_dormant
  ,(SELECT TOP 1 tested_date     FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC) AS tested_date
  ,(SELECT TOP 1 inventory_viability_rule_id FROM inventory_viability iv WHERE iv.inventory_id = i.inventory_id ORDER BY iv.tested_date DESC, inventory_viability_id DESC) AS inventory_viability_rule_id
  ,(SELECT SUBSTRING((SELECT ', ' + ipr1.type_code AS [text()] FROM accession_ipr ipr1 WHERE ipr1.accession_id = i.accession_id AND ipr1.expired_date IS NULL ORDER BY ipr1.accession_id FOR XML PATH ('')), 2, 1000)) AS ipr_restriction
  ,(SELECT 'xPVP' FROM accession_ipr ipr2 WHERE ipr2.accession_id = i.accession_id AND ipr2.type_code = 'PVP' AND ipr2.expired_date IS NOT NULL) AS xpvp_warning
  ,(SELECT SUBSTRING((SELECT ', ' + iq.quarantine_type_code + ' = ' + iq.progress_status_code AS [text()] FROM accession_quarantine iq WHERE iq.accession_id = i.accession_id AND iq.released_date IS NULL ORDER BY iq.accession_id FOR XML PATH ('')), 2, 1000)) AS quarantine_restriction
  ,(SELECT SUBSTRING((SELECT ', ' + tn.noxious_type_code + ' in ' + adm1 + ', ' + cvl.title AS [text()] FROM taxonomy_noxious tn LEFT JOIN accession a ON tn.taxonomy_species_id = a.taxonomy_species_id LEFT JOIN inventory ii ON a.accession_id = ii.accession_id LEFT JOIN geography g ON tn.geography_id = g.geography_id LEFT JOIN code_value cv ON g.country_code = cv.value AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = 1 WHERE ii.inventory_id = i.inventory_id ORDER BY tn.taxonomy_noxious_id FOR XML PATH ('')), 2, 1000)) AS noxious_restriction
  ,(SELECT taxonomy_genus_id FROM taxonomy_species ts LEFT JOIN accession a ON ts.taxonomy_species_id = a.taxonomy_species_id WHERE i.accession_id = a.accession_id) AS taxonomy_genus_id
  ,oi.web_order_request_item_id
  ,(SELECT a.accession_number_part1 FROM accession a WHERE i.accession_id = a.accession_id) AS accession_number_part1
  ,(SELECT a.accession_number_part2 FROM accession a WHERE i.accession_id = a.accession_id) AS accession_number_part2
  ,(SELECT a.accession_number_part3 FROM accession a WHERE i.accession_id = a.accession_id) AS accession_number_part3
FROM
    order_request_item oi
    INNER JOIN inventory i ON  oi.inventory_id = i.inventory_id 
    INNER JOIN order_request o ON oi.order_request_id = o.order_request_id
    INNER JOIN accession a ON i.accession_id = a.accession_id
    INNER JOIN inventory_maint_policy imp ON i.inventory_maint_policy_id = imp.inventory_maint_policy_id
WHERE oi.order_request_id IN (:orderrequestid)
AND oi.quantity_shipped >= 1
ORDER BY oi.sequence_number