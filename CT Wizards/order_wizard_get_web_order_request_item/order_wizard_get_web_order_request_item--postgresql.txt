SELECT
    wori.web_order_request_item_id
    ,wori.web_cooperator_id
    ,wori.web_order_request_id
    ,wori.sequence_number
    ,wori.accession_id
    ,(SELECT c.site_id FROM cooperator c WHERE c.cooperator_id = a.owned_by) AS site_id
    ,(SELECT MIN(plant_name) FROM accession_inv_name an, inventory i WHERE i.inventory_id = an.inventory_id AND i.accession_id = a.accession_id AND plant_name_rank = (SELECT MIN(plant_name_rank) FROM accession_inv_name an2, inventory i2 WHERE an2.inventory_id = i2.inventory_id AND i2.accession_id = a.accession_id)) AS name
    ,a.taxonomy_species_id
    ,(SELECT s.geography_id FROM accession_source s WHERE a.accession_id = s.accession_id AND s.accession_source_id = (SELECT MIN(s2.accession_source_id) FROM accession_source s2 WHERE a.accession_id = s2.accession_id AND is_origin = 'Y')) AS geography_id
    ,wori.quantity_shipped
    ,wori.unit_of_shipped_code
    ,wori.distribution_form_code
    ,wori.status_code
    ,wori.curator_note
    ,wori.user_note
FROM
    web_order_request_item wori
    LEFT JOIN accession a ON wori.accession_id = a.accession_id
WHERE 
    wori.web_order_request_id IN (:weborderrequestid)
