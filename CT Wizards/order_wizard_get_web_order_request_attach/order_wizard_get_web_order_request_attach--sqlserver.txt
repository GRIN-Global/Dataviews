SELECT
    wora.web_order_request_attach_id
    ,wora.web_cooperator_id
    ,wora.web_order_request_id
    ,wora.virtual_path
    ,wora.content_type
    ,wora.title
    ,wora.status
    ,wora.note
FROM
    web_order_request_attach wora
WHERE 
    wora.web_order_request_id IN (:weborderrequestid)
