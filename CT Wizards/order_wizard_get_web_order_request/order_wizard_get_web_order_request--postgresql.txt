SELECT 
    wor.web_order_request_id
    ,wor.web_cooperator_id
    ,wc.last_name
    ,wc.title
    ,wc.first_name
    ,wc.organization
    ,wora.address_line1
    ,wora.address_line2
    ,wora.address_line3
    ,wora.city
    ,wora.postal_index
    ,wora.geography_id
    ,wc.primary_phone
    ,wc.email
    ,wor.ordered_date
    ,wor.intended_use_code
    ,wor.intended_use_note
    ,wor.status_code
    ,wor.note
    ,wor.special_instruction
FROM
    web_order_request wor
    LEFT JOIN web_cooperator wc ON wc.web_cooperator_id = wor.web_cooperator_id
    LEFT JOIN web_order_request_address wora on wora.web_order_request_id = wor.web_order_request_id
WHERE 
    wor.web_order_request_id IN (:weborderrequestid)
