SELECT
  cv.group_name, 
  cv.value,
  cvl.title,
  cvl.description 
FROM
  code_value cv 
  LEFT JOIN code_value_lang cvl
    ON cvl.code_value_id = cv.code_value_id
	and cvl.sys_lang_id = :langid

WHERE 1 = :viewallrows
