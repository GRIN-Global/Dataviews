SELECT
  st.table_name as table_name,
  stf.field_name as table_field_name,
  stfl.title as table_field_title,   
  stfl.description as table_field_description 
FROM
sys_table_field stf inner join sys_table st
on stf.sys_table_id = st.sys_table_id
left join sys_table_field_lang stfl
on stf.sys_table_field_id = stfl.sys_table_field_id
and stfl.sys_lang_id = :langid
where
1 = :viewallrows
 