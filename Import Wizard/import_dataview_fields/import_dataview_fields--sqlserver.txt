SELECT
  sd.dataview_name, 
  sdf.field_name as dataview_field_name,
  sdfl.title field_title, 
  sdfl.description field_description 
FROM
sys_dataview sd inner join sys_dataview_field sdf
on sd.sys_dataview_id = sdf.sys_dataview_id
left join sys_dataview_field_lang sdfl
on sdf.sys_dataview_field_id = sdfl.sys_dataview_field_id
and sdfl.sys_lang_id = :langid
where
1 = :viewallrows
order by
 sd.dataview_name,
 sdf.sort_order