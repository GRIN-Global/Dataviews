SELECT
  r.continent,
  r.subcontinent,
  r.sequence_number,
  r.continent_abbreviation,
  r.subcontinent_abbreviation,
  r.note
FROM
    region r
WHERE 
	1 = :viewallrows
