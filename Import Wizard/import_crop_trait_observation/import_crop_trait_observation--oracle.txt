SELECT
  crop.name,
  ct.coded_name,
  i.inventory_number_part1,
  i.inventory_number_part2,
  i.inventory_number_part3,
  i.form_type_code,
  m.name AS m_name,
  ctc.code,
  cto.numeric_value,
  cto.string_value
FROM
    inventory i
    LEFT JOIN crop_trait_observation cto
      ON  cto.inventory_id = i.inventory_id 
    LEFT JOIN crop_trait_code ctc
      ON  cto.crop_trait_code_id = ctc.crop_trait_code_id 
    LEFT JOIN crop_trait ct
      ON  ctc.crop_trait_id = ct.crop_trait_id 
    LEFT JOIN crop crop
      ON  ct.crop_id = crop.crop_id 
    LEFT JOIN method m
      ON  cto.method_id = m.method_id 

where 
 1 = :viewallrows
