SELECT
  g.country_code,
  g.adm1,
  g.adm1_type_code, 
  g.adm1_abbrev,
  g.adm2,
  g.adm2_type_code,
  g.adm2_abbrev,
  g.adm3,
  g.adm3_type_code,
  g.adm3_abbrev,
  g.adm4,
  g.adm4_type_code,
  g.adm4_abbrev,
  g.changed_date,
  g.note,
  m.name,
  m.elevation_meters,
  m.latitude,
  m.longitude,
  m.uncertainty,
  m.formatted_locality,
  m.georeference_datum,
  m.georeference_protocol_code,
  m.georeference_annotation,
  m.materials_and_methods,
  m.study_reason_code
FROM
    method m
    LEFT JOIN geography g
      ON  m.geography_id = g.geography_id 
WHERE
  1 = :viewallrows
