SELECT
  tf.suprafamily_rank_code AS suprafamily_rank_code,
  tf.suprafamily_rank_name AS suprafamily_rank_name,
  tf.family_name AS family_name,
  tf.family_authority AS family_authority,
  tf.alternate_name AS alternate_name,
  tf.subfamily_name AS subfamily_name,
  tf.tribe_name AS tribe_name,  
  tf.subtribe_name AS subtribe_name,
  tf.note AS note,
  tf1.family_name AS current_family_name,
  tf1.family_authority AS current_family_authority,
  tf1.subfamily_name AS current_subfamily_name,
  tf1.tribe_name AS current_tribe_name, 
  tf1.subtribe_name AS current_subtribe_name,
  tg.genus_name AS type_genus_name,
  tg.genus_authority AS type_genus_authority,
  tg.subgenus_name AS type_subgenus_name,
  tg.section_name AS type_section_name,
  tg.subsection_name AS type_subsection_name,
  tg.series_name AS type_series_name,
  tg.subseries_name AS type_subseries_name,
  tf2.family_name AS type_family_name,
  tf2.family_authority AS type_family_authority,
  tf2.subfamily_name AS type_subfamily_name,
  tf2.tribe_name AS type_tribe_name, 
  tf2.subtribe_name AS type_subtribe_name
FROM 
taxonomy_family tf
    LEFT JOIN taxonomy_family tf1
      ON  tf.current_taxonomy_family_id = tf1.taxonomy_family_id 
    LEFT JOIN taxonomy_genus tg
      ON tf.type_taxonomy_genus_id = tg.taxonomy_genus_id
    LEFT JOIN taxonomy_family tf2
      ON  tg.taxonomy_family_id = tf2.taxonomy_family_id 

where 1 = :viewallrows