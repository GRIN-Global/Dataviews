SELECT
   feedback.feedback_id AS value_member,
   feedback.title AS display_member
FROM
   feedback
WHERE
(feedback.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (feedback.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (feedback.feedback_id IN (:valuemember))
   OR (feedback.feedback_id BETWEEN :startpkey AND :stoppkey)