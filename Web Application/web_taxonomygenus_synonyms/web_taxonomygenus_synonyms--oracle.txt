select 
  tg.taxonomy_genus_id, 
  tg.current_taxonomy_genus_id,
  '(' || tg.qualifying_code || ')' as qualifying_code,
  tg.genus_name, 
   tg.genus_authority as authority
from taxonomy_genus tg
where tg.current_taxonomy_genus_id = :taxonomygenusid
and qualifying_code is not null
and tg.taxonomy_genus_id != :taxonomygenusid
order by tg.genus_name