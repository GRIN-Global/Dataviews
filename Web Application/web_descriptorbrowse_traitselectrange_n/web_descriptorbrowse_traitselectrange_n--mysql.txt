select
    numeric_value as crop_trait_code_text,
    concat('numeric_value|', convert(cto.numeric_value, char))
    as field_name_value
from
    crop_trait_observation cto
    inner join crop_trait ct
        on cto.crop_trait_id = ct.crop_trait_id
    left join crop_trait_lang ctl
        on ct.crop_trait_id = ctl.crop_trait_id
        and ctl.sys_lang_id = :langid1
    left join crop_trait_code ctc
        on cto.crop_trait_code_id = ctc.crop_trait_code_id
    left join crop_trait_code_lang ctcl
        on ctc.crop_trait_code_id = ctcl.crop_trait_code_id
        and ctcl.sys_lang_id = :langid2
    left join inventory i 
        on cto.inventory_id = i.inventory_id
    left join accession a
        on i.accession_id = a.accession_id
where
    cto.numeric_value is not null and
    cto.crop_trait_id = :traitid
    and cto.is_archived = 'N'
group by
    numeric_value,
    concat('numeric_value|', convert(cto.numeric_value, char))
order by
	1, 2