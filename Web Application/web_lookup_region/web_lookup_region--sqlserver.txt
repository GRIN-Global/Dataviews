select 
	region_id, 
	subcontinent 
from 
	region 
where 
	continent in 
	(select continent from region where region_id = :regionid) 
	and subcontinent is not null
order by 
	region_id