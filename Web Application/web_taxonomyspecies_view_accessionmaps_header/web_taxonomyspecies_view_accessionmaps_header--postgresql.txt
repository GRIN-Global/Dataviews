select distinct
        COALESCE (tg.genus_name, '') || ' ' ||
	COALESCE (t.species_name, '') || ' ' ||
	COALESCE (t.species_authority, '') || ' ' ||
	(case when t.subspecies_name IS NOT NULL then 'subsp. ' || t.subspecies_name || ' ' || COALESCE (t.subspecies_authority, '') || ' ' else '' end) ||
	(case when t.variety_name IS NOT NULL then 'var. ' || t.variety_name || ' ' || COALESCE (t.variety_authority, '') || ' ' else '' end) ||
	(case when t.forma_name IS NOT NULL then t.forma_name || ' ' || COALESCE (t.forma_authority, '') || ' ' else '' end)as taxonomy_name,
	(select count(distinct a1.accession_id) from accession a1 left join accession_source ascr on a1.accession_id = ascr.accession_id
	 where a1.taxonomy_species_id = :taxonomy_species_id  and latitude is not null and longitude is not null) as mapped,
        (select count(distinct a2.accession_id) from accession a2 where a2.taxonomy_species_id = :taxonomy_species_id) as total
from
    accession a
    left join taxonomy_species t 
		on a.taxonomy_species_id = t.taxonomy_species_id
	left join taxonomy_genus tg
		on t.taxonomy_genus_id = tg.taxonomy_genus_id
where 
    t.taxonomy_species_id = :taxonomy_species_id