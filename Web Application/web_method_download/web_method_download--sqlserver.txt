select distinct
	c.name as crop,
	m.name as method_name,
	t.name as taxon,
	a.accession_number_part1 as acp,
	a.accession_number_part2 as acno,
	a.accession_number_part3 as acs,
	ct.coded_name,
	case 
		when ct.is_coded = 'Y' then ctc.code
    		when cto.numeric_value is not null then convert(varchar, cast(cto.numeric_value as float))    	
    	else 
        	cto.string_value 
    	end as observation_value
from 
	accession a 
		inner join inventory i 
        	on a.accession_id = i.accession_id 
    	left join crop_trait_observation cto 
        	on cto.inventory_id = i.inventory_id 
    	left join crop_trait ct 
        	on cto.crop_trait_id = ct.crop_trait_id 
    	left join crop_trait_code ctc 
        	on cto.crop_trait_code_id = ctc.crop_trait_code_id 
    	left join method m 
			on cto.method_id = m.method_id 
		join taxonomy_species t
			on a.taxonomy_species_id = t.taxonomy_species_id
		join crop c
			on ct.crop_id = c.crop_id
where 
	cto.method_id = :methodid
order by coded_name