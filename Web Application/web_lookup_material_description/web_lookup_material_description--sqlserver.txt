select
    coalesce(cvl.description, cv.value) as description
from
    code_value cv
left join code_value_lang cvl
    on cv.code_value_id = cvl.code_value_id
    and cvl.sys_lang_id = :langid
where
    cv.group_name = 'GERMPLASM_FORM'
    and cv.value = :formcode



