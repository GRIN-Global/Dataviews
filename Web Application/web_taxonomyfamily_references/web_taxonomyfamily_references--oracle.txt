select
	cm.citation_id,
	c.literature_id,
	COALESCE (c.author_name, l.editor_author_name) as author,
	c.citation_year,
	COALESCE (c.citation_title, l.reference_title) as title,
	case when (COALESCE (l.standard_abbreviation, l.abbreviation)) = l.abbreviation then '(' || l.abbreviation || ')'
		else l.standard_abbreviation
		end
	 as abbrev,
	COALESCE (c.reference, '') as refernece,
	c.note as cit_note,
	l.note
from taxonomy_family tf join citation c
		on tf.taxonomy_family_id = c.taxonomy_family_id  
	left join literature l
		on c.literature_id = l.literature_id
where tf.taxonomy_family_id = :taxonomyfamilyid
order by author asc