select 
	trim(COALESCE (title, '')) as title_order,
	concat('<nobr><a onclick="javascript:return true;" href="ImageDisplay.aspx?type=taxonomy&id=', cast(taxonomy_attach_id as varchar2(50)), '" target="_blank">', trim(COALESCE (title, '')), '</a>') as title, 
	dbms_lob.substr(note,32767,1) AS note
from taxonomy_attach 
where taxonomy_genus_id = :taxonomygenusid
and is_web_visible = 'Y'
and category_code = 'IMAGE'
and virtual_path like '%.jpg'

union

select 
	trim(COALESCE (title, '')) as title_order,
	concat('<nobr><a onclick="javascript:return true;" href="', COALESCE(virtual_path, ''), '", target="_blank">', trim(COALESCE (title, '')), '</a>') as title, 
	dbms_lob.substr(note,32767,1) AS note
from taxonomy_attach 
where taxonomy_genus_id = :taxonomygenusid
and is_web_visible = 'Y'
and category_code = 'LINK'
and virtual_path not like '%.jpg'

order by title_order