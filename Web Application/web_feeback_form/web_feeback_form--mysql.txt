SELECT
  ff.feedback_form_id,
  ff.title,
  ff.created_date,
  ff.created_by,
  ff.modified_date,
  ff.modified_by,
  ff.owned_date,
  ff.owned_by
FROM
    feedback_form ff


