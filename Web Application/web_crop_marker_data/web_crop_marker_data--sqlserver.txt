Select 
	c.name as crop, 
	m.name as method_name, 
	a.accession_number_part1 as acp,
	a.accession_number_part2 as acNumber, 
	a.accession_number_part3 as acs,
	i.inventory_number_part1 as ivp, 
	i.inventory_number_part2 as ivNumber, 
	i.inventory_number_part3 as ivs, 
	i.form_type_code as ivType, 
	gobs.value
From
	genetic_observation_data gobs 
	join genetic_annotation ga 
		on gobs.genetic_annotation_id = ga.genetic_annotation_id
	join genetic_marker gm 
		on ga.genetic_marker_id = gm.genetic_marker_id
	join method m 
		on ga.method_id = m.method_id
	join inventory i  
		on gobs.inventory_id = i.inventory_id
	join accession a 
		on i.accession_id = a.accession_id
	join crop c 
		on gm.crop_id = c.crop_id
where 
	gm.genetic_marker_id = :markerid

order by 
	m.name, 
	a.accession_number_part1, 
	a.accession_number_part2, 
	a.accession_number_part3 