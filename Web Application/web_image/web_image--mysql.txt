select
    ia.accession_inv_attach_id as id,
    ia.virtual_path,
    ia.title,
    ia.description, 
    ia.created_date,
    concat(COALESCE (coop.last_name, ''), ', ', COALESCE (coop.first_name, ''), ', ', COALESCE (coop.organization, '')) as taken_by,
    coop.cooperator_id,
    trim(CONCAT (COALESCE (a.accession_number_part1, ''), ' ', COALESCE (CONVERT(a.accession_number_part2, char), ''), ' ', COALESCE (a.accession_number_part3, ''))) as pi_number,
    concat(COALESCE (i.inventory_number_part1, ''), ' ', COALESCE (CONVERT(i.inventory_number_part2, char), ''), ' ', COALESCE (i.inventory_number_part3, ''), ' ', i.form_type_code) as inv_number,
    COALESCE(ia.copyright_information, '') as copyright,
    COALESCE(ia.note, '') as note,
    ia.attach_date,
    ia.attach_date_code  
  
from accession_inv_attach ia
    left join cooperator coop
        on ia.attach_cooperator_id = coop.cooperator_id
    join inventory i
        on ia.inventory_id = i.inventory_id
    join accession a
 	on i.accession_id = a.accession_id
where ia.accession_inv_attach_id = :inven_imageid

union

select
    ia.taxonomy_attach_id as id,
    ia.virtual_path,
    ia.title,
    ia.description, 
    ia.created_date,
    COALESCE (coop.last_name, '') + ', ' + COALESCE (coop.first_name, '') + ', ' + COALESCE (coop.organization, '') as taken_by,
    coop.cooperator_id,
    '' as pi_number,
    '' as inv_number,
    COALESCE(ia.copyright_information, '') as copyright,
    COALESCE(ia.note, '') as note,
    '' as attach_date,
    '' as attach_date_code  
  
from taxonomy_attach ia
    left join cooperator coop
        on ia.attach_cooperator_id = coop.cooperator_id
where ia.taxonomy_attach_id = :taxon_imageid

union

select
    cta.crop_trait_attach_id as id,
    cta.virtual_path,
    cta.title,
    cta.description, 
    cta.created_date,
    COALESCE (coop.last_name, '') + ', ' + COALESCE (coop.first_name, '') + ', ' + COALESCE (coop.organization, '') as taken_by,
    coop.cooperator_id,
    '' as pi_number,
    '' as inv_number,
    '' as copyright,
    COALESCE(cta.note, '') as note,
    cta.attach_date,
    cta.attach_date_code  
  
from crop_trait_attach cta
    left join cooperator coop
        on cta.attach_cooperator_id = coop.cooperator_id
where cta.crop_trait_attach_id = :cta_imageid and cta.is_web_visible = 'Y'









