select 
	s.site_long_name as sitename, 
	COUNT(distinct a.accession_id) as acount,
	(select count(accession_id)  from accession where status_code = 'ACTIVE' ) as atotal
from 
	accession a join cooperator c 
	on a.owned_by = c.cooperator_id 
	join site s on c.site_id = s.site_id 
	where a.status_code = 'ACTIVE' 
group by 
	s.site_id, s.site_long_name 
order by 
	acount desc, s.site_long_name