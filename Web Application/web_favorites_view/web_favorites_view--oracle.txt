select 
    wuci.web_user_cart_item_id,	
    wuci.accession_id as accession_id,
    (coalesce(a.accession_number_part1,'') ||  ' ' || coalesce(cast(a.accession_number_part2 as varchar2(50)),'') || ' ' || coalesce(a.accession_number_part3,'')) as pi_number,
    wuci.usage_code,    
    wuci.note,
    case when sum(case when i.is_distributable = 'Y' AND i.is_available = 'Y' then 1 else 0 end) = 0 then 'Not Available'
    else 'Available'
    end as availability
from 
    web_user_cart_item wuci 
    left join web_user_cart wuc
	    on wuci.web_user_cart_id = wuc.web_user_cart_id
    left join accession a
	    on wuci.accession_id = a.accession_id
            and i.form_type_code != '**'
where wuc.cart_type_code = 'favorites' and wuc.web_user_id = :wuserid
group by 
	web_user_cart_item_id, 
	wuci.accession_id, 
	(coalesce(a.accession_number_part1,'') ||  ' ' || coalesce(cast(a.accession_number_part2 as varchar2(50)),'') || ' ' || coalesce(a.accession_number_part3,'')),
        wuci.usage_code, 
	wuci.note

Union all

select 
    wuci.web_user_cart_item_id,
    wuci.accession_id as accession_id,
    (coalesce(a.accession_number_part1,'') ||  ' ' || coalesce(cast(a.accession_number_part2 as varchar2(50)),'') || ' ' || coalesce(a.accession_number_part3,'')) as pi_number,
    wuci.usage_code,    
    wuci.note,
    case when sum(case when i.is_distributable = 'Y' AND i.is_available = 'Y' then 1 else 0 end) = 0 then 'Not Available'
    else 'Available'
    end as availability
from 
    web_user_cart_item wuci 
    left join web_user_cart wuc
	    on wuci.web_user_cart_id = wuc.web_user_cart_id
    left join accession a
	    on wuci.accession_id = a.accession_id
            and i.form_type_code != '**'
where wuc.cart_type_code = 'favorites' and wuci.accession_id = :accessionid
group by 
	web_user_cart_item_id, 
	wuci.accession_id, 
	(coalesce(a.accession_number_part1,'') ||  ' ' || coalesce(cast(a.accession_number_part2 as varchar2(50)),'') || ' ' || coalesce(a.accession_number_part3,'')),
        wuci.usage_code, 
	wuci.note
order by 1

