SELECT
  frg.feedback_result_group_id,
  frg.feedback_report_id,
  frg.participant_cooperator_id,
  frg.order_request_id,
  frg.started_date,
  frg.submitted_date,
  frg.accepted_date,
  frg.rejected_date, 
  frg.due_date,
  fr.feedback_id,
  fr.due_interval,
  fr.interval_length_code,
  fr.interval_type_code,
  fr.custom_due_date,
  fr.is_notified_initially,
  fr.is_notified_30days_prior,
  fr.is_notified_15days_prior,
  fr.initial_email_text,
  fr.prior30_email_text,
  fr.prior15_email_text,
  frg.created_date,
  frg.created_by,
  frg.modified_date,
  frg.modified_by,
  frg.owned_date,
  frg.owned_by
FROM
    feedback_result_group frg
left join feedback_report fr
	on fr.feedback_report_id = frg.feedback_report_id 
where frg.feedback_result_group_id = coalesce(:feedbackresultgroupid, frg.feedback_result_group_id)