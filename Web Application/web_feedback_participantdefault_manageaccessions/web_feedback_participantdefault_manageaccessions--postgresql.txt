select distinct
  concat('<nobr><a onclick="javascript:return true;" href="~/feedback/participantaccession.aspx?id=', cast(a.accession_id as varchar), '">', trim(CONCAT (COALESCE (a.accession_number_part1, ''), ' ', COALESCE (cast(a.accession_number_part2 as varchar), ''), ' ', COALESCE (a.accession_number_part3, ''))), '</a></nobr>') as pi_number,
 concat('<nobr><a onclick="javascript:return true;" href="~/feedback/participantorder.aspx?id=', cast(frg.order_request_id as varchar), '">', cast(frg.order_request_id as varchar), '</a></nobr>') as order_request_id,
(select plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id) limit 1) as top_name,
COALESCE (tg.genus_name, '') || ' ' ||
	COALESCE (t.species_name, '') || ' ' ||
		COALESCE (t.species_authority, '') || ' ' ||
			(case when t.subspecies_name IS NOT NULL then 'subsp. ' || t.subspecies_name || ' ' || COALESCE (t.subspecies_authority, '') || ' ' else '' end) ||
			(case when t.variety_name IS NOT NULL then 'var. ' || t.variety_name || ' ' || COALESCE (t.variety_authority, '') || ' ' else '' end) ||
			(case when t.forma_name IS NOT NULL then t.forma_name || ' ' || COALESCE (t.forma_authority, '') || ' ' else '' end)as taxonomy_name,
to_char(oreq.ordered_date, 'MM/DD/YYYY') as ordered_date, 
case 
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NULL then 'Submitted: Pending Review'
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NOT NULL then 'Accepted'
   when frg.started_date IS NOT NULL AND frg.accepted_date IS NULL then 'Started'
 else ''
 end as status,
to_char((select MIN(frg2.due_date) 
			from feedback_result_group frg2 
			  left join order_request oreq3 on oreq3.order_request_id = frg2.order_request_id 
			  where oreq3.requestor_cooperator_id = :cooperatorid AND frg2.order_request_id = frg.order_request_id AND frg2.due_date > now()), 'MM/DD/YYYY') as due_date

from feedback_result frslt
	left join feedback_result_group frg on frslt.feedback_result_group_id = frg.feedback_result_group_id
	left join order_request oreq on oreq.order_request_id = frg.order_request_id
	left join inventory i ON i.inventory_id = frslt.inventory_id
	left join accession a ON a.accession_id = i.accession_id
	left join accession_name an ON an.accession_id = a.accession_id
	left join taxonomy_species t 
		on a.taxonomy_species_id = t.taxonomy_species_id
	left join taxonomy_genus tg
		on t.taxonomy_genus_id = tg.taxonomy_genus_id
	left join feedback_report frpt ON frpt.feedback_report_id = frg.feedback_report_id
	left join feedback fb ON fb.feedback_id = frpt.feedback_id

where frg.participant_cooperator_id = :cooperatorid 
	AND frslt.inventory_id IS NOT NULL
	AND (select MIN(frg2.due_date) from feedback_result_group frg2 
	left join order_request oreq3 on oreq3.order_request_id = frg2.order_request_id 
	where oreq3.requestor_cooperator_id = :cooperatorid AND frg2.order_request_id = frg.order_request_id AND frg2.due_date > now()) = due_date