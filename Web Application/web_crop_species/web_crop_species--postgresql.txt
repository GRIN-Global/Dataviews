select  distinct 
	ts.taxonomy_species_id, 
	ts.name  
from 
	taxonomy_species ts 
	join  taxonomy_crop_map tcm 
	on ts.taxonomy_species_id = tcm.taxonomy_species_id
where 
	crop_id = :cropid 
order by name