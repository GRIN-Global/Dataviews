SELECT
	ROW_NUMBER() OVER (ORDER BY t.name) AS Row_Counter, 
	 concat('<nobr><a onclick="javascript:return true;" href="~/accessiondetail.aspx?id=', cast(a.accession_id as varchar2(50)), '">', trim(CONCAT (COALESCE (a.accession_number_part1, ''), ' ', COALESCE(cast(a.accession_number_part2 as varchar2(50)), ''), ' ', COALESCE (a.accession_number_part3, ''))), '</a></nobr>') as pi_number,
	t.name as taxonomy_name
from 
	accession a 
left join  taxonomy_species t 
	on a.taxonomy_species_id = t.taxonomy_species_id
where a.accession_id in 
   	(select 
		distinct a.accession_id 
	from accession a 
	join accession_source asrc 
		on a.accession_id  = asrc.accession_id
	join accession_source_map asm 
		on asrc.accession_source_id = asm.accession_source_id 
	where 
		asm.cooperator_id in
		(select cooperator_id 
		from cooperator 
		where current_cooperator_id = 
			(select current_cooperator_id 
			from cooperator 
			where cooperator_id = :cooperatorid)))
order by t.name asc
 