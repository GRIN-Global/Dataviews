SELECT
  distinct
  f.feedback_id,
  f.start_date,
  frep.feedback_report_id,
  frep.due_interval,
  frep.interval_length_code,
  frep.interval_type_code,
  frep.custom_due_date,
  frep.is_notified_initially,
  frep.is_notified_30days_prior,
  frep.is_notified_15days_prior,
  frep.initial_email_text,
  frep.prior30_email_text,
  frep.prior15_email_text,
  oreq.order_request_id,
  oreq.ordered_date,
  c.cooperator_id
FROM
    feedback f
    LEFT JOIN feedback_inventory fi
      ON  fi.feedback_id = f.feedback_id 
    LEFT JOIN order_request_item ori
      ON  ori.inventory_id = fi.inventory_id 
    LEFT JOIN order_request oreq
      ON  ori.order_request_id = oreq.order_request_id 
    LEFT JOIN cooperator c
      ON  oreq.requestor_cooperator_id = c.cooperator_id 
    LEFT JOIN feedback_report frep
      ON  frep.feedback_id = f.feedback_id 
    LEFT JOIN feedback_result_group fresg
      ON  fresg.feedback_report_id = frep.feedback_report_id 
          and  fresg.order_request_id = oreq.order_request_id 
    LEFT JOIN feedback_result fres
      ON fres.feedback_result_group_id = fresg.feedback_result_group_id
          and  fres.inventory_id = fi.inventory_id
WHERE oreq.ordered_date between f.start_date and f.end_date
and (fi.inventory_id = ori.inventory_id or f.is_restricted_by_inventory != 'Y')
and frep.feedback_report_id is not null
and fres.feedback_result_id is null
and (f.feedback_id = coalesce(:feedbackid, f.feedback_id)
     and oreq.order_request_id = coalesce(:orderrequestid, oreq.order_request_id)
     and c.cooperator_id = coalesce(:cooperatorid, c.cooperator_id)
    )
order by frep.feedback_report_id