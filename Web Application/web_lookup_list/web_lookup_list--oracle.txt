select
	coalesce(sdl.title, sd.dataview_name) as title,
	sd.dataview_name
from
	sys_dataview sd
	left join sys_dataview_lang sdl
		on sd.sys_dataview_id = sdl.sys_dataview_id
		and sdl.sys_lang_id = __LANGUAGEID__
where
	category_code = 'Web Application'
	and database_area_code = 'Lookup Table'
order by
	1