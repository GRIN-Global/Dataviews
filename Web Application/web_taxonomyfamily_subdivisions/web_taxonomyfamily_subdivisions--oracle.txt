select 
   '<a href="taxonomyfamily.aspx?type=subfamily&id=' || cast(taxonomy_family_id as varchar2(50)) || '">' || subfamily_name || '</a>' as Subfamily,
   '<a href="taxonomyfamily.aspx?type=tribe&id=' || cast(taxonomy_family_id as varchar2(50)) || '">' || tribe_name || '</a>' as Tribe,
   '<a href="taxonomyfamily.aspx?type=subtribe&id=' || cast(taxonomy_family_id as varchar2(50)) || '">' || subtribe_name || '</a>' as Subtribe
From taxonomy_family
where :columntype = 
(select :columntype from taxonomy_family where taxonomy_family_id = :taxonomyfamilyid)
and taxonomy_family_id <> :taxonomyfamilyid
order by subfamily_name, tribe_name, subtribe_name