select 
   concat('<a href="taxonomyfamily.aspx?type=subfamily&id=', convert(taxonomy_family_id, char), '">', subfamily_name, '</a>') as Subfamily,
   concat('<a href="taxonomyfamily.aspx?type=tribe&id=', convert(taxonomy_family_id, char), '">', tribe_name, '</a>') as Tribe,
   concat('<a href="taxonomyfamily.aspx?type=subtribe&id=', convert(taxonomy_family_id, char), '">', subtribe_name, '</a>') as Subtribe
From taxonomy_family
where :columntype = 
(select :columntype from taxonomy_family where taxonomy_family_id = :taxonomyfamilyid)
and taxonomy_family_id <> :taxonomyfamilyid
order by subfamily_name, tribe_name, subtribe_name