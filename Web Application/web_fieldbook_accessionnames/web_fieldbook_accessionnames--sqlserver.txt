select
	concat(an.category_code, ':', an.plant_name) as plantname
from
	accession a
	join inventory i
		on a.accession_id = i.accession_id
	join accession_inv_name an
		on i.inventory_id = an.inventory_id
where 
    a.accession_id = :accessionid
 order by 
	an.plant_name_rank   