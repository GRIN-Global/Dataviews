select
	concat('<a href="taxonomygenus.aspx?type=subgenus&id=', convert(taxonomy_genus_id, char), '">', subgenus_name, '</a>') as Subgenus,
	concat('<a href="taxonomygenus.aspx?type=section&id=', convert(taxonomy_genus_id, char), '">', section_name, '</a>') as Section,
	concat('<a href="taxonomygenus.aspx?type=subsection&id=', convert(taxonomy_genus_id, char), '">', subsection_name, '</a>') as Subsection,
	concat('<a href="taxonomygenus.aspx?type=series&id=', convert(taxonomy_genus_id, char), '">', series_name, '</a>') as Series,
	concat('<a href="taxonomygenus.aspx?type=Subseries&id=', convert(taxonomy_genus_id, char), '">', subseries_name, '</a>') as subseries
From 
	taxonomy_genus
where 
	:columntype = (select :columntype from taxonomy_genus where taxonomy_genus_id = :taxonomygenusid)
	and taxonomy_genus_id <> :taxonomygenusid
	and (qualifying_code is null or qualifying_code <> '=')
order by 
	subgenus_name, section_name, subsection_name, series_name, subseries_name