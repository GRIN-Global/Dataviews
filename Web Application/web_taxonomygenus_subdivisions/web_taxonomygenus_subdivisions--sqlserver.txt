select
	'<a href="taxonomygenus.aspx?type=subgenus&id=' + convert(nvarchar, taxonomy_genus_id) + '">' + subgenus_name + '</a>' as Subgenus,
	'<a href="taxonomygenus.aspx?type=section&id=' + convert(nvarchar, taxonomy_genus_id) + '">' + section_name + '</a>' as Section,
	'<a href="taxonomygenus.aspx?type=subsection&id=' + convert(nvarchar, taxonomy_genus_id) + '">' + subsection_name + '</a>' as Subsection,
	'<a href="taxonomygenus.aspx?type=series&id=' + convert(nvarchar, taxonomy_genus_id) + '">' + series_name + '</a>' as Series,
	'<a href="taxonomygenus.aspx?type=Subseries&id=' + convert(nvarchar, taxonomy_genus_id) + '">' + subseries_name + '</a>' as subseries
From 
	taxonomy_genus
where 
	:columntype = (select :columntype from taxonomy_genus where taxonomy_genus_id = :taxonomygenusid)
	and taxonomy_genus_id <> :taxonomygenusid
	and (qualifying_code is null or qualifying_code <> '=')
order by 
	subgenus_name, section_name, subsection_name, series_name, subseries_name