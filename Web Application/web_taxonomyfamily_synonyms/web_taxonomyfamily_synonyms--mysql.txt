select 
	tf.taxonomy_family_id, 
	tf.current_taxonomy_family_id, 
	tf.family_name, 
	COALESCE (tf.family_authority,'') as author_name
from 
	taxonomy_family tf
where 
	tf.current_taxonomy_family_id = :taxonomyfamilyid 
	and tf.taxonomy_family_id != :taxonomyfamilyid 
order by 
	tf.family_name