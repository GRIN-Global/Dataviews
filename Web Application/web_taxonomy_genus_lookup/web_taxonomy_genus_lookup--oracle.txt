select
	taxonomy_genus_id as value_member,
	coalesce(tf.family_name,'') || ' ' || coalesce(tf.family_authority,'') || ' ' || coalesce(tf.subfamily_name,'') || ' ' || coalesce(tf.tribe_name,'') || ' ' || coalesce(tf.subtribe_name,'') || ' ' || coalesce(tg.genus_name,'') || ' ' || coalesce(tg.genus_authority,'') || ' ' || coalesce(tg.subgenus_name,'') || ' ' || coalesce(tg.section_name,'') || ' ' || coalesce(tg.series_name,'') || ' ' || coalesce(tg.subseries_name,'') as display_member
from
	taxonomy_genus tg left join taxonomy_family tf
		on tg.taxonomy_family_id = tf.taxonomy_family_id
where
	tg.taxonomy_genus_id = :id
	or coalesce(tf.family_name,'') || ' ' || coalesce(tf.family_authority,'') || ' ' || coalesce(tf.subfamily_name,'') || ' ' || coalesce(tf.tribe_name,'') || ' ' || coalesce(tf.subtribe_name,'') || ' ' || coalesce(tf.family_name,'') || ' ' || coalesce(tf.family_authority,'') || ' ' || coalesce(tg.genus_name,'') || ' ' || coalesce(tg.genus_authority,'') || ' ' || coalesce(tg.subgenus_name,'') || ' ' || coalesce(tg.section_name,'') || ' ' || coalesce(tg.series_name,'') || ' ' || coalesce(tg.subseries_name,'') LIKE :txt	
order by
	2