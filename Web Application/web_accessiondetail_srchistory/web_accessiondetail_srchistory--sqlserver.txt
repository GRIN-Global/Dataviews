select distinct
	asrc.source_type_code as type_code,
	asrc.accession_source_id,
	asrc.source_date,
        asrc.source_date_code,
	--LTRIM(RTRIM(concat(g.adm4, ' ', g.adm3, ' ', g.adm2, ' ', g.adm1))) as state_full_name,
	LTRIM(RTRIM((COALESCE(g.adm4, '') + ' ' + COALESCE(g.adm3, '') + ' '+ COALESCE(g.adm2, '') + ' ' + COALESCE(g.adm1, '')))) as state_full_name,
	cvl.title as country_name,
        collector_verbatim_locality as verbatim,
        environment_description as habitat,
        cast(quantity_collected as float) as quantity_collected,
        unit_quantity_collected_code,
	(case when ((COALESCE (convert(nvarchar, asrc.latitude), '') = '') or (COALESCE (convert(nvarchar, asrc.longitude), '') = '')) then ''
          else (COALESCE (asrc.formatted_locality, '')  
		    + ' Latitude: '  
		    + convert(nvarchar, cast(asrc.latitude as integer))
		    + ' deg. ' 
		    + convert(nvarchar, CAST((asrc.latitude - cast(asrc.latitude as integer)) * 60 as integer)) 
		    + ' min. '
		    + convert(nvarchar, cast(((asrc.latitude - CAST(asrc.latitude as integer)) * 60 - CAST((asrc.latitude - cast(asrc.latitude as integer)) * 60 	
			as integer)) * 60 as integer))
		    + ' sec.' +  (case when asrc.latitude > 0 then ' North ' else ' South ' end) + '(' + convert(nvarchar, asrc.latitude) + ')'   
		    + ', '
		    + 'Longitude: ' 
		    + convert(nvarchar, cast(asrc.longitude as integer))
		    + ' deg. ' 
		    + convert(nvarchar, CAST((asrc.longitude - cast(asrc.longitude as integer)) * 60 as integer)) 
		    + ' min. '
		    + convert(nvarchar, cast(((asrc.longitude - CAST(asrc.longitude as integer)) * 60 - CAST((asrc.longitude - cast(asrc.longitude as integer)) * 
			60 as integer)) * 60 as integer))
		    + ' sec.' + (case when asrc.longitude > 0 then ' East ' else ' West ' end) +  '(' + convert(nvarchar, asrc.longitude) + ') '   
		    + '<a onclick=''javascript:return true;'' href=''maps.aspx?id=' + convert(nvarchar, asrc.accession_id) + '''> GoogleMap </a> it. ' )
		    end) as locality, 
	asrc.elevation_meters, 
        asrc.associated_species,
	asrc.note,
	case when asrc.is_web_visible = 'N' or exists (select tu.economic_usage_code from accession a join taxonomy_use tu on a.taxonomy_species_id = tu.taxonomy_species_id where tu.economic_usage_code in ('FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII') and a.accession_id = :accessionid) then 'Y' else 'N' end as Nolocation
from 
	accession_source asrc
	left join geography g on asrc.geography_id = g.geography_id
	left join code_value cv on cv.value = g.country_code
	left join code_value_lang cvl on cvl.code_value_id = cv.code_value_id
	join accession a on asrc.accession_id = a.accession_id
where 
	asrc.accession_id = :accessionid
	and coalesce(cv.group_name, 'GEOGRAPHY_COUNTRY_CODE') = 'GEOGRAPHY_COUNTRY_CODE'
	and coalesce(cvl.sys_lang_id, __LANGUAGEID__) = __LANGUAGEID__