select distinct
	a.accession_id,
    	concat(coalesce(a.accession_number_part1,''), ' ', COALESCE(cast(a.accession_number_part2 as varchar2(50)), ''), ' ', coalesce(a.accession_number_part3,'')) as pi_number,
   	(select TOP 1 plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id)) as top_name,
    	case 
		when ct.is_coded = 'Y' then ctc.code
    		when cto.numeric_value is not null then convert(varchar, cast(cto.numeric_value as float))    	
    	else 
        	cto.string_value 
    	end as traitvalue,
    	cto.minimum_value as mini,
    	cto.maximum_value as maxi,
    	cto.mean_value as mean,
    	cto.standard_deviation as sdev,
    	cto.sample_size as size,
	cto.note
from 
	accession a 
	inner join inventory i 
        	on a.accession_id = i.accession_id 
    	left join crop_trait_observation cto 
        	on cto.inventory_id = i.inventory_id 
    	left join crop_trait ct 
        	on cto.crop_trait_id = ct.crop_trait_id 
    	left join crop_trait_code ctc 
        	on cto.crop_trait_code_id = ctc.crop_trait_code_id 
    	left join method m 
		on cto.method_id = m.method_id 
where 
	ct.crop_trait_id = :traitid	
	and cto.method_id = :methodid
	and a.is_web_visible = 'Y'
order by traitvalue