SELECT
  frg.feedback_result_group_id,
  frg.participant_cooperator_id,
  fr1.title,
  f.title as program_title,
  year(f.start_date) as year,
  frg.submitted_date,
  ltrim(rtrim(coalesce(c.last_name,'') + ', ' + coalesce(c.first_name, ''))) as full_name
FROM
    feedback_result_group frg
    LEFT JOIN feedback_report fr1
      ON  frg.feedback_report_id = fr1.feedback_report_id 
    LEFT JOIN feedback f
      ON  fr1.feedback_id = f.feedback_id 
    LEFT JOIN cooperator c
      ON  frg.participant_cooperator_id = c.cooperator_id 

WHERE 
	frg.accepted_date is null 
	and frg.rejected_date is null 
	and frg.submitted_date is not null
and f.owned_by = :cooperatorid