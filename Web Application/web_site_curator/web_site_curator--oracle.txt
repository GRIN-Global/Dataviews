select 
	c.first_name || ' ' || c.last_name as name,
	cm.note,
	c.email
from 
	cooperator c 
	join cooperator_map cm on c.cooperator_id = cm.cooperator_id   
	join cooperator_group cg on cm.cooperator_group_id = cg.cooperator_group_id 
where 
	c.cooperator_id in 
    	(select 
		cooperator_id 
	 from 
		cooperator_map cm 
		join cooperator_group cg on cm.cooperator_group_id = cg.cooperator_group_id 
    		join site s on  cg.group_tag = s.site_short_name  
	where 
		s.site_id = :siteid)
	and group_tag = 'CURATORS' 
order by
    last_name