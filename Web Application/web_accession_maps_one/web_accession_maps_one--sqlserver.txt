select (coalesce(a.accession_number_part1,'') + ' ' + coalesce(convert(varchar, a.accession_number_part2), '') + coalesce(a.accession_number_part3,'')) as pi_number,
        coalesce(convert(varchar, asrc.latitude), '') as latitude,
        coalesce(convert(varchar, asrc.longitude), '') as longitude,
        coalesce(asrc.collector_verbatim_locality, '') as location,
        0 as count, a.accession_id as accession_id
from accession a 
	 left join accession_source asrc on a.accession_id = asrc.accession_id 
	 where a.accession_id = :accessionid
	 and asrc.latitude is not null and asrc.longitude is not null

 