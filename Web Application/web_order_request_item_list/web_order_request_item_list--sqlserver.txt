select 
   s.site_short_name as site, 
   i.inventory_id, 
   t.taxonomy_species_id, 
   (t.name) as name, 
   'marty.reisinger@ars.usda.gov' as email,
   i.distribution_unit_code, 
   i.distribution_default_form_code as form_type_code, 
   a.accession_id,
   concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(varchar, a.accession_number_part2),''), ' ', coalesce(a.accession_number_part3,'')) as pi_number,
   (select coalesce(aipr.type_code, '') from accession_ipr aipr where aipr.accession_id = a.accession_id and type_code like 'MTA%') as type_code
from 
   accession a 
   left join taxonomy_species t on a.taxonomy_species_id = t.taxonomy_species_id 
   left join taxonomy_genus tg on t.taxonomy_genus_id = tg.taxonomy_genus_id
   left join inventory i on i.accession_id = a.accession_id
   left join cooperator c1 on a.owned_by = c1.cooperator_id
   left join site s on c1.site_id = s.site_id
where 
   a.accession_id in (:idlist)
   and i.form_type_code != '**'
   and i.is_distributable = 'Y' AND i.is_available = 'Y'
order by 
   inventory_id desc
