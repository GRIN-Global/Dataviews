select
  ca.crop_attach_id 
  ,COALESCE (ca.virtual_path, '') as virtual_path
  ,COALESCE (ca.thumbnail_virtual_path, ca.virtual_path, '') as thumbnail_virtual_path
  ,COALESCE (ca.title, '')as title 
  ,COALESCE (ca.description, '') as description
  ,note
from
  crop_attach ca 
where
  crop_id = :cropid
  and is_web_visible = 'Y'
  and category_code = :categorycode
order by sort_order