select
    distinct
	cv.value as value,
	coalesce(to_char(cast(cvl.description as varchar2(2000))), cv.value)  as display_text
from
	inventory i
	left join code_value cv
		on cv.group_name = 'GERMPLASM_FORM'
		and cv.value = i.distribution_default_form_code
	left join code_value_lang cvl
		on cv.code_value_id = cvl.code_value_id
		and cvl.sys_lang_id = :langid
where
	i.accession_id = :accessionid
	and i.is_distributable = 'Y'
	and i.is_available = 'Y'
