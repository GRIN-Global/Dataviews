select 
    (coalesce(a.accession_number_part1,'') + ' ' + coalesce(convert(varchar, a.accession_number_part2), '') + coalesce(a.accession_number_part3,'')) as pi_number,
    COALESCE (tg.genus_name, '') + ' ' +
	COALESCE (t.species_name, '') + ' ' +
	COALESCE (t.species_authority, '') + ' ' +
	(case when t.subspecies_name IS NOT NULL then 'subsp. ' + t.subspecies_name + ' ' + COALESCE (t.subspecies_authority, '') + ' ' else '' end) +
	(case when t.variety_name IS NOT NULL then 'var. ' + t.variety_name + ' ' + COALESCE (t.variety_authority, '') + ' ' else '' end) +
	(case when t.forma_name IS NOT NULL then t.forma_name + ' ' + COALESCE (t.forma_authority, '') + ' ' else '' end)as taxonomy_name,
	(select count(distinct a2.accession_id) from accession a2 
	 	left join accession_source ascr on a2.accession_id = ascr.accession_id
	 	where a2.taxonomy_species_id = (select taxonomy_species_id from accession where accession_id = :accessionid)
                and latitude is not null and longitude is not null and ascr.is_web_visible = 'Y') as total 
from
    accession a
    left join taxonomy_species t 
		on a.taxonomy_species_id = t.taxonomy_species_id
	left join taxonomy_genus tg
		on t.taxonomy_genus_id = tg.taxonomy_genus_id
where 
    a.accession_id = :accessionid