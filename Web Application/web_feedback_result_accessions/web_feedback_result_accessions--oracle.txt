select
	coalesce(a.accession_number_part1,'') || ' ' || coalesce(cast(a.accession_number_part2 as varchar2(50)),'') || ' ' || coalesce(a.accession_number_part3,'') as pi_number,
	fres.feedback_result_id,
 	cast(fres.feedback_result_id as varchar2(50)) || '|' || cast(fres.inventory_id as varchar2(50)) as result_and_inventory
from
	feedback_result_group frg
	left join feedback_result fres
		on fres.feedback_result_group_id = frg.feedback_result_group_id
	left join inventory i
		on fres.inventory_id = i.inventory_id
	left join accession a 
		on a.accession_id = i.accession_id
where
	frg.feedback_result_group_id = :groupid
order by 
	1