SELECT
  fr1.inventory_id as data_id,
  concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(a.accession_number_part2, char),''), ' ', coalesce(a.accession_number_part3,'')) as accession_number,
  'Field' as data_type,
  fff.title as data_name,
  coalesce(frf.admin_value, frf.string_value, 
       (select coalesce(frf2.string_value, fff2.default_value) 
          from feedback_result_field frf2 inner join feedback_result fr2 on frf2.feedback_result_id = fr2.feedback_result_id
	    inner join feedback_result_group frg2 on fr2.feedback_result_group_id = frg2.feedback_result_group_id 
            inner join feedback_form_field fff2 on fff2.feedback_form_field_id = frf2.feedback_form_field_id
          where concat(convert(frg2.feedback_report_id, char), '|', convert(fff2.feedback_form_field_id, char)) = fff.references_tag limit 1)) as data_value
FROM 
    feedback f
    INNER JOIN feedback_report fr
	on f.feedback_id = fr.feedback_id
    LEFT JOIN feedback_form ff
	on fr.feedback_form_id = ff.feedback_form_id
    LEFT JOIN feedback_form_field fff
      ON  fr.feedback_form_id = fff.feedback_form_id 
    LEFT JOIN feedback_result_group frg
      ON  frg.feedback_report_id = fr.feedback_report_id
    LEFT JOIN feedback_result fr1
      ON  fr1.feedback_result_group_id = frg.feedback_result_group_id 
    LEFT JOIN feedback_result_field frf
      ON  frf.feedback_result_id = fr1.feedback_result_id 
          and frf.feedback_form_field_id = fff.feedback_form_field_id
    LEFT JOIN inventory i 
      ON  i.inventory_id = fr1.inventory_id
    LEFT JOIN accession a
      ON  a.accession_id = i.accession_id

WHERE 
  fr1.feedback_result_group_id = coalesce(:feedbackresultgroupid, fr1.feedback_result_group_id)
UNION
SELECT
  fr1.inventory_id as data_id,
  concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(a.accession_number_part2, char),''), ' ', coalesce(a.accession_number_part3,'')) as accession_number,
  'Trait' as data_type,
  coalesce(fft.title, ctl.title, ct.coded_name) as data_name,   coalesce(frto.admin_value, frto.string_value, convert(frto.numeric_value, char),
    (select coalesce(frto2.admin_value, frto2.string_value, convert(frto2.numeric_value, char))
       from feedback_result_trait_obs frto2 inner join feedback_result fr2 on frto2.feedback_result_id = fr2.feedback_result_id
	  inner join feedback_result_group frg2 on fr2.feedback_result_group_id = frg2.feedback_result_group_id 
         inner join feedback_form_trait fft2 on fft2.feedback_form_trait_id = frto2.feedback_form_trait_id
       where concat(convert(frg2.feedback_report_id, char), '|', convert(fft2.feedback_form_trait_id, char)) = fft.references_tag limit 1)) as data_value
FROM
    feedback_report fr
    LEFT JOIN feedback_form ff
      ON  fr.feedback_form_id = ff.feedback_form_id 
    LEFT JOIN feedback_form_trait fft
      ON  fft.feedback_form_id = ff.feedback_form_id 
    LEFT JOIN feedback_result_group frg
      ON  frg.feedback_report_id = fr.feedback_report_id
    LEFT JOIN feedback_result fr1
      ON  fr1.feedback_result_group_id = frg.feedback_result_group_id 
    LEFT JOIN feedback_result_trait_obs frto
      ON  frto.feedback_result_id = fr1.feedback_result_id 
          and frto.crop_trait_id = fft.crop_trait_id 
    LEFT JOIN crop_trait ct
      ON  fft.crop_trait_id = ct.crop_trait_id 
    LEFT JOIN crop_trait_lang ctl
      ON  ctl.crop_trait_id = ct.crop_trait_id 
          and  ctl.sys_lang_id = __LANGUAGEID__ 
    LEFT JOIN inventory i
      ON  i.inventory_id = fr1.inventory_id
    LEFT JOIN accession a
      ON  a.accession_id = i.accession_id
WHERE 
  fr1.feedback_result_group_id = coalesce(:feedbackresultgroupid, fr1.feedback_result_group_id)
  and fr.feedback_form_id is not null 
  and fft.feedback_form_trait_id is not null
order by 
  1, 2, 3