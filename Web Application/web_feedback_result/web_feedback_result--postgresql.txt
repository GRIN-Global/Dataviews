SELECT
  fr.feedback_result_id,
  frg.feedback_report_id,
  frg.participant_cooperator_id,
  frg.order_request_id,
  fr.feedback_result_group_id,
  fr.inventory_id,
  coalesce(a.accession_number_part1,'') || ' ' || coalesce(cast(a.accession_number_part2 as varchar(50)),'') || ' ' || coalesce(a.accession_number_part3,'') as accession_number,
  frg.started_date,
  frg.submitted_date,
  frg.accepted_date,
  frg.due_date,
  fr.created_date,
  fr.created_by,
  fr.modified_date,
  fr.modified_by,
  fr.owned_date,
  fr.owned_by
FROM
    feedback_result fr inner join feedback_result_group frg
	on fr.feedback_result_group_id = frg.feedback_result_group_id
	left join inventory i on i.inventory_id = fr.inventory_id
	left join accession a on a.accession_id = i.accession_id
where fr.feedback_result_id = coalesce(:feedbackresultid, fr.feedback_result_id)