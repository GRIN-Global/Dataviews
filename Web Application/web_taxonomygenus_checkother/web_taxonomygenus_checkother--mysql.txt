select
  concat('<a href="http://botany.si.edu/ing/INGsearch.cfm?searchword=', tg.genus_name, '"  target="_blank"><b> ING  </b> </a>')  as otherDBlink, 'Index Nominum Genericorum' as 
otherDB 
from taxonomy_genus tg
  where tg.taxonomy_genus_id = :taxonomygenusid
Union
select
  concat('<a href="http://www.bgbm.org/scripts/asp/IAPT/ncugentry.asp?name=', tg.genus_name, '"  target="_blank"><b> NCU-3e </b> </a>')  as otherDBlink, 'Names in Current Use 
  for Extant Plant Genera' as otherDB 
from taxonomy_genus tg
where tg.taxonomy_genus_id = :taxonomygenusid