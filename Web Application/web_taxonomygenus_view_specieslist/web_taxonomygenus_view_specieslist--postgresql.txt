select ('<a href="taxonomydetail.aspx?id=' || cast(taxonomy_species_id as varchar(50)) || '">' || 
	COALESCE (tg.genus_name, '') || ' ' ||
	COALESCE (t.species_name, '') || ' ' ||
	(case when t.subspecies_name IS NOT NULL then 'subsp. ' || t.subspecies_name || ' ' else '' end)   
|| '  ' ||  '</a><b>' || COALESCE (t.species_authority, '') || '</b>') as name
from taxonomy_species t join taxonomy_genus tg on t.taxonomy_genus_id = tg.taxonomy_genus_id
and t.taxonomy_genus_id in
(select taxonomy_genus_id from taxonomy_genus where :columntype =
	(select :columntype from taxonomy_genus where taxonomy_genus_id = :taxonomygenusid))
order by t.species_name