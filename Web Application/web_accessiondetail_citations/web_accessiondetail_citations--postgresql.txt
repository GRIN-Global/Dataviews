select 
    cit.citation_year,
    cit.reference,
    cit.citation_title as ctitle,
    cit.author_name,
    cit.note,
    l.abbreviation,
    cit.url,
    cit.doi_reference,
    cit.title,
    cit.description
from citation cit
    left join literature l
	on cit.literature_id = l.literature_id
where cit.accession_id = :accessionid
order by 1 desc