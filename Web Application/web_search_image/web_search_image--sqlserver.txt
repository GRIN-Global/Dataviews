SELECT
  a.accession_id,
 '<a onclick="javascript:return true;" href="~/accessiondetail.aspx?id=' + rtrim(cast(a.accession_id as varchar(12))) + '">View</a>' AS Url,
  rtrim(a.accession_number_part1) + ' ' + str(a.accession_number_part2,8,0) + ' ' + ltrim(isnull(accession_number_part3,' ')) AS pi_number,
s.name as species, 
dbo.get_images(a.accession_id) as image
  from accession a 
join taxonomy_species s on a.taxonomy_species_id=s.taxonomy_species_id
where a.accession_id in (:idlist)  and a.is_web_visible = 'Y'