select distinct
    ct.crop_id,
    ct.crop_trait_id, 
    ct.category_code,
    ct.coded_name,
    concat('<nobr><a href="descriptordetail.aspx?id=', convert(ct.crop_trait_id, char), '">', coalesce(ct.coded_name, ctl.title), '</a></nobr>') as display_text,
    coalesce(ctl.title, ct.coded_name) as title
from 
    crop_trait ct left join crop_trait_lang ctl
        on ct.crop_trait_id = ctl.crop_trait_id
        and ctl.sys_lang_id = :langid
    join crop_trait_observation cto
        on ct.crop_trait_id = cto.crop_trait_id
where 
    ct.category_code = coalesce(:cat, ct.category_code)
    and ct.crop_id = :cropid
    and cto.is_archived = 'N'
order by  
    ct.category_code,
    ct.coded_name
