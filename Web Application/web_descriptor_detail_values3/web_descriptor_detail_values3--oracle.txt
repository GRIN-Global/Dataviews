SELECT
	COUNT(distinct a.accession_id) as cnt
from 
	crop_trait_observation cto 
	join inventory i 
		on cto.inventory_id =  i.inventory_id 
         join accession a 
		on a.accession_id = i.accession_id
 where 
	cto.crop_trait_id = :descriptorid
	and cto.numeric_value >= :value1 
	and cto.numeric_value <= :value2    