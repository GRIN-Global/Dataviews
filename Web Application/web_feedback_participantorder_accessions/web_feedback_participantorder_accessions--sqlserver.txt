select distinct
frg.order_request_id,
ori.web_order_request_item_id,
ori.inventory_id,
a.accession_id,
(coalesce(a.accession_number_part1,'') + ' ' + coalesce(convert(varchar, a.accession_number_part2), '') + coalesce(a.accession_number_part3,'')) as pi_number,
(select TOP 1 plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id)) as top_name,
COALESCE (tg.genus_name, '') + ' ' +
	COALESCE (t.species_name, '') + ' ' +
		COALESCE (t.species_authority, '') + ' ' +
			(case when t.subspecies_name IS NOT NULL then 'subsp. ' + t.subspecies_name + ' ' + COALESCE (t.subspecies_authority, '') + ' ' else '' end) +
			(case when t.variety_name IS NOT NULL then 'var. ' + t.variety_name + ' ' + COALESCE (t.variety_authority, '') + ' ' else '' end) +
			(case when t.forma_name IS NOT NULL then t.forma_name + ' ' + COALESCE (t.forma_authority, '') + ' ' else '' end)as taxonomy_name 
from feedback_result_group frg
	left join order_request_item ori ON ori.order_request_id = frg.order_request_id
	left join inventory i ON i.inventory_id = ori.inventory_id
	left join accession a ON a.accession_id = i.accession_id
	left join taxonomy_species t 
		on a.taxonomy_species_id = t.taxonomy_species_id
	left join taxonomy_genus tg
		on t.taxonomy_genus_id = tg.taxonomy_genus_id
where frg.order_request_id = :order_request_id