select  (coalesce(a.accession_number_part1,'') + ' ' + coalesce(convert(varchar, a.accession_number_part2), '') + coalesce(a.accession_number_part3,'')) as pi_number,
        coalesce(convert(varchar, asrc.latitude), '') as latitude,
        coalesce(convert(varchar, asrc.longitude), '') as longitude,
        coalesce(asrc.collector_verbatim_locality, '') as location,
        1 as count, a.accession_id as accession_id 
from accession a 
	join accession_source asrc on a.accession_id = asrc.accession_id
	join 
	(select ascr.latitude, ascr.longitude 
  	from accession_source ascr 
 	where accession_id  in (select a.accession_id from accession a where a.taxonomy_species_id = :taxonomy_species_id)
	and latitude is not null and longitude is not null and is_web_visible= 'Y' 
	group by ascr.latitude, ascr.longitude 
	having COUNT(ascr.accession_source_id) = 1) ascr2 
	on asrc.latitude = ascr2.latitude and asrc.longitude = ascr2.longitude  
	and asrc.accession_id in (select a.accession_id from accession a where a.taxonomy_species_id = :taxonomy_species_id)

union

 select distinct '' as pi_number,
        coalesce(convert(varchar, ascr.latitude), '') as latitude,
        coalesce(convert(varchar, ascr.longitude), '') as longitudem,
        '' as location,
        --coalesce(convert(varchar, ascr.collector_verbatim_locality), '') as location,
        COUNT(ascr.accession_source_id) as count, 0 as accession_id 
 from accession_source ascr 
 where accession_id  in (select a.accession_id from accession a where a.taxonomy_species_id = :taxonomy_species_id)
 	and latitude is not null and longitude is not null and is_web_visible= 'Y' 
 group  by ascr.latitude, ascr.longitude--, ascr.collector_verbatim_locality 
 having COUNT(ascr.accession_source_id) > 1	
