select
	asrc.source_type_code as type_code,
	coalesce(g.adm1, '') as state_full_name,
	(select cvl.title 
                from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
                where cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' and cvl.sys_lang_id = __LANGUAGEID__  
               and cv.value in (select g.country_code from geography g join accession_source asr on g.geography_id = asr.geography_id
               and asr.accession_source_id = :accessionsrcid) limit 1) as country_name,
    coop.cooperator_id,
    coalesce(coop.last_name,'') as last_name,
    coalesce(coop.first_name,'') as first_name,
    coalesce(coop.organization,'') as organization
from accession_source asrc 
	left join geography g
		on asrc.geography_id = g.geography_id
	left join accession_source_map asrcmap
		on asrc.accession_source_id = asrcmap.accession_source_id
	join cooperator coop
		on asrcmap.cooperator_id = coop.cooperator_id
where asrc.accession_source_id = :accessionsrcid