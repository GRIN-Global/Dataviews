select
    accession_inv_group_id as value,
    group_name as title
from
    accession_inv_group
where
    is_web_visible = 'Y'

