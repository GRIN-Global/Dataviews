select 
	ROW_NUMBER() OVER (ORDER BY a.accession_id) AS row_count,
	a.accession_id,
    	concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(varchar, a.accession_number_part2),''), ' ', coalesce(a.accession_number_part3,'')) as pi_number,
   	(select TOP 1 plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id)) as top_name,
	ts.name as species,
	cto.string_value as traitvalue
 from 
	accession a 
	inner join inventory i 
        	on a.accession_id = i.accession_id 
    	left join crop_trait_observation cto 
        	on cto.inventory_id = i.inventory_id 
	join taxonomy_species ts
		on a.taxonomy_species_id = ts.taxonomy_species_id
where 
	cto.crop_trait_id = :traitid	
	and cto.string_value = :value
	and a.is_web_visible = 'Y'
order by 1