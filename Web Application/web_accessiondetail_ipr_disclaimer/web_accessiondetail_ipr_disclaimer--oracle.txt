select 
		cvl.title as title,   
		coalesce(cvl.description, to_clob('')) as description,
		aipr.accession_ipr_id,
		aipr.type_code,
		aipr.ipr_number    
	from accession_ipr aipr    
	left join code_value cv    
	    on aipr.type_code = cv.value   
	left join code_value_lang cvl    
	    on cv.code_value_id = cvl.code_value_id                 
	    and cvl.sys_lang_id = __LANGUAGEID__   
	where aipr.accession_ipr_id = :accessioniprid   
	    and cv.group_name = 'ACCESSION_RESTRICTION_TYPE'