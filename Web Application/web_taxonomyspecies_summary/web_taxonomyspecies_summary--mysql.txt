select
    t.taxonomy_species_id,
    convert(t.nomen_number, char) as nomen_number,
    concat('<i>', COALESCE (tg.genus_name, ''), ' ',
	COALESCE (t.species_name, ''), '</i> ',
	COALESCE (t.species_authority, ''), ' ',
	(case when t.subspecies_name IS NOT NULL then concat('subsp. <i>', t.subspecies_name, '</i> ', COALESCE (t.subspecies_authority, ''),  ' ') else '' end),
	(case when t.variety_name IS NOT NULL then concat('var. <i>', t.variety_name, '</i> ', COALESCE (t.variety_authority, ''), ' ') else '' end),
	(case when t.forma_name IS NOT NULL then concat('forma <i>', t.forma_name, '</i> ', COALESCE (t.forma_authority, ''), ' ') else '' end)) as taxonomy_name,
    t.name,
    (select concat('<h2>Synonym of <a href=''taxonomydetail.aspx?id=', convert(t2.taxonomy_species_id, char), '''>', t2.name, '</a></h2>') from taxonomy_species t2 where taxonomy_species_id = t.current_taxonomy_species_id and t.synonym_code in ('B', 'S', '=') and t2.taxonomy_species_id != t.taxonomy_species_id limit 1) as synonym_for_taxonomy,
    t.name_authority,
    (select concat('<a href="taxonomygenus.aspx?id=', convert(taxonomy_genus_id, char), '">',  genus_name, '</a>') from taxonomy_genus where genus_name = tg.genus_name and subgenus_name is null and section_name is null and subsection_name is null and series_name is null and subseries_name is null and qualifying_code is null) as genus_name,
    (select concat('<a href="taxonomygenus.aspx?type=subgenus&id=', convert(taxonomy_genus_id, char), '">',  subgenus_name, '</a>') from taxonomy_genus where subgenus_name = tg.subgenus_name and section_name is null and subsection_name is null and series_name is null and subseries_name is null limit 1) as subgenus_name,
    (select concat('<a href="taxonomygenus.aspx?type=section&id=', convert(taxonomy_genus_id, char), '">', section_name, '</a>') from taxonomy_genus where section_name = tg.section_name and subsection_name is null and series_name is null and subseries_name is null limit 1) as section_name,
    (select concat('<a href="taxonomygenus.aspx?type=subsection&id=', convert(taxonomy_genus_id, char), '">', subsection_name, '</a>') from taxonomy_genus where subsection_name = tg.subsection_name and series_name is null and subseries_name is null limit 1) as subsection_name,
    (select concat('<a href="taxonomyfamily.aspx?id=', convert(taxonomy_family_id, char), '">', family_name, '</a>') from taxonomy_family where family_name = tf.family_name and subfamily_name is null and tribe_name is null and subtribe_name is null limit 1) as family_name,
    (select top 1 tf.alternate_name from taxonomy_family) as alt_familyname,
    (select concat('<a href="taxonomyfamily.aspx?type=subfamily&id=', convert(taxonomy_family_id, char), '">', subfamily_name, '</a>') from taxonomy_family where subfamily_name = tf.subfamily_name and tribe_name is null and subtribe_name is null limit 1) as subfamily,
    (select concat('<a href="taxonomyfamily.aspx?type=tribe&id=', convert(taxonomy_family_id, char), '">',  tribe_name, '</a>') from taxonomy_family where tribe_name = tf.tribe_name and subtribe_name is null limit 1) as tribe,
    concat('<a href="taxonomyfamily.aspx?type=subtribe&id=', convert(tf.taxonomy_family_id, char), '">', subtribe_name, '</a>')  as subtribe,
    t.protologue,
    (select COUNT(a.accession_id) from accession a where a.taxonomy_species_id = t.taxonomy_species_id and a.is_web_visible = 'Y') as access_count,
    case when exists (select taxonomy_use_id from taxonomy_use where taxonomy_species_id in (select taxonomy_species_id from taxonomy_species where current_taxonomy_species_id = (select current_taxonomy_species_id from taxonomy_species where taxonomy_species_id = :taxonomyid)) and economic_usage_code in ('FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII')) then 'Y' else 'N' end as Nolocation,
    (select COUNT (a.accession_id) from accession a join accession_source acs on a.accession_id = acs.accession_id where a.taxonomy_species_id = t.taxonomy_species_id and acs.latitude is not null and acs.longitude is not null and a.is_web_visible = 'Y') as acc_mapcnt,    
    t.current_taxonomy_species_id,
    t.is_specific_hybrid,
    t.species_name,
    t.species_authority,
    t.is_subspecific_hybrid,
    t.subspecies_name,
    t.subspecies_authority,
    t.is_varietal_hybrid,
    t.variety_name,
    t.variety_authority,
    t.is_subvarietal_hybrid,
    t.subvariety_name,
    t.subvariety_authority,
    t.is_forma_hybrid,
    t.forma_rank_type,
    t.forma_name,
    t.forma_authority,
    t.taxonomy_genus_id,

    tcm.crop_id,
    c.name as crop_name,
    concat('<a href="site.aspx?id=',convert(s.site_id, char), '">', s.site_short_name, '</a>') as priority_site_1,
    s1.site_long_name as site_1_long,
    s2.site_short_name as priority_site_2,
    t.restriction_code,
    t.life_form_code,
    t.common_fertilization_code,
    t.is_name_pending,
    t.synonym_code,
    t.verifier_cooperator_id,
    t.name_verified_date,
    concat(coalesce(cop.first_name,''), ' ', coalesce(cop.last_name, '')) as verifier_name,
    t.note,
    t.site_note,
    t.alternate_name,
    t.created_date,
    t.created_by,
    t.modified_date,
    t.modified_by,
    t.owned_date,
    t.owned_by,
    case when t.is_specific_hybrid = 'Y' then 'images/checked.gif' else 'images/unchecked.gif' end as 

is_specific_hybrid_image,
    case when t.is_subspecific_hybrid = 'Y' then 'images/checked.gif' else 'images/unchecked.gif' end as 

is_subspecific_hybrid_image,
    case when t.is_varietal_hybrid = 'Y' then 'images/checked.gif' else 'images/unchecked.gif' end as 

is_varietal_hybrid_image,
    case when t.is_subvarietal_hybrid = 'Y' then 'images/checked.gif' else 'images/unchecked.gif' end as 

is_subvarietal_hybrid_image,
    case when t.is_forma_hybrid = 'Y' then 'images/checked.gif' else 'images/unchecked.gif' end as 

is_forma_hybrid_image,
    case when t.is_name_pending = 'Y' then 'images/checked.gif' else 'images/unchecked.gif' end as 

is_name_pending_image
from
    taxonomy_species t
    left join taxonomy_genus tg
        on t.taxonomy_genus_id = tg.taxonomy_genus_id
    left join taxonomy_family tf
	on tg.taxonomy_family_id = tf.taxonomy_family_id
    left join taxonomy_crop_map tcm
	on t.taxonomy_species_id = tcm.taxonomy_species_id
    left join crop c
        on tcm.crop_id = c.crop_id
	left join site s1 
		on t.priority1_site_id = s1.site_id
	left join site s2
		on t.priority2_site_id = s2.site_id
     left join cooperator cop
	on t.verifier_cooperator_id = cop.cooperator_id
where t.taxonomy_species_id = :taxonomyid