select 
    preference_value
from 
    web_user_preference
where 
    web_user_id = :userid
    and preference_name = :prefname
