select
     max(length(ctcl.description)) as maxLen
from
    crop_trait_observation cto
     left join crop_trait_code ctc
        on cto.crop_trait_code_id = ctc.crop_trait_code_id
    left join crop_trait_code_lang ctcl
        on ctc.crop_trait_code_id = ctcl.crop_trait_code_id
        and ctcl.sys_lang_id = :langid
where
    cto.numeric_value is null and
    cto.crop_trait_id in (:traitids) 
    and cto.is_archived = 'N'