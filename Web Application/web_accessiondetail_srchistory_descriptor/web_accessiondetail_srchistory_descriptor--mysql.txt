select distinct
	sdo.accession_source_id, 
	sd.source_descriptor_id,
	sd.coded_name as trait_short_name, 
	sd.category_code,
	COALESCE(sdl.title, coded_name) as trait_name,
    	case when sd.is_coded = 'Y' then 
        	COALESCE(sdcl.description, sdc.code)
    	when sdo.numeric_value is not null then 
        	convert(varchar, cast(sdo.numeric_value as float)) 
    	else 
        	sdo.string_value 
    	end 
    	as trait_value
from 
	source_desc_observation sdo
	left join source_descriptor sd 
        	on sdo.source_descriptor_id = sd.source_descriptor_id
    	left join source_descriptor_code sdc
        	on sdo.source_descriptor_code_id = sdc.source_descriptor_code_id
    	left join source_descriptor_code_lang sdcl 
       		on sdc.source_descriptor_code_id  = sdcl.source_descriptor_code_id 
        	and sdcl.sys_lang_id = __LANGUAGEID__
    	left join source_descriptor_lang sdl
		on sd.source_descriptor_id  = sdl.source_descriptor_id
		and sdl.sys_lang_id = __LANGUAGEID__
 where 
    sdo.accession_source_id = :accessionsrcid
 order by  
	category_code, trait_name, trait_value