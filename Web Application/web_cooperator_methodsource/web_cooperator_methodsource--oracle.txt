SELECT
	ROW_NUMBER() OVER (ORDER BY t.name) AS Row_Counter, 
	 concat('<nobr><a onclick="javascript:return true;" href="~/method.aspx?id=', cast(m.method_id as varchar2(50)), '">', m.name, '</a></nobr>') as method
from 
	method_map mm join method m 
	on mm.method_id = m.method_id
where  
	mm.cooperator_id in
		(select cooperator_id 
		from cooperator 
		where current_cooperator_id = 
			(select current_cooperator_id 
			from cooperator 
			where cooperator_id = :cooperatorid))
order by m.name  