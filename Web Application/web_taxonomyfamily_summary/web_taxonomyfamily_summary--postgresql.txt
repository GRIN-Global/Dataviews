select
    cast(tf.taxonomy_family_id as varchar(50)) as family_number,
    '<i>' || COALESCE (tf.family_name, '') || '</i> ' || COALESCE(tf.family_authority, '') as family_name,
    tf.family_name as family_short_name,
    COALESCE(tf.alternate_name, '') as altfamily,
    COALESCE(tf.subfamily_name, '') as subfamily,
    COALESCE(tf.tribe_name, '')  as tribe,
    COALESCE(tf.subtribe_name, '') as subtribe,
    (select COUNT (t.taxonomy_genus_id) from taxonomy_genus t where t.taxonomy_family_id = tf.taxonomy_family_id) as genera_count,
    (select '<i>' || genus_name || '</i> ' || genus_authority from taxonomy_genus where taxonomy_genus_id = tf.type_taxonomy_genus_id) as genus_type,
    tf.created_date, 
    tf.modified_date,
    tf.note,
    
    (select '<h2>Synonym of <a href=''taxonomyfamily.aspx?id=' || cast(t2.taxonomy_family_id as varchar(50)) || '''>' || t2.family_name || 
' ' || t2.family_authority || '</a></h2>' from taxonomy_family t2 where taxonomy_family_id = tf.current_taxonomy_family_id and taxonomy_family_id <> tf.taxonomy_family_id) as synonym_for_family

from
   taxonomy_family tf 
where tf.taxonomy_family_id = :taxonomyfamilyid
