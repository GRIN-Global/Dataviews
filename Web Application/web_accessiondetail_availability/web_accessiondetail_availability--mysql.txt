select
	1 as sortorder,
	a.accession_id,
	sum(case when i.is_distributable = 'Y' and i.is_available = 'Y' then coalesce(i.distribution_default_quantity, 1) else 0 end) as qty,
	case when i.is_distributable = 'Y' and i.is_available = 'Y' then 'Available'
	else 'Not Available' end as availability_status,
	i.distribution_default_quantity,
	(select top 1 coalesce(cvl.description, cv.value) from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id where cv.group_name = 'GERMPLASM_FORM' and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = i.distribution_default_form_code and i.is_distributable = 'Y' and i.is_available = 'Y') as distribution_type,
        select top 1 coalesce(cvl.description, cvl.title) from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id where cv.group_name = 'UNIT_OF_QUANTITY' and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = i.distribution_unit_code and i.is_distributable = 'Y' and i.is_available = 'Y') as distribution_unit
from
	accession a
	left join inventory i
	on a.accession_id = i.accession_id
where 
	a.accession_id = :accessionid
group by
	a.accession_id,
	i.is_distributable, 
	i.is_available,
	i.distribution_default_quantity,
	i.distribution_default_form_code,
	i.distribution_unit_code
having
	sum(case when i.is_distributable = 'Y' and i.is_available = 'Y' then coalesce(i.distribution_default_quantity, 1) else 0 end) > 0 

union

select
	2 as sortorder,
	:accessionid2,
	0,
	'Not Available',
	0,
	'', ''
order by 1 asc 