select ('<a href="taxonomygenus.aspx?id=' || cast(taxonomy_genus_id as varchar(50)) || '">' || 
	COALESCE (tg.genus_name, '') || '  ' ||  '</a><b>' || COALESCE (tg.genus_authority, '') || '</b>') as name
from taxonomy_genus tg  join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
and tg.taxonomy_family_id in (
select taxonomy_family_id from taxonomy_family where :columntype =  
(select :columntype from taxonomy_family where taxonomy_family_id = :taxonomyfamilyid)) 
order by tg.genus_name