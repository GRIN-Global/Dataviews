select 
	cvl1.title as quarantine_type,		
	cvl2.title as quarantine_status, 
	aq.custodial_cooperator_id as cooperator_id,
	aq.entered_date,
	aq.released_date, 
	aq.note 
from accession_quarantine aq   
	left join code_value cv1    
	    on aq.quarantine_type_code = cv1.value   
	left join code_value_lang cvl1    
	    on cv1.code_value_id = cvl1.code_value_id                 
	    and cvl1.sys_lang_id = __LANGUAGEID__ 
	    and cv1.group_name = 'ACCESSION_QUARANTINE_TYPE'  
	left join code_value cv2    
	    on aq.progress_status_code = cv2.value   
	left join code_value_lang cvl2    
	    on cv2.code_value_id = cvl2.code_value_id                 
	    and cvl2.sys_lang_id = __LANGUAGEID__ 
	    and cv2.group_name = 'ACCESSION_QUARANTINE_STATUS'  
where aq.accession_id = :accessionid   
limit 1