SELECT
  fi.feedback_inventory_id,
  fi.feedback_id,
  fi.inventory_id,
  coalesce(i.inventory_number_part1,'') || ' ' || coalesce(cast(i.inventory_number_part2 as varchar(50)),'') || ' ' || coalesce(i.inventory_number_part3,'') || ' ' || i.form_type_code as inventory_number,
  fi.created_date,
  fi.created_by,
  fi.modified_date,
  fi.modified_by,
  fi.owned_date,
  fi.owned_by
FROM
    feedback_inventory fi
	left join inventory i
	on fi.inventory_id = i.inventory_id
where fi.feedback_id = coalesce(:feedbackid, fi.feedback_id)
and fi.feedback_inventory_id = coalesce(:feedbackinventoryid, fi.feedback_inventory_id)
