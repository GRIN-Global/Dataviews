select 
	0 as gid, 
	'' as statename 
union 
select 
distinct 
	geography_id as gid, 
	adm1 as stateName 
from 
	geography 
where 
	country_code = :countrycode 
	and adm1 is not null and adm2 is null and adm3 is null and adm4 is null
order by stateName