SELECT
  c.cooperator_id,
  c.status_code,
  c.last_name,
  c.title,
  c.first_name,
  c.job,
  c.organization_abbrev,
  c.organization,
  c.address_line1,
  c.address_line2,
  c.address_line3,
  c.city,
  c.geography_id,
  c.postal_index,
  c.primary_phone,
  c.secondary_organization_abbrev,
  c.secondary_organization,
  c.secondary_address_line1,
  c.secondary_address_line2,
  c.secondary_address_line3,
  c.secondary_city,
  c.secondary_geography_id,
  c.secondary_postal_index,
  c.secondary_phone,
  c.fax,
  c.email,
  c.secondary_email,
  c.discipline_code,
  c.category_code,
  c.organization_region_code,
  c.note,
  c.sys_lang_id,
  c.site_id,
  c.current_cooperator_id,
  coalesce(g.adm1, '') as state,
  cvl.title as country
FROM
    cooperator AS c
    left join geography g
	on c.geography_id = g.geography_id
    left join code_value cv
	on g.country_code = cv.value 
	and cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
    left join code_value_lang cvl
        on cv.code_value_id = cvl.code_value_id 
  	and cvl.sys_lang_id = __LANGUAGEID__
WHERE
  c.cooperator_id = :cooperatorid
   