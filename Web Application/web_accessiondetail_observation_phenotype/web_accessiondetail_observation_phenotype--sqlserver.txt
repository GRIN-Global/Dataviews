select distinct
	a.accession_id, 
	ct.crop_trait_id, 
	ct.coded_name as trait_short_name, 
	ct.category_code,
	COALESCE(ctl.title, coded_name) as trait_name,
	COALESCE(ctl.description, ctl.title, coded_name) as trait_desc,
    	case when ct.is_coded = 'Y' then 
        	ctc.code + ' - ' + ctcl.description 
   	when cto.numeric_value is not null then 
        	convert(varchar, cast(cto.numeric_value as float)) 
    	else 
        	cto.string_value 
    	end as trait_value,
	m.name as method_name,
	m.materials_and_methods,
	m.method_id,
	trim(CONCAT (COALESCE (i.inventory_number_part1, ''), ' ', COALESCE (CONVERT(nvarchar, i.inventory_number_part2), ''), ' ', COALESCE (i.inventory_number_part3, ''), ' ', i.form_type_code)) as inventory_id,
	i.form_type_code
from 
   	accession a inner join inventory i 
        	on a.accession_id = i.accession_id 
    	left join crop_trait_observation cto 
        	on cto.inventory_id = i.inventory_id 
    	left join crop_trait ct 
        	on cto.crop_trait_id = ct.crop_trait_id 
    	left join crop_trait_code ctc 
        	on cto.crop_trait_code_id = ctc.crop_trait_code_id 
    	left join crop_trait_code_lang ctcl 
       		on ctc.crop_trait_code_id = ctcl.crop_trait_code_id 
        	and ctcl.sys_lang_id = __LANGUAGEID__
    	left join crop_trait_lang ctl
		on ct.crop_trait_id = ctl.crop_trait_id
		and ctl.sys_lang_id = __LANGUAGEID__
    	left join method m on cto.method_id = m.method_id
where 
    	a.accession_id = :accessionid 
    	and ct.crop_trait_id is not null  
    	and cto.is_archived = 'N'   
order by  
	category_code, trait_name, name, trait_value