select
	coalesce(inventory_number_part1,'') || ' ' || coalesce(cast(inventory_number_part2 as varchar2(50)),'') || ' ' || coalesce(inventory_number_part3,'') || ' ' || form_type_code as inventory_number,
	inventory_id
from
	inventory
where
	(coalesce(inventory_number_part1,'') || ' ' || coalesce(cast(inventory_number_part2 as varchar2(50)),'') || ' ' || coalesce(inventory_number_part3,'') || ' ' || form_type_code) like :inventorynumber