select
  a.accession_id,
  a.accession_number_part1,
  a.accession_number_part2,
  a.accession_number_part3,
  t.name  as taxonomy_name 
from
  accession a
  left join taxonomy_species t 
  on a.taxonomy_species_id = t.taxonomy_species_id
where
  a.accession_id in (:idlist)
order by
 1