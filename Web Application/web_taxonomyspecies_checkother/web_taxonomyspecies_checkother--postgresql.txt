select
'<a href="http://193.62.154.38/cgi-bin/nph-readbtree.pl/feout?FAMILY_XREF=&GENUS_XREF=' || tg.genus_name || '&SPECIES_XREF=' || t.species_name || 
'&TAXON_NAME_XREF=' || t.species_name || '" target="_blank"> Flora Europaea </a>'  as otherDBlink, 'Database of European Plants (ESFEDS)' as otherDB 
from taxonomy_genus tg join taxonomy_species t on tg.taxonomy_genus_id = t.taxonomy_genus_id
and t.taxonomy_species_id = :taxonomyid

Union

select
'<a href="http://mansfeld.ipk-gatersleben.de/pls/htmldb_pgrc/f?p=185:45:0::NO::P7_BOTNAME:' || tg.genus_name || '%20' || t.species_name  ||'"  
target="_blank"> Mansfeld </a>'  as otherDBlink, 'Mansfeld World Database of Agricultural and Horticultural Crops' as otherDB 
from taxonomy_genus tg join taxonomy_species t on tg.taxonomy_genus_id = t.taxonomy_genus_id
and t.taxonomy_species_id = :taxonomyid
