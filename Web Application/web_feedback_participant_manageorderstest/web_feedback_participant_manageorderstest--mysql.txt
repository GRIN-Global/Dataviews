SELECT
distinct
  oreq.order_request_id,
  oreq.ordered_date,
  (select COUNT(ori2.order_request_item_id) from order_request_item ori2 left join order_request oreq2 on oreq2.order_request_id = ori2.order_request_id where oreq2.requestor_cooperator_id = 5331 AND ori2.order_request_id = ori.order_request_id ) AS item_count,
  concat(fbr.title, '(', f.title, ')') AS report_name,
  frg.due_date,
 case 
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NULL then 'Submitted: Pending Review'
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NOT NULL then 'Accepted'
   when frg.started_date IS NOT NULL AND frg.accepted_date IS NULL then 'Started'
 else ''
 end as status
FROM
    order_request oreq
    left join order_request_item ori on ori.order_request_id = oreq.order_request_id
    left join feedback_inventory fbi on fbi.inventory_id = ori.inventory_id
    left join feedback f on f.feedback_id = fbi.feedback_id
    left join feedback_report fbr on fbr.feedback_id = f.feedback_id
    left join feedback_result_group frg on frg.feedback_report_id = fbr. feedback_report_id
    
 WHERE
 frg.participant_cooperator_id = :cooperatorid