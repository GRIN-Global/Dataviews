select 
    s.site_short_name as site, 
    c.email
from 
   web_order_request wor
   left join order_request o on wor.web_order_request_id = o.web_order_request_id
   left join cooperator c on o.owned_by = c.cooperator_id
   left join site s on c.site_id = s.site_id
where 
   wor.web_order_request_id = :wor