select distinct
frg.participant_cooperator_id,
fb.title AS program_title,
frpt.title,
frg.due_date,
frg.order_request_id,
frg.feedback_result_group_id

from feedback_result frslt
left join feedback_result_group frg
 on frslt.feedback_result_group_id = frg.feedback_result_group_id
	left join feedback_report frpt ON frpt.feedback_report_id = frg.feedback_report_id
	left join order_request ordr ON ordr.order_request_id = frg.order_request_id
	left join feedback fb ON fb.feedback_id = frpt.feedback_id

where frg.order_request_id = :order_request_id
