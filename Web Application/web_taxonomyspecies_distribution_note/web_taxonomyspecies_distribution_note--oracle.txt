select
   r.continent, 
   dbms_lob.substr(tgm.note,32767,1) AS note
FROM taxonomy_geography_map tgm
  left join geography g
    on tgm.geography_id = g.geography_id
  left join geography_region_map grm
    on g.geography_id = grm.geography_id
  left join region r
    on grm.region_id = r.region_id
where tgm.taxonomy_species_id = :taxonomyid
    and tgm.geography_status_code = :geostatuscode
group by r.continent, dbms_lob.substr(tgm.note,32767,1)