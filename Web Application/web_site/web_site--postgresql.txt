select  
	c.organization,
	
	s.site_long_name,
	
	c.address_line1,
	
	c.address_line2,
	
	c.address_line3,
	
	c.city || ', ' || g.adm1 || ' ' || (case when g.country_code = 'USA' then c.postal_index else g.country_code end)  as city,
	
	c.primary_phone,
	
	c.secondary_phone,
	
	c.fax,
	
	c.email,
	
	c.note

From 
	
	cooperator c
	
	join geography g on c.geography_id = g.geography_id
	
	join site s on c.organization_abbrev = s.site_short_name

Where
	s.site_id = :siteid
	and c.last_name  is null
	
	and c.status_code = 'ACTIVE'

union
select 
	'
<H2>Inactive germplasm</H2>
The record is maintained in the GRIN database for historical reasons.  The germplasm may have been duplicated by another accession or removed from the collection because it could not be maintained.  You may wish to contact the <A HREF=mailto:peo@ars-grin.gov>Plant Exchange Office</A> to determine if the germplasm is available from another source.
',
	Null,
Null,
Null,
Null,
Null,
Null,
Null,
Null,
Null,
Null
where 
	:siteid = -1