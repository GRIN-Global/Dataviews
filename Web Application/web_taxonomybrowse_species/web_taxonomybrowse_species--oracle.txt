select 
    t.taxonomy_species_id as value,
    t.name,
    t.name || ' (' || cast(count(distinct a.accession_id) as varchar2(50)) || ')' as display_text,
    COUNT(distinct a.accession_id) as accession_count,
    (select top 1 t2.name  from taxonomy_species t2 where taxonomy_species_id = t.current_taxonomy_species_id and t.synonym_code = 'S' and t2.taxonomy_species_id != t.taxonomy_species_id ) as synonym
from 
    taxonomy_species t :jointype join accession a
		on t.taxonomy_species_id = a.taxonomy_species_id
  :jointype join taxonomy_genus tg
    on t.taxonomy_genus_id = tg.taxonomy_genus_id
where
    t.taxonomy_genus_id = coalesce(:id, t.taxonomy_genus_id)
    and tg.genus_name like :genus and (t.species_name like :species or t.name like :species)
group by 
	t.taxonomy_species_id,
	t.name,
        t.current_taxonomy_species_id, 
        t.synonym_code
order by
    :orderby
    t.name