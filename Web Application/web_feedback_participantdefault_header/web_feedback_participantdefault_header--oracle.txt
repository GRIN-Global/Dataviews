select
concat(coalesce(c.first_name,''), ' ', coalesce(c.last_name,'')) as participant_name

from cooperator c

where c.cooperator_id = :cooperatorid