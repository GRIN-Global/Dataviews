select 
   	coop.cooperator_id,
   	coalesce(coop.last_name,'') as last_name,
    	coalesce(coop.first_name,'') as first_name,
    	coalesce(coop.organization,'') as organization 
from 
	method m 
   	join method_map mm 
		on m.method_id = mm.method_id
   	join cooperator coop 
		on mm.cooperator_id = coop.cooperator_id
where
	m.method_id = :methodid
