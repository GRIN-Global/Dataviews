SELECT
  i.inventory_id AS value_member
  ,LTRIM(RTRIM(LTRIM(COALESCE(i.inventory_number_part1, '') || ' ') || LTRIM(COALESCE(cast(i.inventory_number_part2 as varchar2(50)), '') || ' ') || LTRIM(COALESCE(i.inventory_number_part3, '') || ' ') || COALESCE((SELECT cvl.title FROM code_value_lang cvl JOIN code_value cv ON cvl.code_value_id = cv.code_value_id WHERE cv.value = i.form_type_code AND cvl.sys_lang_id = __LANGUAGEID__ AND cv.group_name= 'GERMPLASM_FORM'), ''))) AS display_member
  ,i.inventory_number_part1
  ,i.inventory_number_part2
  ,i.inventory_number_part3
  ,(SELECT cvl.title FROM code_value_lang cvl JOIN code_value cv ON cvl.code_value_id = cv.code_value_id WHERE cv.value = i.form_type_code AND cvl.sys_lang_id = __LANGUAGEID__ AND cv.group_name= 'GERMPLASM_FORM') AS inventory_type
FROM
  inventory i
WHERE
  LTRIM(RTRIM(LTRIM(COALESCE(i.inventory_number_part1, '') || ' ') || LTRIM(COALESCE(cast(i.inventory_number_part2 as varchar2(50)), '') || ' ') || LTRIM(COALESCE(i.inventory_number_part3, '') || ' ') || COALESCE((SELECT cvl.title FROM code_value_lang cvl JOIN code_value cv ON cvl.code_value_id = cv.code_value_id WHERE cv.value = i.form_type_code AND cvl.sys_lang_id = __LANGUAGEID__ AND cv.group_name= 'GERMPLASM_FORM'), ''))) like :likeinventory
