SELECT
  ct.crop_trait_id,
  c.name as crop_name, 
  ct.crop_id,
  coalesce(ctl.description, ctl.title, ct.coded_name) as descriptor_definition,
  concat(coalesce(ctl.title, ct.coded_name), ' (', ct.coded_name, ')') as descriptor_sname,
  coalesce(cvl_ct.title, ct.category_code) as category_name,
  case when ct.is_peer_reviewed = 'Y' then 'Peer Review Committee approved' else '' end  as approve_status,
  coalesce(cvl_dt.title, ct.data_type_code) as data_type,
  ct.data_type_code,
  ct.is_coded,
  ct.max_length as maximum_length,
  ct.numeric_format as data_format,
  ct.numeric_maximum,
  ct.numeric_minimum,
  ct.original_value_type_code,
  ct.original_value_format,
  ct.is_archived,
  ct.ontology_url,
  ct.note,
  s.site_id,
  s.site_short_name,
  s.site_long_name
FROM
    crop_trait ct
    join crop c on ct.crop_id = c.crop_id 
    left join crop_trait_lang ctl
		on ct.crop_trait_id = ctl.crop_trait_id
	left join code_value cv_ct
	on cv_ct.value = ct.category_code
         and cv_ct.group_name = 'DESCRIPTOR_CATEGORY'
    left join code_value_lang cvl_ct
        on cv_ct.code_value_id = cvl_ct.code_value_id
        and cvl_ct.sys_lang_id = :langid
	left join code_value cv_dt
	on cv_dt.value = ct.data_type_code
         and cv_dt.group_name = 'CROP_TRAIT_DATA_TYPE'
    left join code_value_lang cvl_dt
        on cv_dt.code_value_id = cvl_dt.code_value_id
        and cvl_dt.sys_lang_id = :langid
    join cooperator coop
	on ct.owned_by = coop.cooperator_id
    left join site s
	on coop.site_id = s.site_id
    where ct.crop_trait_id = :descriptorid
