select distinct ts.curator1_cooperator_id as cooperator_id
from taxonomy_species ts 
join taxonomy_crop_map tcm 
	on ts.taxonomy_species_id = tcm.taxonomy_species_id
join crop c
	on tcm.crop_id = c.crop_id and c.crop_id = :crop_id
where ts.curator1_cooperator_id is not null  

union

select distinct ts.curator2_cooperator_id as cooperator_id  
from taxonomy_species ts 
join taxonomy_crop_map tcm 
	on ts.taxonomy_species_id = tcm.taxonomy_species_id
join crop c
	on tcm.crop_id = c.crop_id and c.crop_id = :crop_id
where ts.curator2_cooperator_id is not null  