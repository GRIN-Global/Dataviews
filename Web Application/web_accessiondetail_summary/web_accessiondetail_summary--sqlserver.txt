select top 1
   (select (concat(coalesce(g.adm1, ''), ' ', COALESCE(cvl.title, g.country_code)))
	from geography g 
	    join accession_source asr on g.geography_id = asr.geography_id 
	    join code_value cv on cv.value = g.country_code and cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
	    left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = __LANGUAGEID__ 
	where accession_source_id = (select min(accession_source_id) from accession_source where accession_id = a.accession_id and is_origin = 'Y')) as developed_in,
    case when a.is_backed_up = 'Y' then ('<a href="site.aspx?id='+ convert(nvarchar, sb1.site_id)+ '">' + sb1.site_long_name + '</a>') else '' end as backup_location,
    (select cvl.title from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
	where cv.group_name = 'ACCESSION_LIFE_FORM'  and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = a.life_form_code) as life_form_code,
    (select cvl.title from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
	where cv.group_name = 'IMPROVEMENT_LEVEL'  and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = a.improvement_status_code) as improvement_status_code,
    (select cvl.title from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
	where cv.group_name = 'REPRODUCTIVE_UNIFORMITY'  and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = a.reproductive_uniformity_code) as reproductive_uniformity_code,
    a.initial_received_form_code,
    a.initial_received_date,
    case when a.status_code = 'INACTIVE' then '<a href="site.aspx?id=-1">Not in the NPGS Collection.</a>'
    	else '<a href="site.aspx?id='+ convert(nvarchar, s.site_id)+ '">' + s.site_long_name + '</a>' end as site_code,
    (select cvl.description from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
where cv.group_name = 'GERMPLASM_FORM'  and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = a.initial_received_form_code) as initial_material_type_desc,
    (select top 1 cast(quantity_on_hand as float) from inventory where accession_id = :accessionid AND is_distributable = 'Y') as 
inventory_volume,
    ap.description as pedigree,
    asrc.source_type_code,
    a.status_code
from
    accession a
    left join accession_source asrc
        on a.accession_id = asrc.accession_id
    left join geography g
        on asrc.geography_id = g.geography_id
    left join inventory i
		on a.accession_id = i.accession_id
    left join accession_pedigree ap
        on a.accession_id = ap.accession_id
	left join cooperator c on a.owned_by = c.cooperator_id
	left join site s on c.site_id = s.site_id
	left join site sb1
	on a.backup_location1_site_id = sb1.site_id
where
    a.accession_id = :accessionid
