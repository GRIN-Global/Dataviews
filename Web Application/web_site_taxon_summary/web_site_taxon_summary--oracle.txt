select  
	COUNT(a.accession_id) as a_cnt,  
	COUNT(distinct ts.taxonomy_genus_id) as g_cnt,
	COUNT(distinct ts.taxonomy_species_id) as t_cnt, 
	(select COUNT(distinct ts.taxonomy_species_id) 
	from accession a 
	join taxonomy_species ts on a.taxonomy_species_id = ts.taxonomy_species_id 
	join cooperator c on a.owned_by = c.cooperator_id
	where c.site_id = :siteid and ts.subspecies_name is null and ts.variety_name is null and subvariety_authority is null and forma_name is null) as s_cnt
 from 
	accession a 
	join taxonomy_species ts 
		on a.taxonomy_species_id = ts.taxonomy_species_id 
	join cooperator c 
		on a.owned_by = c.cooperator_id
where  
 	c.site_id = :siteid