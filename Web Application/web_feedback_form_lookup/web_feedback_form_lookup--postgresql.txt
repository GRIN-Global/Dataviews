SELECT
   feedback_form.feedback_form_id AS value_member,
   feedback_form.title AS display_member
FROM
   feedback_form
WHERE
(feedback_form.created_date > COALESCE(:createddate, '1753-01-01'))
   OR (feedback_form.modified_date > COALESCE(:modifieddate, '1753-01-01'))
   OR (feedback_form.feedback_form_id IN (:valuemember))
   OR (feedback_form.feedback_form_id BETWEEN :startpkey AND :stoppkey)
