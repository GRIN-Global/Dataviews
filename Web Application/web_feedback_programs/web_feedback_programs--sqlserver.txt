SELECT
  f.feedback_id,
  f.title,
  f.start_date,
  f.end_date,
  f.is_restricted_by_inventory,
  f.created_date,
  f.created_by,
  f.modified_date,
  f.modified_by,
  f.owned_date,
  f.owned_by
FROM
    feedback f
where f.feedback_id = coalesce(:feedbackid, f.feedback_id)

