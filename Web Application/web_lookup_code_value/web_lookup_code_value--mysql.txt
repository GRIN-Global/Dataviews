select 
	:first as Value, 
	:first as Text 
union 
select 
	cv.value as Value,  
	coalesce(cvl.title, cv.value) as Text 
from 
	code_value cv 
	left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
where 
	cv.group_name = :groupname
	and cvl.sys_lang_id = :languageid
order by value