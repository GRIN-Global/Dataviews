select 
    LTRIM(RTRIM(COALESCE(c.last_name, '') + ', ' + COALESCE(c.first_name, ''))) as name,
    aa.annotation_type_code as action_name, 
    aa.annotation_date as action_date,
    (case when t1.name IS NULL then ' ' else t1.name end) as old_name,
    (case when t2.name IS NULL then ' ' else t2.name end) as new_name
from  accession_inv_annotation aa 
left join inventory i
    on aa.inventory_id = i.inventory_id and i.form_type_code = '**'
left join taxonomy_species t1 on aa.old_taxonomy_species_id = t1.taxonomy_species_id  
left join taxonomy_species t2 on aa.new_taxonomy_species_id = t2.taxonomy_species_id 
left join cooperator c on aa.annotation_cooperator_id = c.cooperator_id
where i.accession_id = :accessionid