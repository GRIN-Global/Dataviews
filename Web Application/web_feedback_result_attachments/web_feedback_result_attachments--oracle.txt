SELECT
  fra.feedback_result_attach_id,
  fra.feedback_result_id,
  fra.virtual_path,
  fra.thumbnail_virtual_path,
  fra.sort_order,
  fra.title,
  fra.description,
  fra.content_type,
  fra.category_code,
  fra.is_web_visible,
  fra.note,
  fra.created_date,
  fra.created_by,
  fra.modified_date,
  fra.modified_by,
  fra.owned_date,
  fra.owned_by 
FROM
    feedback_result_attach fra

where fra.feedback_result_id = :feedbackresultid and fra.feedback_result_attach_id = coalesce(:feedbackresultattachid, fra.feedback_result_attach_id)
