select 
	tf.taxonomy_family_id as value,
	tf.family_name,
	tf.family_name + ' ' + coalesce(tf.subfamily_name,'') + ' ' + coalesce(tf.tribe_name,'') + ' ' + coalesce(tf.subtribe_name,'') + ' (' +  replace(convert(nvarchar, convert(money, count(distinct a.accession_id)), 1), '.00', '') + ')' as display_text,
    COUNT(distinct a.accession_id) as accession_count
from 
	taxonomy_family tf :jointype join taxonomy_genus tg
		on tf.taxonomy_family_id = tg.taxonomy_family_id
	:jointype join taxonomy_species t
		on tg.taxonomy_genus_id = t.taxonomy_genus_id
	:jointype join accession a
		on t.taxonomy_species_id = a.taxonomy_species_id
where
  tf.taxonomy_family_id = coalesce(:id, tf.taxonomy_family_id)
  and tg.genus_name like :genus and (t.species_name like :species or t.name like :species)
group by 
    tf.taxonomy_family_id,
    tf.family_name,
    tf.subfamily_name,
    tf.tribe_name,
    tf.subtribe_name
order by
    :orderby
    tf.family_name + ' ' + coalesce(tf.subfamily_name,'') + ' ' + coalesce(tf.tribe_name,'') + ' ' + coalesce(tf.subtribe_name,'')
