select
    dv.dataview_name,
    coalesce(dvl.title, dv.dataview_name, '') as title
from
    sys_dataview dv left join sys_dataview_lang dvl
        on dv.sys_dataview_id = dvl.sys_dataview_id
        and dvl.sys_lang_id = :langid
where
    dv.category_code = 'Web Application'
    and dv.database_area_code = 'Search'
order by
    dv.database_area_code_sort_order, 2 