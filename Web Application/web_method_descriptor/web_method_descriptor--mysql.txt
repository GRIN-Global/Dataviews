select 
	cto.crop_trait_id, 
	ct.coded_name, 
	cto.method_id,
   	(select 
		count(a.accession_id) 
   	from 
		accession a
   	join inventory i 
   		on a.accession_id = i.accession_id 
   	and i.inventory_id in 
   		(select 
			inventory_id 
   		from 
			crop_trait_observation
   		where 
			crop_trait_id = cto.crop_trait_id
   		and method_id = :methodid)) as cnt 
   from 
	crop_trait_observation cto 
   join crop_trait ct
   	on cto.crop_trait_id = ct.crop_trait_id
   group by 
	cto.crop_trait_id, ct.coded_name, cto.method_id  
   having  
	cto.method_id = :methodid 
   order by 
	ct.coded_name asc
 
