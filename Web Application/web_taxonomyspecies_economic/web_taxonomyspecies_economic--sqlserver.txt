select 
	coalesce(cvl1.title, tu.economic_usage_code) as economic_usage,	
	tu.usage_type,
	tu.note
 from taxonomy_use tu
  	left join code_value cv1 
		on tu.economic_usage_code = cv1.value
	left join code_value_lang cvl1 
		on cv1.code_value_id = cvl1.code_value_id
where cv1.group_name = 'TAXONOMY_USAGE' and cvl1.sys_lang_id = __LANGUAGEID__ 
and tu.taxonomy_species_id = :taxonomyid


