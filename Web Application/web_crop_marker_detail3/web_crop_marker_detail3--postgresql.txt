select 
	concat('<a href="method.aspx?id=',cast(m.method_id as varchar(50)), '">', m.name, '</a>') as Method, 
	m.materials_and_methods as Evaluation_Methods, 
	ga.assay_method as Assay_Method, 
	ga.scoring_method as Scoring_Method, 
	ga.control_values as Control_Values, 
	ga.observation_alleles_count as Observation_Alleles_Count, 
	ga.max_gob_alleles as Max_Gob_Alleles, 
	ga.size_alleles as Size_of_Alleles, 
	ga.unusual_alleles as Unusual_Alleles, 
	ga.note as Comment
from 
	genetic_annotation ga 
	join method m on ga.method_id = m.method_id
where 
	ga.genetic_marker_id = :markerid