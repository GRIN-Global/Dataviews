select 
	COUNT(distinct tf.family_name) as family,
	0 as genus,
	0 as species,
	0 as accession
	from taxonomy_family tf 
	join taxonomy_genus tg on tf.taxonomy_family_id = tg.taxonomy_family_id
	join taxonomy_species ts on tg.taxonomy_genus_id = ts.taxonomy_genus_id 
    where tf.subfamily_name is null and tf.tribe_name is null and tf.subtribe_name is null
    and ts.taxonomy_species_id in (select taxonomy_species_id from accession)
  
Union

select 
	0 as family, 
	COUNT(distinct tg.genus_name) as genus,
	0 as species,
	0 as accession
	from taxonomy_genus tg 
	join taxonomy_species ts on tg.taxonomy_genus_id = ts.taxonomy_genus_id 
    where tg.subgenus_name is null and tg.section_name is null and tg.subsection_name is null and tg.series_name is null and tg.subseries_name is null and tg.qualifying_code is null
    and ts.taxonomy_species_id in (select taxonomy_species_id from accession) 

Union

select 
	0 as family, 
	0 as genus, 
	COUNT(distinct ts.taxonomy_species_id) as species,
	0 as accession
	from taxonomy_species ts where ts.taxonomy_species_id in (select taxonomy_species_id from accession)  
	
Union

select 
	0 as family, 
	0 as genus, 
	0 as species,
	COUNT(accession_id) as accession from accession