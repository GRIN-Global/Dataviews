select 
	l.abbreviation as Reference_Abbreviation, 
	c.citation_title as Citation_Title, 
	c.author_name as Author_of_Publication, 
	c.citation_year as Citation_Year,
	c.reference as Citation_Reference, 
	l.standard_abbreviation as Standard_Abbreviation, 
	l.reference_title as Reference_Title
from 
	citation c 
	join literature l on c.literature_id = l.literature_id
where 
	c.genetic_marker_id = :markerid