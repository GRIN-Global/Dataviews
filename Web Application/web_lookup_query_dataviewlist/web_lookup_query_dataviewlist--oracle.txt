select
    dv.dataview_name,
    coalesce(dvl.title, dv.dataview_name, '') as title,
    dv.configuration_options
from
    sys_dataview dv left join sys_dataview_lang dvl
        on dv.sys_dataview_id = dvl.sys_dataview_id
        and dvl.sys_lang_id = :langid
where
    dv.category_code = 'Web Reports'
    and dv.dataview_name like :dvName
order by
    dv.dataview_name 