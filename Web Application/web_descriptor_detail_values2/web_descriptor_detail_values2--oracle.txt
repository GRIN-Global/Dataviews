SELECT
	string_value as Value, 
	concat('<a href="descriptoraccession.aspx?id1=', cast(crop_trait_id as varchar2(50)), '&id2=', string_value, '&type=2">', cast(COUNT(inventory_id) as varchar2(50)), '</a>') as Number_of_Accessions
from 
	crop_trait_observation cto
where 
	crop_trait_id = :descriptorid 
group by 
	crop_trait_id, string_value
order by 
	string_value
    