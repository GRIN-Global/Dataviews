select distinct
frg.participant_cooperator_id,
frg.feedback_result_group_id,
frpt.title,
fb.title AS program_name,
to_char(fb.start_date, 'YYYY') AS report_year,
frg.order_request_id,
frg.due_date,
 case 
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NULL then 'Submitted: Pending Review'
   when frg.submitted_date IS NOT NULL AND frg.accepted_date IS NOT NULL then 'Accepted'
   when frg.started_date IS NOT NULL AND frg.accepted_date IS NULL then 'Started'
 else ''
 end as status
 
from feedback_result_group frg 
	left join feedback_report frpt ON frpt.feedback_report_id = frg.feedback_report_id
	left join order_request ordr ON ordr.order_request_id = frg.order_request_id
	left join feedback fb ON fb.feedback_id = frpt.feedback_id

where frg.participant_cooperator_id = :cooperatorid
order by frg.due_date asc