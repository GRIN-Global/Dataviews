select
  ia.accession_inv_attach_id 
  ,COALESCE (ia.virtual_path, '') as virtual_path
  ,COALESCE (ia.thumbnail_virtual_path, ia.virtual_path, '') as thumbnail_virtual_path
  ,concat(cast(ia.accession_inv_attach_id as varchar(50)), ', ', COALESCE (ia.title, ''), ', ', COALESCE (co.last_name, ''), ', ', COALESCE (co.first_name, ''), ', ', COALESCE (co.organization, '')) as title 
  ,COALESCE (ia.description, '') as description
from
  accession_inv_attach ia 
	join inventory i 
		on ia.inventory_id = i.inventory_id
	left join cooperator co
		on ia.attach_cooperator_id = co.cooperator_id
where
  i.accession_id = :accessionid
  and ia.is_web_visible = 'Y'
  and ia.category_code = 'IMAGE'
  order by sort_order