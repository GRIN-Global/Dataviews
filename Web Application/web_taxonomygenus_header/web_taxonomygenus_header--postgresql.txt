select 
('Accessions in: ' || tg.genus_name || ' ' || tg.genus_authority) as genus_name_auth
from taxonomy_genus tg
where tg.taxonomy_genus_id = :taxonomygenusid