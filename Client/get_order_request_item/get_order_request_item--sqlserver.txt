SELECT
  oi.order_request_item_id,
  oi.order_request_id,
  oi.sequence_number,
  i.accession_id,
  oi.inventory_id,
  oi.name,
  CASE WHEN (SELECT MIN(plant_name_rank) FROM accession_inv_name n0 WHERE n0.inventory_id = i.inventory_id) IS NOT NULL
    THEN (select TOP 1 plant_name from accession_inv_name an where i.inventory_id = an.inventory_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 where i.inventory_id = an2.inventory_id))
    ELSE (SELECT TOP 1 plant_name FROM accession_inv_name an INNER JOIN inventory i2 ON i2.inventory_id = an.inventory_id
          WHERE i.accession_id = i2.accession_id and an.plant_name_rank =
            (SELECT MIN(plant_name_rank) FROM accession_inv_name n0
              INNER JOIN inventory i0 ON i0.inventory_id = n0.inventory_id AND i0.accession_id = i.accession_id))
    END AS plant_name,
  oi.external_taxonomy,
  a.taxonomy_species_id,
  i.quantity_on_hand,
  i.quantity_on_hand_unit_code,
  oi.quantity_shipped,
  oi.quantity_shipped_unit_code,
  oi.distribution_form_code,
  i.availability_status_code,
  oi.status_code,
  oi.status_date,
  oi.note,
  oi.source_cooperator_id,
  (select c.geography_id from cooperator c where oi.source_cooperator_id = c.cooperator_id)as geography_id,
  i.is_distributable,
  i.distribution_default_form_code,
  i.distribution_default_quantity,
  i.distribution_unit_code,
  i.storage_location_part1,
  i.storage_location_part2,
  i.storage_location_part3,
  i.storage_location_part4,
  oi.created_date,
  oi.created_by,
  oi.modified_date,
  oi.modified_by,
  oi.owned_date,
  oi.owned_by
FROM
    order_request_item oi
    INNER JOIN inventory i ON oi.inventory_id = i.inventory_id
    INNER JOIN accession a ON i.accession_id = a.accession_id
    INNER JOIN (
        SELECT order_request_item_id FROM order_request_item
            WHERE order_request_item_id IN (:orderrequestitemid)

        UNION SELECT order_request_item_id FROM order_request_item
            WHERE order_request_id IN (:orderrequestid)

        UNION SELECT order_request_item_id FROM order_request_item
            WHERE web_order_request_item_id IN (:weborderrequestitemid)

        UNION SELECT order_request_item_id FROM order_request_item
            WHERE inventory_id IN (:inventoryid)

        UNION SELECT order_request_item_id FROM order_request_item
            WHERE source_cooperator_id IN (:cooperatorid)

	UNION SELECT ori.order_request_item_id FROM order_request_item ori
            INNER JOIN inventory i ON i.inventory_id = ori.inventory_id
            WHERE i.accession_id IN (:accessionid)

	UNION SELECT ori.order_request_item_id FROM order_request_item ori
            INNER JOIN inventory i ON i.inventory_id = ori.inventory_id
            WHERE i.owned_by in (:ownedby)

	UNION SELECT ori.order_request_item_id FROM order_request_item ori
            INNER JOIN order_request o ON o.order_request_id = ori.order_request_id
            WHERE o.web_order_request_id IN (:weborderrequestid)
    ) list ON oi.order_request_item_id = list.order_request_item_id