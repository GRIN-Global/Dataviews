select
     distinct(o.order_request_id)
    ,o.ordered_date
    ,o.web_order_request_id 
    ,o.original_order_request_id
    ,o.local_number
    ,(select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = o.owned_by)) AS owner_site_id    
    ,o.order_type_code
    ,o.intended_use_code
    ,o.intended_use_note
    ,o.completed_date
    ,(select count(*) from order_request_item ori where ori.order_request_id = o.order_request_id) as items
    ,o.final_recipient_cooperator_id
    ,o.requestor_cooperator_id
    ,o.ship_to_cooperator_id
    ,o.order_obtained_via
    ,o.special_instruction
    ,o.note
    ,o.created_date
    ,o.created_by
    ,o.modified_date
    ,o.modified_by
    ,o.owned_date
    ,o.owned_by
from
    order_request o
where
    o.order_request_id in (:orderrequestid)

union

select
     distinct(o.order_request_id)
    ,o.ordered_date
    ,o.web_order_request_id
    ,o.original_order_request_id
    ,o.local_number
    ,(select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = o.owned_by)) AS owner_site_id    
    ,o.order_type_code
    ,o.intended_use_code
    ,o.intended_use_note
    ,o.completed_date
    ,(select count(*) from order_request_item ori where ori.order_request_id = o.order_request_id) as items
    ,o.final_recipient_cooperator_id
    ,o.requestor_cooperator_id
    ,o.ship_to_cooperator_id
    ,o.order_obtained_via
    ,o.special_instruction
    ,o.note
    ,o.created_date
    ,o.created_by
    ,o.modified_date
    ,o.modified_by
    ,o.owned_date
    ,o.owned_by
from
    order_request o, order_request_item oi, inventory i
where
    o.order_request_id = oi.order_request_id
    and oi.inventory_id = i.inventory_id
    and i.accession_id in (:accessionid)

union

select
     distinct(o.order_request_id)
    ,o.ordered_date
    ,o.web_order_request_id
    ,o.original_order_request_id
    ,o.local_number
    ,(select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = o.owned_by)) AS owner_site_id    
    ,o.order_type_code
    ,o.intended_use_code
    ,o.intended_use_note
    ,o.completed_date
    ,(select count(*) from order_request_item oi where oi.order_request_id = o.order_request_id) as items
    ,o.final_recipient_cooperator_id
    ,o.requestor_cooperator_id
    ,o.ship_to_cooperator_id
    ,o.order_obtained_via
    ,o.special_instruction
    ,o.note
    ,o.created_date
    ,o.created_by
    ,o.modified_date
    ,o.modified_by
    ,o.owned_date
    ,o.owned_by
from
    order_request o, order_request_item oi
where
    o.order_request_id = oi.order_request_id
    and oi.inventory_id in (:inventoryid)

union

select
     distinct(o.order_request_id)
    ,o.ordered_date
    ,o.web_order_request_id
    ,o.original_order_request_id
    ,o.local_number
    ,(select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = o.owned_by)) AS owner_site_id    
    ,o.order_type_code
    ,o.intended_use_code
    ,o.intended_use_note
    ,o.completed_date
    ,(select count(*) from order_request_item oi where oi.order_request_id = o.order_request_id) as items
    ,o.final_recipient_cooperator_id
    ,o.requestor_cooperator_id
    ,o.ship_to_cooperator_id
    ,o.order_obtained_via
    ,o.special_instruction
    ,o.note
    ,o.created_date
    ,o.created_by
    ,o.modified_date
    ,o.modified_by
    ,o.owned_date
    ,o.owned_by
from
  order_request o
where
  o.final_recipient_cooperator_id in (:cooperatorid)

UNION

select
     distinct(o.order_request_id)
    ,o.ordered_date
    ,o.web_order_request_id
    ,o.original_order_request_id
    ,o.local_number
    ,(select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = o.owned_by)) AS owner_site_id    
    ,o.order_type_code
    ,o.intended_use_code
    ,o.intended_use_note
    ,o.completed_date
    ,(select count(*) from order_request_item oi where oi.order_request_id = o.order_request_id) as items
    ,o.final_recipient_cooperator_id
    ,o.requestor_cooperator_id
    ,o.ship_to_cooperator_id
    ,o.order_obtained_via
    ,o.special_instruction
    ,o.note
    ,o.created_date
    ,o.created_by
    ,o.modified_date
    ,o.modified_by
    ,o.owned_date
    ,o.owned_by
from
    order_request o, order_request_item oi, inventory i
where
    o.order_request_id = oi.order_request_id
    and oi.inventory_id = i.inventory_id
    and i.owned_by in (:ownedby)
    and oi.status_code in (:orderitemstatus)

