SELECT
  distinct a.accession_id,
  a.accession_number_part1,
  a.accession_number_part2,
  a.accession_number_part3,
  a.taxonomy_species_id,
  (select min(plant_name) from accession_inv_name an, inventory i where 
    i.inventory_id = an.inventory_id and i.accession_id = a.accession_id and
    plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2, inventory i2 where an2.inventory_id = i2.inventory_id and i2.accession_id = a.accession_id)) AS plant_name,
  (select s.geography_id from accession_source s where a.accession_id = s.accession_id and s.accession_source_id = (select MIN(s2.accession_source_id) from accession_source s2 where a.accession_id = s2.accession_id and is_origin = 'Y')) AS geography_id,
  (select s.site_id from site s where s.site_id in (select c.site_id from cooperator c where c.cooperator_id = a.owned_by)) AS owner_site_id,
  a.is_core,
  a.is_backed_up,
  a.backup_location1_site_id,
  a.backup_location2_site_id,
  a.status_code,
  a.life_form_code,
  a.improvement_status_code,
  a.reproductive_uniformity_code,
  a.initial_received_form_code,
  a.initial_received_date_code,
  a.initial_received_date,
  a.is_web_visible,
  a.note,
  (SELECT accession_id FROM accession WHERE accession_id = a.accession_id) AS accession_id_string,
  a.created_date,
  a.created_by,
  a.modified_date,
  a.modified_by,
  a.owned_date,
  a.owned_by
FROM
    accession a
    INNER JOIN (
        SELECT accession_id FROM accession WHERE accession_id IN (:accessionid)

        UNION SELECT accession_id FROM accession
            WHERE taxonomy_species_id IN (:taxonomyspeciesid)

        UNION SELECT accession_id FROM inventory
            WHERE inventory_id IN (:inventoryid)

        UNION SELECT accession_id FROM inventory i
            INNER JOIN order_request_item ori ON ori.inventory_id = i.inventory_id
            WHERE ori.order_request_id IN (:orderrequestid)

        UNION SELECT accession_id FROM accession_source
            WHERE geography_id IN (:geographyid)

        UNION SELECT accession_id FROM accession_source asrc
            INNER JOIN accession_source_map asm ON asrc.accession_source_id = asm.accession_source_id
            WHERE asm.cooperator_id IN (:cooperatorid)

        UNION SELECT accession_id FROM accession a
            INNER JOIN taxonomy_species t ON a.taxonomy_species_id = t.taxonomy_species_id
            WHERE t.taxonomy_genus_id in (:taxonomygenusid)

    ) list ON list.accession_id = a.accession_id