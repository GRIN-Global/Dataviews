SELECT
  aact.accession_action_id,
  aact.accession_id,
  aact.action_name_code,
  aact.started_date_code,
  aact.started_date,
  aact.completed_date_code,
  aact.completed_date,
  aact.is_web_visible,
  aact.cooperator_id,
  aact.method_id,
  aact.note,
  (SELECT taxonomy_species_id FROM accession acc WHERE (aact.accession_id = acc.accession_id)) AS taxonomy_species_id,
  aact.created_date,
  aact.created_by,
  aact.modified_date,
  aact.modified_by,
  aact.owned_date,
  aact.owned_by
FROM
    accession_action AS aact
    INNER JOIN (
	SELECT accession_action_id FROM accession_action
            WHERE accession_action_id in (:accessionactionid)

	UNION SELECT accession_action_id FROM accession_action
            WHERE accession_id in (:accessionid)

	UNION SELECT accession_action_id FROM accession_action
            WHERE cooperator_id in (:cooperatorid)

	UNION SELECT accession_action_id FROM accession_action
            WHERE method_id in (:methodid)

	UNION SELECT accession_action_id FROM accession_action
            INNER JOIN inventory ON accession_action.accession_id = inventory.accession_id
            WHERE inventory.inventory_id in (:inventoryid)

	UNION SELECT accession_action_id FROM accession_action
            INNER JOIN inventory ON accession_action.accession_id = inventory.accession_id
            INNER JOIN order_request_item ON inventory.inventory_id = order_request_item.inventory_id
            WHERE order_request_item.order_request_id in (:orderrequestid)

    ) list ON aact.accession_action_id = list.accession_action_id