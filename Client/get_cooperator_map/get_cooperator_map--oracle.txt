SELECT
  cm.cooperator_map_id,
  cm.cooperator_id,
  cm.cooperator_group_id,
  dbms_lob.substr(cm.note,32767,1) as note,
  cm.created_date,
  cm.created_by,
  cm.modified_date,
  cm.modified_by,
  cm.owned_date,
  cm.owned_by
FROM
    cooperator_map AS cm
WHERE
    cm.cooperator_map_id in (:cooperatormapid)

 UNION
SELECT
  cm.cooperator_map_id,
  cm.cooperator_id,
  cm.cooperator_group_id,
  dbms_lob.substr(cm.note,32767,1) as note,
  cm.created_date,
  cm.created_by,
  cm.modified_date,
  cm.modified_by,
  cm.owned_date,
  cm.owned_by
FROM
    cooperator_map cm
WHERE
    cm.cooperator_id in (:cooperatorid)
 UNION
SELECT
  cm.cooperator_map_id,
  cm.cooperator_id,
  cm.cooperator_group_id,
  dbms_lob.substr(cm.note,32767,1) AS note,
  cm.created_date,
  cm.created_by,
  cm.modified_date,
  cm.modified_by,
  cm.owned_date,
  cm.owned_by
FROM
    cooperator_map cm, accession_source_map asm, accession_source asrc
WHERE
    cm.cooperator_id = asm.cooperator_id and
    asm.accession_source_id = asrc.accession_source_id and
    asrc.accession_id in (:accessionid)
 UNION
SELECT
  cm.cooperator_map_id,
  cm.cooperator_id,
  cm.cooperator_group_id,
  dbms_lob.substr(cm.note,32767,1) AS note,
  cm.created_date,
  cm.created_by,
  cm.modified_date,
  cm.modified_by,
  cm.owned_date,
  cm.owned_by
FROM
    cooperator_map cm, accession_source_map asm, accession_source asrc, inventory i
WHERE
    cm.cooperator_id = asm.cooperator_id and
    asm.accession_source_id = asrc.accession_source_id and
    asrc.accession_id = i.accession_id and
    i.inventory_id in (:inventoryid)
 UNION
SELECT
  cm.cooperator_map_id,
  cm.cooperator_id,
  cm.cooperator_group_id,
  dbms_lob.substr(cm.note,32767,1) AS note,
  cm.created_date,
  cm.created_by,
  cm.modified_date,
  cm.modified_by,
  cm.owned_date,
  cm.owned_by
FROM
    cooperator_map cm, order_request ord
WHERE
    cm.cooperator_id = ord.final_recipient_cooperator_id and
    ord.order_request_id in (:orderrequestid)