SELECT
  ctca.crop_trait_code_attach_id,
  ct.crop_id,
  ctc.crop_trait_id,
  ctca.crop_trait_code_id,
  ctca.virtual_path,
  ctca.thumbnail_virtual_path,
  ctca.sort_order,
  ctca.title,
  ctca.description,
  ctca.content_type,
  ctca.category_code,
  ctca.is_web_visible,
  ctca.attach_cooperator_id,
  ctca.attach_date_code,
  ctca.attach_date,
  ctca.note,
  ctca.created_date,
  ctca.created_by,
  ctca.modified_date,
  ctca.modified_by,
  ctca.owned_date,
  ctca.owned_by
FROM
    crop_trait_code_attach ctca
    INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctca.crop_trait_code_id
    INNER JOIN crop_trait      ct  ON ct.crop_trait_id = ctc.crop_trait_id
    INNER JOIN (
        SELECT crop_trait_code_attach_id FROM crop_trait_code_attach
            WHERE crop_trait_code_attach_id IN (:croptraitcodeattachid)

        UNION SELECT crop_trait_code_attach_id FROM crop_trait_code_attach
            WHERE crop_trait_code_id in (:croptraitcodeid)

        UNION SELECT ctca.crop_trait_code_attach_id FROM crop_trait_code_attach ctca
            INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctca.crop_trait_code_id
            WHERE ctc.crop_trait_id IN (:croptraitid)

        UNION SELECT ctca.crop_trait_code_attach_id FROM crop_trait_code_attach ctca
            INNER JOIN crop_trait_code ctc ON ctc.crop_trait_code_id = ctca.crop_trait_code_id
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctc.crop_trait_id
            WHERE ct.crop_id IN (:cropid)

        UNION SELECT ctca.crop_trait_code_attach_id FROM crop_trait_code_attach ctca
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctca.crop_trait_code_id
            WHERE cto.crop_trait_observation_id in (:croptraitobservationid)

        UNION SELECT ctca.crop_trait_code_attach_id FROM crop_trait_code_attach ctca
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctca.crop_trait_code_id
            WHERE cto.inventory_id in (:inventoryid)

        UNION SELECT ctca.crop_trait_code_attach_id FROM crop_trait_code_attach ctca
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctca.crop_trait_code_id
            INNER JOIN inventory i ON i.inventory_id = cto.inventory_id
            WHERE i.accession_id in (:accessionid)

        UNION SELECT ctca.crop_trait_code_attach_id FROM crop_trait_code_attach ctca
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctca.crop_trait_code_id
            INNER JOIN order_request_item ori ON ori.inventory_id = cto.inventory_id
            WHERE ori.order_request_id in (:orderrequestid)

    ) list ON ctca.crop_trait_code_attach_id = list.crop_trait_code_attach_id