SELECT
  cto.crop_trait_observation_id,
  i.accession_id,
  cto.inventory_id,
  ct.crop_id,
  cto.crop_trait_id,
  cto.crop_trait_code_id,
  (select ctc.code from crop_trait_code ctc where ctc.crop_trait_code_id = cto.crop_trait_code_id) AS code,  
  cto.numeric_value,
  cto.string_value,
  cto.method_id,
  cto.is_archived,
  cto.data_quality_code,
  cto.original_value,
  cto.frequency,
  cto.rank,
  cto.mean_value,
  cto.maximum_value,
  cto.minimum_value,
  cto.standard_deviation,
  cto.sample_size,
  cto.note,
  (SELECT taxonomy_species_id FROM accession WHERE accession_id = i.accession_id) AS taxonomy_species_id,
  cto.created_date,
  cto.created_by,
  cto.modified_date,
  cto.modified_by,
  cto.owned_date,
  cto.owned_by
FROM
    crop_trait_observation cto
    INNER JOIN inventory i ON  i.inventory_id = cto.inventory_id 
    INNER JOIN crop_trait ct ON  ct.crop_trait_id = cto.crop_trait_id 
    INNER JOIN (
        SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE crop_trait_observation_id IN (:croptraitobservationid)

        UNION SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE inventory_id in (:inventoryid)

        UNION SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE crop_trait_id in (:croptraitid)

        UNION SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE crop_trait_code_id in (:croptraitcodeid)

        UNION SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE method_id in (:methodid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN inventory i ON i.inventory_id = cto.inventory_id
            WHERE i.accession_id in (:accessionid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN order_request_item AS ori ON ori.inventory_id = cto.inventory_id
            WHERE ori.order_request_id IN (:orderrequestid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait ct ON ct.crop_trait_id = cto.crop_trait_id
            WHERE ct.crop_id IN (:cropid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait_code_attach ctca ON ctca.crop_trait_code_id = cto.crop_trait_code_id
            WHERE ctca.crop_trait_code_attach_id IN (:croptraitcodeattachid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait_attach cta ON cta.crop_trait_id = cto.crop_trait_id
            WHERE cta.crop_trait_attach_id IN (:croptraitattachid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait ct ON ct.crop_trait_id = cto.crop_trait_id
            INNER JOIN crop_attach ca ON ca.crop_id = ct.crop_id
            WHERE ca.crop_attach_id IN (:cropattachid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait_code_lang ctcl ON ctcl.crop_trait_code_id = cto.crop_trait_code_id
            WHERE ctcl.crop_trait_code_lang_id IN (:croptraitcodelangid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait_lang ctl ON ctl.crop_trait_id = cto.crop_trait_id
            WHERE ctl.crop_trait_lang_id IN (:croptraitlangid)

        UNION SELECT ctod.crop_trait_observation_id FROM crop_trait_observation_data ctod
            WHERE ctod.crop_trait_observation_data_id IN (:croptraitobservationdataid)
    ) list ON cto.crop_trait_observation_id = list.crop_trait_observation_id