SELECT 
       via.inventory_viability_id,
       via.inventory_viability_rule_id,
       via.inventory_id,
       via.tested_date_code,
       via.tested_date,
       via.percent_normal,
       via.percent_abnormal,
       via.percent_dormant,
       via.percent_viable,
       via.vigor_rating_code,
       via.total_tested_count,
       via.replication_count,
       via.percent_empty,
       via.percent_infested,
       via.percent_dead,
       via.percent_unknown,
       dbms_lob.substr(via.note,32767,1) AS note,
       via.created_date,
       via.created_by,
       via.modified_date,
       via.modified_by,
       via.owned_date,
       via.owned_by
FROM   inventory_viability via
	INNER JOIN (
		SELECT inventory_viability_id
			FROM inventory_viability WHERE inventory_viability_id IN (:inventoryviabilityid)

		UNION SELECT inventory_viability_id
			FROM inventory_viability via WHERE inventory_id IN (:inventoryid)

		UNION SELECT inventory_viability_id
			FROM inventory_viability via WHERE inventory_viability_rule_id IN (:inventoryviabilityruleid)

		UNION SELECT inventory_viability_id
			FROM inventory_viability via
			INNER JOIN inventory i ON i.inventory_id = via.inventory_id
			WHERE i.accession_id IN (:accessionid)

		UNION SELECT inventory_viability_id
			FROM inventory_viability via
			INNER JOIN  order_request_item ori ON ori.inventory_id = via.inventory_id
			WHERE ori.order_request_id IN (:orderrequestid)

		UNION SELECT inventory_viability_id
			FROM inventory_viability via
			INNER JOIN inventory i ON i.inventory_id = via.inventory_id
			INNER JOIN accession a ON i.accession_id = a.accession_id
			INNER JOIN taxonomy_species ts ON ts.taxonomy_species_id = a.taxonomy_species_id
			WHERE  ts.taxonomy_genus_id IN (:taxonomygenusid)
	) list ON via.inventory_viability_id = list.inventory_viability_id