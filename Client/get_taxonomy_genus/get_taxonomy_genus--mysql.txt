SELECT tg.taxonomy_genus_id
    ,tg.current_taxonomy_genus_id
    ,tg.genus_name
    ,tg.qualifying_code
    ,tg.hybrid_code
    ,tg.genus_authority
    ,tg.subgenus_name
    ,tg.section_name
    ,tg.series_name
    ,tg.subseries_name
    ,tg.subsection_name
    ,tcn.name as genus_common_name
    ,tg.note
    ,tg.taxonomy_family_id
    ,tf.subfamily_name
    ,tf.tribe_name
    ,tf.subtribe_name
    ,tg.created_date
    ,tg.created_by
    ,tg.modified_date
    ,tg.modified_by
    ,tg.owned_date
    ,tg.owned_by
FROM    taxonomy_family tf
	inner join taxonomy_genus tg 
		on tf.taxonomy_family_id = tg.taxonomy_family_id
	inner join taxonomy_species ts
		on tg.taxonomy_genus_id = ts.taxonomy_genus_id
	inner join accession a
		on ts.taxonomy_species_id = a.taxonomy_species_id
	left join inventory i
		on a.accession_id = i.accession_id
	left join taxonomy_common_name tcn
		on tg.taxonomy_genus_id = tcn.taxonomy_genus_id
WHERE
    a.accession_id in (:accessionid)

UNION

SELECT tg.taxonomy_genus_id
    ,tg.current_taxonomy_genus_id
    ,tg.genus_name
    ,tg.qualifying_code
    ,tg.hybrid_code
    ,tg.genus_authority
    ,tg.subgenus_name
    ,tg.section_name
    ,tg.series_name
    ,tg.subseries_name
    ,tg.subsection_name
    ,tcn.name as genus_common_name
    ,tg.note
    ,tg.taxonomy_family_id
    ,tf.subfamily_name
    ,tf.tribe_name
    ,tf.subtribe_name
    ,tg.created_date
    ,tg.created_by
    ,tg.modified_date
    ,tg.modified_by
    ,tg.owned_date
    ,tg.owned_by
FROM    taxonomy_family tf
	inner join taxonomy_genus tg 
		on tf.taxonomy_family_id = tg.taxonomy_family_id
	inner join taxonomy_species ts
		on tg.taxonomy_genus_id = ts.taxonomy_genus_id
	inner join accession a
		on ts.taxonomy_species_id = a.taxonomy_species_id
	left join inventory i
		on a.accession_id = i.accession_id
	left join taxonomy_common_name tcn
		on tg.taxonomy_genus_id = tcn.taxonomy_genus_id
WHERE
    i.inventory_id in (:inventoryid)

UNION

SELECT tg.taxonomy_genus_id
    ,tg.current_taxonomy_genus_id
    ,tg.genus_name
    ,tg.qualifying_code
    ,tg.hybrid_code
    ,tg.genus_authority
    ,tg.subgenus_name
    ,tg.section_name
    ,tg.series_name
    ,tg.subseries_name
    ,tg.subsection_name
    ,tcn.name as genus_common_name
    ,tg.note
    ,tg.taxonomy_family_id
    ,tf.subfamily_name
    ,tf.tribe_name
    ,tf.subtribe_name
    ,tg.created_date
    ,tg.created_by
    ,tg.modified_date
    ,tg.modified_by
    ,tg.owned_date
    ,tg.owned_by
FROM    taxonomy_family tf
	inner join taxonomy_genus tg 
		on tf.taxonomy_family_id = tg.taxonomy_family_id
	left join taxonomy_common_name tcn
		on tg.taxonomy_genus_id = tcn.taxonomy_genus_id
WHERE
    tg.taxonomy_genus_id in (:taxonomygenusid)

UNION

SELECT tg.taxonomy_genus_id
    ,tg.current_taxonomy_genus_id
    ,tg.genus_name
    ,tg.qualifying_code
    ,tg.hybrid_code
    ,tg.genus_authority
    ,tg.subgenus_name
    ,tg.section_name
    ,tg.series_name
    ,tg.subseries_name
    ,tg.subsection_name
    ,tcn.name as genus_common_name
    ,tg.note
    ,tg.taxonomy_family_id
    ,tf.subfamily_name
    ,tf.tribe_name
    ,tf.subtribe_name
    ,tg.created_date
    ,tg.created_by
    ,tg.modified_date
    ,tg.modified_by
    ,tg.owned_date
    ,tg.owned_by
FROM    taxonomy_family tf
	inner join taxonomy_genus tg 
		on tf.taxonomy_family_id = tg.taxonomy_family_id
	left join taxonomy_common_name tcn
		on tg.taxonomy_genus_id = tcn.taxonomy_genus_id
        left join taxonomy_species ts
                on ts.taxonomy_genus_id = tg.taxonomy_genus_id
        left join taxonomy_crop_map tcm
                on ts.taxonomy_species_id = tcm.taxonomy_species_id
WHERE 
  tcm.crop_id in (:cropid)