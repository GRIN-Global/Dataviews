SELECT
  ga.genetic_annotation_id,
  gm.crop_id,
  ga.genetic_marker_id,
  ga.method_id,
  dbms_lob.substr(ga.assay_method,32767,1) AS assay_method,
  dbms_lob.substr(ga.scoring_method,32767,1) AS scoring_method,
  dbms_lob.substr(ga.control_values,32767,1) AS control_values,
  ga.observation_alleles_count,
  ga.max_gob_alleles,
  ga.size_alleles,
  ga.unusual_alleles,
  dbms_lob.substr(ga.note,32767,1) AS note,
  ga.created_date,
  ga.created_by,
  ga.modified_date,
  ga.modified_by,
  ga.owned_date,
  ga.owned_by
FROM 
    genetic_annotation ga
    INNER JOIN genetic_marker gm ON ga.genetic_marker_id = gm.genetic_marker_id
    INNER JOIN (
	SELECT genetic_annotation_id FROM genetic_annotation
            WHERE genetic_annotation_id in (:geneticannotationid)

	UNION SELECT genetic_annotation_id FROM genetic_annotation
            WHERE genetic_marker_id in (:geneticmarkerid)

	UNION SELECT genetic_annotation_id FROM genetic_annotation
            WHERE method_id in (:methodid)

	UNION SELECT genetic_annotation_id FROM genetic_annotation
            INNER JOIN genetic_marker ON genetic_annotation.genetic_marker_id = genetic_marker.genetic_marker_id
            WHERE genetic_marker.crop_id in (:cropid)

	UNION SELECT genetic_annotation.genetic_annotation_id FROM genetic_annotation
            INNER JOIN genetic_observation_data ON genetic_annotation.genetic_annotation_id = genetic_observation_data.genetic_annotation_id
            WHERE genetic_observation_data.inventory_id in (:inventoryid)

	UNION SELECT genetic_annotation.genetic_annotation_id FROM genetic_annotation
            INNER JOIN genetic_observation_data ON genetic_annotation.genetic_annotation_id = genetic_observation_data.genetic_annotation_id
            WHERE genetic_observation_data.genetic_observation_data_id in (:geneticobservationdataid)

	UNION SELECT genetic_annotation.genetic_annotation_id FROM genetic_annotation
            INNER JOIN genetic_observation_data ON genetic_annotation.genetic_annotation_id = genetic_observation_data.genetic_annotation_id
	    INNER JOIN inventory ON genetic_observation_data.inventory_id = inventory.inventory_id
            WHERE inventory.accession_id in (:accessionid)

    ) list ON ga.genetic_annotation_id = list.genetic_annotation_id