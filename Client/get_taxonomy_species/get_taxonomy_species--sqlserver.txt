SELECT
  distinct ts.taxonomy_species_id,
  ts.nomen_number,
  ts.current_taxonomy_species_id,
  ts.is_specific_hybrid,
  ts.taxonomy_genus_id as extended_genus_name,
  (select tg.genus_name from taxonomy_genus tg where tg.taxonomy_genus_id = ts.taxonomy_genus_id) as genus_name,
  (select count(*) from accession a2 where a2.taxonomy_species_id = ts.taxonomy_species_id) as accession_count,
  ts.species_name,
  ts.species_authority,
  ts.is_subspecific_hybrid,
  ts.subspecies_name,
  ts.subspecies_authority,
  ts.is_varietal_hybrid,
  ts.variety_name,
  ts.variety_authority,
  ts.is_subvarietal_hybrid,
  ts.subvariety_name,
  ts.subvariety_authority,
  ts.is_forma_hybrid,
  ts.forma_rank_type,
  ts.forma_name,
  ts.forma_authority,
  ts.priority1_site_id,
  ts.priority2_site_id,
  ts.curator1_cooperator_id,
  ts.curator2_cooperator_id,
  ts.restriction_code,
  ts.life_form_code,
  ts.common_fertilization_code,
  ts.is_name_pending,
  ts.synonym_code,
  ts.verifier_cooperator_id,
  ts.name_verified_date,
  ts.name,
  ts.name_authority,
  ts.protologue,
  (select min(name) from taxonomy_common_name tc where ts.taxonomy_species_id = tc.taxonomy_species_id) AS species_common_name,
  ts.note,
  ts.site_note,
  ts.alternate_name,
  ts.created_date,
  ts.created_by,
  ts.modified_date,
  ts.modified_by,
  ts.owned_date,
  ts.owned_by
FROM  taxonomy_species ts 
where
  ts.taxonomy_species_id in (:taxonomyspeciesid)
UNION
SELECT
  distinct ts.taxonomy_species_id,
  ts.nomen_number,
  ts.current_taxonomy_species_id,
  ts.is_specific_hybrid,
  ts.taxonomy_genus_id as extended_genus_name,
  (select tg.genus_name from taxonomy_genus tg where tg.taxonomy_genus_id = ts.taxonomy_genus_id) as genus_name,
  (select count(*) from accession a2 where a2.taxonomy_species_id = ts.taxonomy_species_id) as accession_count,
  ts.species_name,
  ts.species_authority,
  ts.is_subspecific_hybrid,
  ts.subspecies_name,
  ts.subspecies_authority,
  ts.is_varietal_hybrid,
  ts.variety_name,
  ts.variety_authority,
  ts.is_subvarietal_hybrid,
  ts.subvariety_name,
  ts.subvariety_authority,
  ts.is_forma_hybrid,
  ts.forma_rank_type,
  ts.forma_name,
  ts.forma_authority,
  ts.priority1_site_id,
  ts.priority2_site_id,
  ts.curator1_cooperator_id,
  ts.curator2_cooperator_id,
  ts.restriction_code,
  ts.life_form_code,
  ts.common_fertilization_code,
  ts.is_name_pending,
  ts.synonym_code,
  ts.verifier_cooperator_id,
  ts.name_verified_date,
  ts.name,
  ts.name_authority,
  ts.protologue,
  (select min(name) from taxonomy_common_name tc where ts.taxonomy_species_id = tc.taxonomy_species_id) AS species_common_name,
  ts.note,
  ts.site_note,
  ts.alternate_name,
  ts.created_date,
  ts.created_by,
  ts.modified_date,
  ts.modified_by,
  ts.owned_date,
  ts.owned_by
FROM  taxonomy_species ts inner join accession a
	on ts.taxonomy_species_id = a.taxonomy_species_id

where
  a.accession_id in (:accessionid)
UNION
SELECT
  distinct ts.taxonomy_species_id,
  ts.nomen_number,
  ts.current_taxonomy_species_id,
  ts.is_specific_hybrid,
  ts.taxonomy_genus_id as extended_genus_name,
  (select tg.genus_name from taxonomy_genus tg where tg.taxonomy_genus_id = ts.taxonomy_genus_id) as genus_name,
  (select count(*) from accession a2 where a2.taxonomy_species_id = ts.taxonomy_species_id) as accession_count,
  ts.species_name,
  ts.species_authority,
  ts.is_subspecific_hybrid,
  ts.subspecies_name,
  ts.subspecies_authority,
  ts.is_varietal_hybrid,
  ts.variety_name,
  ts.variety_authority,
  ts.is_subvarietal_hybrid,
  ts.subvariety_name,
  ts.subvariety_authority,
  ts.is_forma_hybrid,
  ts.forma_rank_type,
  ts.forma_name,
  ts.forma_authority,
  ts.priority1_site_id,
  ts.priority2_site_id,
  ts.curator1_cooperator_id,
  ts.curator2_cooperator_id,
  ts.restriction_code,
  ts.life_form_code,
  ts.common_fertilization_code,
  ts.is_name_pending,
  ts.synonym_code,
  ts.verifier_cooperator_id,
  ts.name_verified_date,
  ts.name,
  ts.name_authority,
  ts.protologue,
  (select min(name) from taxonomy_common_name tc where ts.taxonomy_species_id = tc.taxonomy_species_id) AS species_common_name,
  ts.note,
  ts.site_note,
  ts.alternate_name,
  ts.created_date,
  ts.created_by,
  ts.modified_date,
  ts.modified_by,
  ts.owned_date,
  ts.owned_by
FROM  taxonomy_species ts inner join accession a
	on ts.taxonomy_species_id = a.taxonomy_species_id
	left join inventory i
	on i.accession_id = a.accession_id

where
  i.inventory_id in (:inventoryid)

UNION
SELECT
  distinct ts.taxonomy_species_id,
  ts.nomen_number,
  ts.current_taxonomy_species_id,
  ts.is_specific_hybrid,
  ts.taxonomy_genus_id as extended_genus_name,
  (select tg.genus_name from taxonomy_genus tg where tg.taxonomy_genus_id = ts.taxonomy_genus_id) as genus_name,
  (select count(*) from accession a2 where a2.taxonomy_species_id = ts.taxonomy_species_id) as accession_count,
  ts.species_name,
  ts.species_authority,
  ts.is_subspecific_hybrid,
  ts.subspecies_name,
  ts.subspecies_authority,
  ts.is_varietal_hybrid,
  ts.variety_name,
  ts.variety_authority,
  ts.is_subvarietal_hybrid,
  ts.subvariety_name,
  ts.subvariety_authority,
  ts.is_forma_hybrid,
  ts.forma_rank_type,
  ts.forma_name,
  ts.forma_authority,
  ts.priority1_site_id,
  ts.priority2_site_id,
  ts.curator1_cooperator_id,
  ts.curator2_cooperator_id,
  ts.restriction_code,
  ts.life_form_code,
  ts.common_fertilization_code,
  ts.is_name_pending,
  ts.synonym_code,
  ts.verifier_cooperator_id,
  ts.name_verified_date,
  ts.name,
  ts.name_authority,
  ts.protologue,
  (select min(name) from taxonomy_common_name tc where ts.taxonomy_species_id = tc.taxonomy_species_id) AS species_common_name,
  ts.note,
  ts.site_note,
  ts.alternate_name,
  ts.created_date,
  ts.created_by,
  ts.modified_date,
  ts.modified_by,
  ts.owned_date,
  ts.owned_by
FROM taxonomy_species ts left join taxonomy_genus tg
	on ts.taxonomy_genus_id = tg.taxonomy_genus_id
WHERE 
ts.taxonomy_genus_id in (:taxonomygenusid)
UNION
SELECT
  distinct ts.taxonomy_species_id,
  ts.nomen_number,
  ts.current_taxonomy_species_id,
  ts.is_specific_hybrid,
  ts.taxonomy_genus_id as extended_genus_name,
  (select tg.genus_name from taxonomy_genus tg where tg.taxonomy_genus_id = ts.taxonomy_genus_id) as genus_name,
  (select count(*) from accession a2 where a2.taxonomy_species_id = ts.taxonomy_species_id) as accession_count,
  ts.species_name,
  ts.species_authority,
  ts.is_subspecific_hybrid,
  ts.subspecies_name,
  ts.subspecies_authority,
  ts.is_varietal_hybrid,
  ts.variety_name,
  ts.variety_authority,
  ts.is_subvarietal_hybrid,
  ts.subvariety_name,
  ts.subvariety_authority,
  ts.is_forma_hybrid,
  ts.forma_rank_type,
  ts.forma_name,
  ts.forma_authority,
  ts.priority1_site_id,
  ts.priority2_site_id,
  ts.curator1_cooperator_id,
  ts.curator2_cooperator_id,
  ts.restriction_code,
  ts.life_form_code,
  ts.common_fertilization_code,
  ts.is_name_pending,
  ts.synonym_code,
  ts.verifier_cooperator_id,
  ts.name_verified_date,
  ts.name,
  ts.name_authority,
  ts.protologue,
  (select min(name) from taxonomy_common_name tc where ts.taxonomy_species_id = tc.taxonomy_species_id) AS species_common_name,
  ts.note,
  ts.site_note,
  ts.alternate_name,
  ts.created_date,
  ts.created_by,
  ts.modified_date,
  ts.modified_by,
  ts.owned_date,
  ts.owned_by
FROM  taxonomy_species ts inner join accession a
	on ts.taxonomy_species_id = a.taxonomy_species_id
	left join inventory i
	on i.accession_id = a.accession_id

WHERE
  ts.curator1_cooperator_id in (:cooperatorid) or
  ts.curator2_cooperator_id in (:cooperatorid) or
  ts.verifier_cooperator_id in (:cooperatorid)

UNION
SELECT
  distinct ts.taxonomy_species_id,
  ts.nomen_number,
  ts.current_taxonomy_species_id,
  ts.is_specific_hybrid,
  ts.taxonomy_genus_id as extended_genus_name,
  (select tg.genus_name from taxonomy_genus tg where tg.taxonomy_genus_id = ts.taxonomy_genus_id) as genus_name,
  (select count(*) from accession a2 where a2.taxonomy_species_id = ts.taxonomy_species_id) as accession_count,
  ts.species_name,
  ts.species_authority,
  ts.is_subspecific_hybrid,
  ts.subspecies_name,
  ts.subspecies_authority,
  ts.is_varietal_hybrid,
  ts.variety_name,
  ts.variety_authority,
  ts.is_subvarietal_hybrid,
  ts.subvariety_name,
  ts.subvariety_authority,
  ts.is_forma_hybrid,
  ts.forma_rank_type,
  ts.forma_name,
  ts.forma_authority,
  ts.priority1_site_id,
  ts.priority2_site_id,
  ts.curator1_cooperator_id,
  ts.curator2_cooperator_id,
  ts.restriction_code,
  ts.life_form_code,
  ts.common_fertilization_code,
  ts.is_name_pending,
  ts.synonym_code,
  ts.verifier_cooperator_id,
  ts.name_verified_date,
  ts.name,
  ts.name_authority,
  ts.protologue,
  (select min(name) from taxonomy_common_name tc where ts.taxonomy_species_id = tc.taxonomy_species_id) AS species_common_name,
  ts.note,
  ts.site_note,
  ts.alternate_name,
  ts.created_date,
  ts.created_by,
  ts.modified_date,
  ts.modified_by,
  ts.owned_date,
  ts.owned_by
FROM 
  taxonomy_species ts, taxonomy_geography_map tgm
	
WHERE 
  ts.taxonomy_species_id = tgm.taxonomy_species_id and
  tgm.geography_id in (:geographyid)

UNION
SELECT
  distinct ts.taxonomy_species_id,
  ts.nomen_number,
  ts.current_taxonomy_species_id,
  ts.is_specific_hybrid,
  ts.taxonomy_genus_id as extended_genus_name,
  (select tg.genus_name from taxonomy_genus tg where tg.taxonomy_genus_id = ts.taxonomy_genus_id) as genus_name,
  (select count(*) from accession a2 where a2.taxonomy_species_id = ts.taxonomy_species_id) as accession_count,
  ts.species_name,
  ts.species_authority,
  ts.is_subspecific_hybrid,
  ts.subspecies_name,
  ts.subspecies_authority,
  ts.is_varietal_hybrid,
  ts.variety_name,
  ts.variety_authority,
  ts.is_subvarietal_hybrid,
  ts.subvariety_name,
  ts.subvariety_authority,
  ts.is_forma_hybrid,
  ts.forma_rank_type,
  ts.forma_name,
  ts.forma_authority,
  ts.priority1_site_id,
  ts.priority2_site_id,
  ts.curator1_cooperator_id,
  ts.curator2_cooperator_id,
  ts.restriction_code,
  ts.life_form_code,
  ts.common_fertilization_code,
  ts.is_name_pending,
  ts.synonym_code,
  ts.verifier_cooperator_id,
  ts.name_verified_date,
  ts.name,
  ts.name_authority,
  ts.protologue,
  (select min(name) from taxonomy_common_name tc where ts.taxonomy_species_id = tc.taxonomy_species_id) AS species_common_name,
  ts.note,
  ts.site_note,
  ts.alternate_name,
  ts.created_date,
  ts.created_by,
  ts.modified_date, 
  ts.modified_by,
  ts.owned_date,
  ts.owned_by
FROM 
  taxonomy_species ts, taxonomy_crop_map tcm
	
WHERE 
  ts.taxonomy_species_id = tcm.taxonomy_species_id and
  tcm.crop_id in (:cropid)

