SELECT
  gob.genetic_observation_id,
  i.accession_id,
  gob.inventory_id,
  (SELECT crop_id FROM genetic_marker WHERE genetic_marker_id = 
    (SELECT genetic_marker_id FROM genetic_annotation ga WHERE ga.genetic_annotation_id = gob.genetic_annotation_id)) AS crop_id,
  gob.genetic_annotation_id,
  gob.is_archived,
  gob.data_quality_code,
  gob.frequency,
  gob.value,
  gob.rank,
  gob.mean_value,
  gob.maximum_value,
  gob.minimum_value,
  gob.standard_deviation,
  gob.sample_size,
  gob.note,
  (SELECT taxonomy_species_id FROM accession WHERE accession_id = i.accession_id) AS taxonomy_species_id,
  gob.created_date,
  gob.created_by,
  gob.modified_date,
  gob.modified_by,
  gob.owned_date,
  gob.owned_by
FROM
    genetic_observation gob
    INNER JOIN inventory i ON gob.inventory_id = i.inventory_id
    INNER JOIN (
	SELECT genetic_observation_id FROM genetic_observation
            WHERE genetic_observation_id IN (:geneticobservationid)

	UNION SELECT genetic_observation_id FROM genetic_observation_data
            WHERE genetic_observation_id IN (:geneticobservationid)

	UNION SELECT genetic_observation_id FROM genetic_observation
            WHERE genetic_annotation_id IN (:geneticannotationid)

	UNION SELECT genetic_observation_id FROM genetic_observation
            WHERE inventory_id IN (:inventoryid)

	UNION SELECT genetic_observation_id FROM genetic_observation
	    INNER JOIN inventory i ON genetic_observation.inventory_id = i.inventory_id
            WHERE accession_id IN (:accessionid)

	UNION SELECT genetic_observation_id FROM genetic_observation
	    INNER JOIN inventory i ON genetic_observation.inventory_id = i.inventory_id
	    INNER JOIN order_request_item ori ON i.inventory_id = ori.inventory_id
            WHERE ori.order_request_id in (:orderrequestid)

	UNION SELECT genetic_observation_id FROM genetic_observation
	    INNER JOIN genetic_annotation ga ON genetic_observation.genetic_annotation_id = ga.genetic_annotation_id
            WHERE genetic_marker_id IN (:geneticmarkerid)

	UNION SELECT genetic_observation_id FROM genetic_observation
	    INNER JOIN genetic_annotation ga ON genetic_observation.genetic_annotation_id = ga.genetic_annotation_id
            WHERE method_id IN (:methodid)

	UNION SELECT genetic_observation_id FROM genetic_observation
	    INNER JOIN genetic_annotation ga ON genetic_observation.genetic_annotation_id = ga.genetic_annotation_id
	    INNER JOIN genetic_marker gm ON ga.genetic_marker_id = gm.genetic_marker_id
            WHERE crop_id IN (:cropid)

    ) list ON gob.genetic_observation_id = list.genetic_observation_id