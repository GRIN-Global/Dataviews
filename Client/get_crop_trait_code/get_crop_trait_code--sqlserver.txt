SELECT
  ctc.crop_trait_code_id,
  ct.crop_id,
  ct.coded_name,
  ctc.crop_trait_id,
  ctl.description as trait_description,
  ctc.code,
  ctcl.title,
  ctcl.description as code_description,
  ctc.created_date,
  ctc.created_by,
  ctc.modified_date,
  ctc.modified_by,
  ctc.owned_date,
  ctc.owned_by
FROM  
    crop_trait_code ctc
    INNER JOIN crop_trait ct ON ctc.crop_trait_id = ct.crop_trait_id
    LEFT JOIN crop_trait_lang ctl ON ctl.crop_trait_id = ct.crop_trait_id
                                  AND ctl.sys_lang_id = __LANGUAGEID__
    LEFT JOIN crop_trait_code_lang ctcl ON ctcl.crop_trait_code_id = ctc.crop_trait_code_id
                                        AND ctcl.sys_lang_id = __LANGUAGEID__
    INNER JOIN (
	SELECT crop_trait_code_id FROM crop_trait_code
            WHERE crop_trait_code_id in (:croptraitcodeid)

	UNION SELECT ctc.crop_trait_code_id FROM crop_trait_code ctc
            WHERE crop_trait_id in (:croptraitid)

	UNION SELECT ctcl.crop_trait_code_id FROM crop_trait_code_lang ctcl
            WHERE ctcl.crop_trait_code_lang_id in (:croptraitcodelangid)

	UNION SELECT ctc.crop_trait_code_id FROM crop_trait_code ctc
            INNER JOIN crop_trait ct ON ct.crop_trait_id = ctc.crop_trait_id
            WHERE ct.crop_id in (:cropid)

	UNION SELECT ctc.crop_trait_code_id FROM crop_trait_code ctc
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctc.crop_trait_code_id
            WHERE cto.crop_trait_observation_id IN (:croptraitobservationid)

	UNION SELECT ctc.crop_trait_code_id FROM crop_trait_code ctc
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctc.crop_trait_code_id
            WHERE cto.inventory_id in (:inventoryid)

	UNION SELECT ctc.crop_trait_code_id FROM crop_trait_code ctc
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctc.crop_trait_code_id
	    INNER JOIN inventory i ON cto.inventory_id = i.inventory_id
            WHERE i.accession_id in (:accessionid)

	UNION SELECT ctc.crop_trait_code_id FROM crop_trait_code ctc
            INNER JOIN crop_trait_observation cto ON cto.crop_trait_code_id = ctc.crop_trait_code_id
	    INNER JOIN inventory i ON cto.inventory_id = i.inventory_id
	    INNER JOIN order_request_item ori ON ori.inventory_id = i.inventory_id
            WHERE ori.order_request_id in (:orderrequestid)

    ) list ON ctc.crop_trait_code_id = list.crop_trait_code_id