SELECT
  mm.method_map_id,
  mm.cooperator_id,
  mm.method_id,
  mm.created_date,
  mm.created_by,
  mm.modified_date,
  mm.modified_by,
  mm.owned_date,
  mm.owned_by
FROM
    method_map mm
    INNER JOIN (
        SELECT method_map_id FROM method_map WHERE method_map_id IN (:methodmapid)

        UNION SELECT method_map_id FROM method_map WHERE cooperator_id IN (:cooperatorid)

        UNION SELECT method_map_id FROM method_map WHERE method_id IN (:methodid)

        UNION SELECT method_map_id FROM method_map
            INNER JOIN cooperator ON method_map.cooperator_id = cooperator.cooperator_id 
            WHERE geography_id IN (:geographyid)

    ) list ON list.method_map_id = mm.method_map_id