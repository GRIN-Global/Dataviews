SELECT
  tu.taxonomy_use_id,
  tu.taxonomy_species_id,
  tu.economic_usage_code,
  tu.usage_type,
  tu.plant_part_code,
  tu.citation_id,
  (SELECT literature_id l FROM citation c WHERE c.citation_id = tu.citation_id) AS literature_id,
  tu.note,
  tu.created_date,
  tu.created_by,
  tu.modified_date,
  tu.modified_by,
  tu.owned_date,
  tu.owned_by
FROM
    taxonomy_use tu
    INNER JOIN (
        SELECT taxonomy_use_id FROM taxonomy_use WHERE taxonomy_use_id IN (:taxonomyuseid)

        UNION SELECT taxonomy_use_id FROM taxonomy_use
            WHERE taxonomy_species_id IN (:taxonomyspeciesid)

        UNION SELECT taxonomy_use_id FROM taxonomy_use
            WHERE citation_id in (:citationid)

        UNION SELECT taxonomy_use_id FROM taxonomy_use
            INNER JOIN citation ON taxonomy_use.citation_id = citation.citation_id
            WHERE literature_id in (:literatureid)

        UNION SELECT taxonomy_use_id FROM taxonomy_use
            INNER JOIN taxonomy_species ON taxonomy_use.taxonomy_species_id = taxonomy_species.taxonomy_species_id
            WHERE taxonomy_genus_id IN (:taxonomygenusid)

        UNION SELECT DISTINCT taxonomy_use_id FROM taxonomy_use
            INNER JOIN accession ON taxonomy_use.taxonomy_species_id = accession.taxonomy_species_id
            WHERE accession_id IN (:accessionid)

    ) list ON list.taxonomy_use_id = tu.taxonomy_use_id