SELECT
  s.accession_source_id,
  s.accession_id,
  s.source_type_code,
  s.source_date_code,
  s.source_date,
  s.geography_id,
  s.is_origin,
  s.acquisition_source_code,
  s.quantity_collected,
  s.unit_quantity_collected_code,
  s.collected_form_code,
  s.number_plants_sampled,
  s.environment_description,
  s.collector_verbatim_locality,
  s.elevation_meters,
  s.latitude,
  s.longitude,
  s.uncertainty,
  s.formatted_locality,
  s.georeference_datum,
  s.georeference_protocol_code,
  s.georeference_annotation,
  s.associated_species,
  s.is_web_visible,
  s.note,
  (SELECT taxonomy_species_id FROM accession WHERE accession_id = s.accession_id) AS taxonomy_species_id,
  s.created_date,
  s.created_by,
  s.modified_date,
  s.modified_by,
  s.owned_date,
  s.owned_by
FROM
    accession_source s
    INNER JOIN (
        SELECT s.accession_source_id FROM accession_source s
            WHERE s.accession_source_id IN (:accessionsourceid)

        UNION SELECT s.accession_source_id FROM accession_source s
            WHERE s.accession_id IN (:accessionid)

        UNION SELECT s.accession_source_id FROM accession_source s
            WHERE s.geography_id IN (:geographyid)

        UNION SELECT s.accession_source_id FROM accession_source s
            INNER JOIN accession_source_map asm ON s.accession_source_id = asm.accession_source_id
            WHERE asm.cooperator_id IN (:cooperatorid)

        UNION SELECT s.accession_source_id FROM accession_source s
            INNER JOIN inventory i ON i.accession_id = s.accession_id
            WHERE i.inventory_id IN (:inventoryid)

        UNION SELECT s.accession_source_id FROM accession_source s
            INNER JOIN inventory i ON i.accession_id = s.accession_id
            INNER JOIN order_request_item ori ON ori.inventory_id = i.inventory_id
            WHERE ori.order_request_id IN (:orderrequestid)

        UNION SELECT sdc.accession_source_id FROM source_desc_observation sdc
            WHERE sdc.source_desc_observation_id IN (:sourcedescobservationid)

    ) list ON list.accession_source_id = s.accession_source_id
