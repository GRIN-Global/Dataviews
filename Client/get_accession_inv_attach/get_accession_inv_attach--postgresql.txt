SELECT
  ia.accession_inv_attach_id AS accession_inv_attach_id,
  i.accession_id,
  ia.inventory_id,
  ia.virtual_path,
  ia.thumbnail_virtual_path,
  ia.sort_order,
  ia.title,
  ia.description,
  ia.content_type,
  ia.category_code,
  ia.copyright_information,
  ia.attach_cooperator_id,
  ia.is_web_visible,
  ia.attach_date_code,
  ia.attach_date,
  ia.note,
  (SELECT taxonomy_species_id FROM accession WHERE accession_id = i.accession_id) AS taxonomy_species_id,
  ia.created_date,
  ia.created_by,
  ia.modified_date,
  ia.modified_by,
  ia.owned_date,
  ia.owned_by
FROM
    accession_inv_attach ia
    INNER JOIN inventory i ON ia.inventory_id = i.inventory_id
    INNER JOIN (
	SELECT accession_inv_attach_id FROM accession_inv_attach
            WHERE accession_inv_attach_id in (:accessioninvattachid)

	UNION SELECT accession_inv_attach_id FROM accession_inv_attach
            WHERE inventory_id in (:inventoryid)

	UNION SELECT accession_inv_attach_id FROM accession_inv_attach
            WHERE attach_cooperator_id in (:cooperatorid)

	UNION SELECT accession_inv_attach_id FROM accession_inv_attach
            INNER JOIN inventory ON accession_inv_attach.inventory_id = inventory.inventory_id
            WHERE inventory.accession_id in (:accessionid)

	UNION SELECT accession_inv_attach_id FROM accession_inv_attach
            INNER JOIN order_request_item ON accession_inv_attach.inventory_id = order_request_item.inventory_id
            WHERE order_request_item.order_request_id in (:orderrequestid)

    ) list ON ia.accession_inv_attach_id = list.accession_inv_attach_id
