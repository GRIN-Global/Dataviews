SELECT distinct c.crop_id
    ,c.name
    ,dbms_lob.substr(c.note,32767,1) AS note
    ,c.created_date
    ,c.created_by
    ,c.modified_date
    ,c.modified_by
    ,c.owned_date
    ,c.owned_by
FROM
    crop c
    INNER JOIN (
        SELECT crop_id FROM crop
            WHERE crop_id IN (:cropid)
              OR (':cropid' = '-1' AND ':cropattachid' = '-1' AND ':croptraitid' = '-1'
                  AND ':croptraitcodeid' = '-1' AND ':croptraitobservationid' = '-1'
                  AND ':inventoryid' = '-1' AND ':accessionid' = '-1' 
                  AND ':orderrequestid' = '-1' AND ':taxonomygenusid' = '-1')

        UNION SELECT crop_id FROM crop_attach
            WHERE crop_attach_id IN (:cropattachid)

        UNION SELECT c.crop_id FROM crop c
            INNER JOIN crop_trait ct ON ct.crop_id = c.crop_id
            WHERE crop_trait_id IN (:croptraitid)

        UNION SELECT c.crop_id FROM crop c
            INNER JOIN crop_trait ct ON ct.crop_id = c.crop_id
	    INNER JOIN crop_trait_code ctc ON ctc.crop_trait_id = ct.crop_trait_id
            WHERE ctc.crop_trait_code_id IN (:croptraitcodeid)

        UNION SELECT c.crop_id FROM crop c
            INNER JOIN crop_trait ct ON ct.crop_id = c.crop_id
	    INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
            WHERE cto.crop_trait_observation_id IN (:croptraitobservationid)

        UNION SELECT c.crop_id FROM crop c
            INNER JOIN crop_trait ct ON ct.crop_id = c.crop_id
	    INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
            WHERE cto.inventory_id IN (:inventoryid)

        UNION SELECT c.crop_id FROM crop c
            INNER JOIN crop_trait ct ON ct.crop_id = c.crop_id
	    INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
	    INNER JOIN inventory i ON i.inventory_id = cto.inventory_id
            WHERE i.accession_id IN (:accessionid)

        UNION SELECT c.crop_id FROM crop c
            INNER JOIN crop_trait ct ON ct.crop_id = c.crop_id
	    INNER JOIN crop_trait_observation cto ON cto.crop_trait_id = ct.crop_trait_id
	    INNER JOIN inventory i ON cto.inventory_id = i.inventory_id
	    INNER JOIN order_request_item ori ON ori.inventory_id = i.inventory_id
            WHERE ori.order_request_id IN (:orderrequestid)

        UNION SELECT tcm.crop_id FROM taxonomy_crop_map tcm
            INNER JOIN taxonomy_species ts ON ts.taxonomy_species_id = tcm.taxonomy_species_id
            WHERE ts.taxonomy_genus_id IN (:taxonomygenusid)

    ) list ON list.crop_id = c.crop_id
