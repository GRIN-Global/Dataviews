SELECT
  sdo.source_desc_observation_id,
  sdo.accession_source_id,
  sdo.source_descriptor_id,
  sdo.source_descriptor_code_id,
  (select sdc.code from source_descriptor_code sdc where sdc.source_descriptor_code_id = sdo.source_descriptor_code_id) AS sdcode,  
  sdo.numeric_value,
  sdo.string_value,
  sdo.original_value,
  sdo.note,
  sdo.created_date,
  sdo.created_by,
  sdo.modified_date,
  sdo.modified_by,
  sdo.owned_date,
  sdo.owned_by
FROM
    source_desc_observation sdo
WHERE
    sdo.source_desc_observation_id in (:sourcedescobservationid)

UNION
SELECT
  sdo.source_desc_observation_id,
  sdo.accession_source_id,
  sdo.source_descriptor_id,
  sdo.source_descriptor_code_id,
  (select sdc.code from source_descriptor_code sdc where sdc.source_descriptor_code_id = sdo.source_descriptor_code_id) AS sdcode,  
  sdo.numeric_value,
  sdo.string_value,
  sdo.original_value,
  sdo.note,
  sdo.created_date,
  sdo.created_by,
  sdo.modified_date,
  sdo.modified_by,
  sdo.owned_date,
  sdo.owned_by
FROM
    source_desc_observation sdo, accession a, accession_source src
WHERE
  src.accession_source_id = sdo.accession_source_id and
  src.accession_id = a.accession_id and
  a.accession_id in (:accessionid)

UNION
SELECT
  sdo.source_desc_observation_id,
  sdo.accession_source_id,
  sdo.source_descriptor_id,
  sdo.source_descriptor_code_id,
  (select sdc.code from source_descriptor_code sdc where sdc.source_descriptor_code_id = sdo.source_descriptor_code_id) AS sdcode,  
  sdo.numeric_value,
  sdo.string_value,
  sdo.original_value,
  sdo.note,
  sdo.created_date,
  sdo.created_by,
  sdo.modified_date,
  sdo.modified_by,
  sdo.owned_date,
  sdo.owned_by
FROM
    source_desc_observation sdo, accession a, accession_source src, inventory i
WHERE
  src.accession_source_id = sdo.accession_source_id and
  src.accession_id = a.accession_id and
  a.accession_id = i.accession_id and
  i.inventory_id in (:inventoryid)
  
UNION
SELECT
  sdo.source_desc_observation_id,
  sdo.accession_source_id,
  sdo.source_descriptor_id,
  sdo.source_descriptor_code_id,
  (select sdc.code from source_descriptor_code sdc where sdc.source_descriptor_code_id = sdo.source_descriptor_code_id) AS sdcode,  
  sdo.numeric_value,
  sdo.string_value,
  sdo.original_value,
  sdo.note,
  sdo.created_date,
  sdo.created_by,
  sdo.modified_date,
  sdo.modified_by,
  sdo.owned_date,
  sdo.owned_by
FROM
    source_desc_observation sdo
WHERE
  sdo.accession_source_id in(:accessionsourceid) 

UNION
SELECT
  sdo.source_desc_observation_id,
  sdo.accession_source_id,
  sdo.source_descriptor_id,
  sdo.source_descriptor_code_id,
  (select sdc.code from source_descriptor_code sdc where sdc.source_descriptor_code_id = sdo.source_descriptor_code_id) AS sdcode,  
  sdo.numeric_value,
  sdo.string_value,
  sdo.original_value,
  sdo.note,
  sdo.created_date,
  sdo.created_by,
  sdo.modified_date,
  sdo.modified_by,
  sdo.owned_date,
  sdo.owned_by
FROM
    source_desc_observation sdo
WHERE
  sdo.source_descriptor_id in(:sourcedescriptorid) 

UNION
SELECT
  sdo.source_desc_observation_id,
  sdo.accession_source_id,
  sdo.source_descriptor_id,
  sdo.source_descriptor_code_id,
  (select sdc.code from source_descriptor_code sdc where sdc.source_descriptor_code_id = sdo.source_descriptor_code_id) AS sdcode,  
  sdo.numeric_value,
  sdo.string_value,
  sdo.original_value,
  sdo.note,
  sdo.created_date,
  sdo.created_by,
  sdo.modified_date,
  sdo.modified_by,
  sdo.owned_date,
  sdo.owned_by
FROM
    source_desc_observation sdo
WHERE
  sdo.source_descriptor_code_id in(:sourcedescriptorcodeid) 