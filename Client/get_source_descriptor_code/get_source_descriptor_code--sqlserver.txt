SELECT
  sdc.source_descriptor_code_id,
  sdc.source_descriptor_id,
  sdc.code,
  sdc.created_date,
  sdc.created_by,
  sdc.modified_date,
  sdc.modified_by,
  sdc.owned_date,
  sdc.owned_by
FROM
    source_descriptor_code sdc
WHERE
  sdc.source_descriptor_code_id in (:sourcedescriptorcodeid)

UNION
SELECT
  sdc.source_descriptor_code_id,
  sdc.source_descriptor_id,
  sdc.code,
  sdc.created_date,
  sdc.created_by,
  sdc.modified_date,
  sdc.modified_by,
  sdc.owned_date,
  sdc.owned_by
FROM
    source_descriptor_code sdc
WHERE
  sdc.source_descriptor_id in (:sourcedescriptorid)

UNION
SELECT
  sdc.source_descriptor_code_id,
  sdc.source_descriptor_id,
  sdc.code,
  sdc.created_date,
  sdc.created_by,
  sdc.modified_date,
  sdc.modified_by,
  sdc.owned_date,
  sdc.owned_by
FROM
    source_descriptor_code sdc, source_desc_observation sdo, accession_source asrc
WHERE
    sdc.source_descriptor_id = sdo.source_descriptor_id and
    sdo.accession_source_id = asrc.accession_source_id and
    asrc.accession_id in (:accessionid)

UNION
SELECT
  sdc.source_descriptor_code_id,
  sdc.source_descriptor_id,
  sdc.code,
  sdc.created_date,
  sdc.created_by,
  sdc.modified_date,
  sdc.modified_by,
  sdc.owned_date,
  sdc.owned_by
FROM
    source_descriptor_code sdc, source_desc_observation sdo, accession_source asrc, inventory i
WHERE
    sdc.source_descriptor_id = sdo.source_descriptor_id and
    sdo.accession_source_id = asrc.accession_source_id and
    asrc.accession_id = i.accession_id and
    i.inventory_id in (:inventoryid)






