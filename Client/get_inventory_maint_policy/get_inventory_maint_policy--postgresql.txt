SELECT
  imp.inventory_maint_policy_id,
  imp.maintenance_name,
  imp.form_type_code,
  imp.quantity_on_hand_unit_code,
  imp.web_availability_note,
  imp.is_auto_deducted,
  imp.distribution_default_form_code, 
  imp.distribution_default_quantity,
  imp.distribution_unit_code,
  imp.distribution_critical_quantity,
  imp.regeneration_critical_quantity,
  imp.regeneration_method_code,
  imp.curator_cooperator_id,
  imp.note,
  imp.created_date,
  imp.created_by,
  imp.modified_date,
  imp.modified_by,
  imp.owned_date,
  imp.owned_by
FROM
    inventory_maint_policy imp
WHERE
    imp.inventory_maint_policy_id in (:inventorymaintpolicyid)

UNION

SELECT
  imp.inventory_maint_policy_id,
  imp.maintenance_name,
  imp.form_type_code,
  imp.quantity_on_hand_unit_code,
  imp.web_availability_note,
  imp.is_auto_deducted,
  imp.distribution_default_form_code, 
  imp.distribution_default_quantity,
  imp.distribution_unit_code,
  imp.distribution_critical_quantity,
  imp.regeneration_critical_quantity,
  imp.regeneration_method_code,
  imp.curator_cooperator_id,
  imp.note,
  imp.created_date,
  imp.created_by,
  imp.modified_date,
  imp.modified_by,
  imp.owned_date,
  imp.owned_by
FROM
    inventory_maint_policy imp,
    inventory AS i
WHERE
    imp.inventory_maint_policy_id = i.inventory_maint_policy_id AND
    i.accession_id in (:accessionid)

UNION

SELECT
  imp.inventory_maint_policy_id,
  imp.maintenance_name,
  imp.form_type_code,
  imp.quantity_on_hand_unit_code,
  imp.web_availability_note,
  imp.is_auto_deducted,
  imp.distribution_default_form_code,
  imp.distribution_default_quantity,
  imp.distribution_unit_code,
  imp.distribution_critical_quantity,
  imp.regeneration_critical_quantity,
  imp.regeneration_method_code,
  imp.curator_cooperator_id,
  imp.note,
  imp.created_date,
  imp.created_by,
  imp.modified_date,
  imp.modified_by,
  imp.owned_date,
  imp.owned_by
FROM
    inventory_maint_policy imp,
    inventory AS i
WHERE
    imp.inventory_maint_policy_id = i.inventory_maint_policy_id AND
    i.inventory_id in (:inventoryid)

UNION

SELECT
  imp.inventory_maint_policy_id,
  imp.maintenance_name,
  imp.form_type_code,
  imp.quantity_on_hand_unit_code,
  imp.web_availability_note,
  imp.is_auto_deducted,
  imp.distribution_default_form_code,
  imp.distribution_default_quantity,
  imp.distribution_unit_code,
  imp.distribution_critical_quantity,
  imp.regeneration_critical_quantity,
  imp.regeneration_method_code,
  imp.curator_cooperator_id,
  imp.note,
  imp.created_date,
  imp.created_by,
  imp.modified_date,
  imp.modified_by,
  imp.owned_date,
  imp.owned_by
FROM
    inventory_maint_policy imp,
    inventory AS i,
    order_request_item AS ori
WHERE
    imp.inventory_maint_policy_id = i.inventory_maint_policy_id AND
    i.inventory_id = ori.inventory_id AND
    ori.order_request_id in (:orderrequestid)

UNION

SELECT
  imp.inventory_maint_policy_id,
  imp.maintenance_name,
  imp.form_type_code,
  imp.quantity_on_hand_unit_code,
  imp.web_availability_note,
  imp.is_auto_deducted,
  imp.distribution_default_form_code,
  imp.distribution_default_quantity,
  imp.distribution_unit_code,
  imp.distribution_critical_quantity,
  imp.regeneration_critical_quantity,
  imp.regeneration_method_code,
  imp.curator_cooperator_id,
  imp.note,
  imp.created_date,
  imp.created_by,
  imp.modified_date,
  imp.modified_by,
  imp.owned_date,
  imp.owned_by
FROM
    inventory_maint_policy imp
WHERE
    imp.curator_cooperator_id in (:cooperatorid)







