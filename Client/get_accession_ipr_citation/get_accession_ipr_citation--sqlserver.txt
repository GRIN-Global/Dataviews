SELECT
  c.citation_id,
  c.accession_ipr_id,
  c.type_code,
  c.author_name,
  c.citation_year,
  c.title,
  c.citation_title,
  l.abbreviation,
  c.reference,
  c.literature_id, 
  c.doi_reference,
  c.url,
  c.description,
  c.note,
  c.created_date,
  c.created_by,
  c.modified_date,
  c.modified_by,
  c.owned_date,
  c.owned_by
FROM
    citation c
    LEFT JOIN literature l ON c.literature_id = l.literature_id
    INNER JOIN (
        SELECT citation_id FROM citation WHERE citation_id IN (:citationid)
        UNION SELECT citation_id FROM citation WHERE accession_ipr_id IN (:accessioniprid)
        UNION SELECT citation_id FROM citation WHERE literature_id IN (:literatureid) AND accession_ipr_id IS NOT NULL
        UNION SELECT citation_id FROM citation 
            INNER JOIN accession_ipr ON citation.accession_ipr_id = accession_ipr.accession_ipr_id
            WHERE accession_ipr.accession_id IN (:accessionid)
        UNION SELECT citation_id FROM citation 
            INNER JOIN accession_ipr ON citation.accession_ipr_id = accession_ipr.accession_ipr_id
            WHERE accession_ipr.cooperator_id IN (:cooperatorid)
        UNION SELECT citation_id FROM citation 
            INNER JOIN accession_ipr ON citation.accession_ipr_id = accession_ipr.accession_ipr_id
            INNER JOIN accession ON accession_ipr.accession_id = accession.accession_id
            WHERE accession.taxonomy_species_id IN (:taxonomyspeciesid)
     ) list on list.citation_id = c.citation_id AND c.accession_ipr_id IS NOT NULL