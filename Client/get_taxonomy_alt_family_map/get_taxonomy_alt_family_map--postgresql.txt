SELECT
  tafm.taxonomy_alt_family_map_id,
  tafm.taxonomy_genus_id,
  tafm.taxonomy_family_id,
  tafm.created_date,
  tafm.created_by,
  tafm.modified_date,
  tafm.modified_by,
  tafm.owned_date,
  tafm.owned_by
FROM
    taxonomy_alt_family_map tafm
WHERE
    tafm.taxonomy_alt_family_map_id in (:taxonomyaltfamilymapid)
UNION
SELECT
  tafm.taxonomy_alt_family_map_id,
  tafm.taxonomy_genus_id,
  tafm.taxonomy_family_id,
  tafm.created_date,
  tafm.created_by,
  tafm.modified_date,
  tafm.modified_by,
  tafm.owned_date,
  tafm.owned_by
FROM
    taxonomy_alt_family_map AS tafm, taxonomy_species ts, accession a
WHERE
    tafm.taxonomy_genus_id = ts.taxonomy_genus_id and
    ts.taxonomy_species_id = a.taxonomy_species_id and
    a.accession_id in (:accessionid)
UNION
SELECT
  tafm.taxonomy_alt_family_map_id,
  tafm.taxonomy_genus_id,
  tafm.taxonomy_family_id,
  tafm.created_date,
  tafm.created_by,
  tafm.modified_date,
  tafm.modified_by,
  tafm.owned_date,
  tafm.owned_by
FROM
    taxonomy_alt_family_map AS tafm, taxonomy_species ts, accession a, inventory i
WHERE
    tafm.taxonomy_genus_id = ts.taxonomy_genus_id and
    ts.taxonomy_species_id = a.taxonomy_species_id and
    a.accession_id = i.accession_id and
    i.inventory_id in (:inventoryid)
UNION
SELECT
  tafm.taxonomy_alt_family_map_id,
  tafm.taxonomy_genus_id,
  tafm.taxonomy_family_id,
  tafm.created_date,
  tafm.created_by,
  tafm.modified_date,
  tafm.modified_by,
  tafm.owned_date,
  tafm.owned_by
FROM
    taxonomy_alt_family_map AS tafm
WHERE
    tafm.taxonomy_genus_id in (:taxonomygenusid)


