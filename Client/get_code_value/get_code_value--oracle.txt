SELECT
  code_value_id,
  cv.group_name,
  value,
  created_date,
  created_by,
  modified_date,
  modified_by,
  owned_date,
  owned_by
FROM
    code_value cv
WHERE
    cv.code_value_id IN (:codevalueid)
    OR ':codevalueid' = '-1'
