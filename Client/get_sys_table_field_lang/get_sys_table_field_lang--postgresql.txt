SELECT
  stfl.sys_table_field_lang_id,
  stf.sys_table_id,
  stfl.sys_table_field_id,
  stfl.sys_lang_id,
  stfl.title,
  stfl.description,
  stfl.created_date,
  stfl.created_by,
  stfl.modified_date,
  stfl.modified_by,
  stfl.owned_date,
  stfl.owned_by
FROM
    sys_table_field_lang stfl
    LEFT JOIN sys_table_field stf
      ON  stfl.sys_table_field_id = stf.sys_table_field_id 
    INNER JOIN (
        SELECT sys_table_field_lang_id FROM sys_table_field_lang
            WHERE sys_table_field_lang_id IN (:systablefieldlangid)

        UNION SELECT sys_table_field_lang_id FROM sys_table_field_lang
            WHERE sys_table_field_id IN (:systablefieldid)

        UNION SELECT sys_table_field_lang_id FROM sys_table_field_lang
            WHERE sys_lang_id IN (:syslangid)

        UNION SELECT sys_table_field_lang_id FROM sys_table_field_lang
            INNER JOIN sys_table_field ON sys_table_field_lang.sys_table_field_id = sys_table_field.sys_table_field_id
            WHERE sys_table_id IN (:systableid)

    ) list ON list.sys_table_field_lang_id = stfl.sys_table_field_lang_id
