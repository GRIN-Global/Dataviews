SELECT
  c.citation_id,
  gm.crop_id,
  c.genetic_marker_id,
  c.type_code,
  c.author_name,
  c.citation_year,
  c.title,
  c.citation_title,
  l.abbreviation,
  c.reference,
  c.literature_id,
  c.doi_reference,
  c.url,
  c.description,
  c.note,
  c.created_date,
  c.created_by,
  c.modified_date,
  c.modified_by,
  c.owned_date,
  c.owned_by
FROM
    citation c
    LEFT JOIN literature l ON c.literature_id = l.literature_id
    LEFT JOIN genetic_marker gm ON c.genetic_marker_id = gm.genetic_marker_id
    INNER JOIN (
        SELECT citation_id FROM citation WHERE citation_id IN (:citationid)
        UNION SELECT citation_id FROM citation WHERE genetic_marker_id IN (:geneticmarkerid)
        UNION SELECT citation_id FROM citation WHERE literature_id IN (:literatureid) AND genetic_marker_id IS NOT NULL
        UNION SELECT citation_id FROM citation
            INNER JOIN genetic_marker ON citation.genetic_marker_id = genetic_marker.genetic_marker_id
            WHERE genetic_marker.crop_id IN (:cropid)
     ) list on list.citation_id = c.citation_id AND c.genetic_marker_id IS NOT NULL
