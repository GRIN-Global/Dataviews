SELECT
  grm.geography_region_map_id,
  grm.geography_id,
  grm.region_id,
  grm.created_date,
  grm.created_by,
  grm.modified_date,
  grm.modified_by,
  grm.owned_date,
  grm.owned_by
FROM
    geography_region_map AS grm
WHERE  grm.geography_region_map_id in (:geographyregionmapid)

UNION

SELECT
  grm.geography_region_map_id,
  grm.geography_id,
  grm.region_id,
  grm.created_date,
  grm.created_by,
  grm.modified_date,
  grm.modified_by,
  grm.owned_date,
  grm.owned_by
FROM
    geography_region_map AS grm
WHERE  grm.geography_id in (:geographyid)

UNION

SELECT
  grm.geography_region_map_id,
  grm.geography_id,
  grm.region_id,
  grm.created_date,
  grm.created_by,
  grm.modified_date,
  grm.modified_by,
  grm.owned_date,
  grm.owned_by
FROM
    geography_region_map AS grm
WHERE  grm.region_id in (:regionid)
