SELECT
  ta.taxonomy_attach_id,
  ta.taxonomy_family_id,
  ta.taxonomy_genus_id,
  ta.taxonomy_species_id,
  ta.virtual_path,
  ta.thumbnail_virtual_path,
  ta.sort_order,
  ta.title,
  ta.description,
  ta.content_type,
  ta.category_code,
  ta.is_web_visible,
  ta.copyright_information,
  ta.attach_cooperator_id,
  ta.note,
  ta.created_date,
  ta.created_by,
  ta.modified_date,
  ta.modified_by,
  ta.owned_date,
  ta.owned_by
FROM
    taxonomy_attach AS ta
WHERE
    ta.taxonomy_attach_id in (:taxonomyattachid)
UNION
SELECT
  ta.taxonomy_attach_id,
  ta.taxonomy_family_id,
  ta.taxonomy_genus_id,
  ta.taxonomy_species_id,
  ta.virtual_path,
  ta.thumbnail_virtual_path,
  ta.sort_order,
  ta.title,
  ta.description,
  ta.content_type,
  ta.category_code,
  ta.is_web_visible,
  ta.copyright_information,
  ta.attach_cooperator_id,
  ta.note,
  ta.created_date,
  ta.created_by,
  ta.modified_date,
  ta.modified_by,
  ta.owned_date,
  ta.owned_by
FROM
    taxonomy_attach AS ta,
    accession a
WHERE
    ta.taxonomy_species_id = a.taxonomy_species_id and
    a.accession_id in (:accessionid)
UNION
SELECT
  ta.taxonomy_attach_id,
  ta.taxonomy_family_id,
  ta.taxonomy_genus_id,
  ta.taxonomy_species_id,
  ta.virtual_path,
  ta.thumbnail_virtual_path,
  ta.sort_order,
  ta.title,
  ta.description,
  ta.content_type,
  ta.category_code,
  ta.is_web_visible,
  ta.copyright_information,
  ta.attach_cooperator_id,
  ta.note,
  ta.created_date,
  ta.created_by,
  ta.modified_date,
  ta.modified_by,
  ta.owned_date,
  ta.owned_by
FROM
    taxonomy_attach AS ta,
    accession a,
    inventory i
WHERE
    ta.taxonomy_species_id = a.taxonomy_species_id and
    a.accession_id = i.accession_id and
    i.inventory_id in (:inventoryid)
UNION
SELECT
  ta.taxonomy_attach_id,
  ta.taxonomy_family_id,
  ta.taxonomy_genus_id,
  ta.taxonomy_species_id,
  ta.virtual_path,
  ta.thumbnail_virtual_path,
  ta.sort_order,
  ta.title,
  ta.description,
  ta.content_type,
  ta.category_code,
  ta.is_web_visible,
  ta.copyright_information,
  ta.attach_cooperator_id,
  ta.note,
  ta.created_date,
  ta.created_by,
  ta.modified_date,
  ta.modified_by,
  ta.owned_date,
  ta.owned_by
FROM
    taxonomy_attach AS ta
WHERE
    ta.taxonomy_genus_id in (:taxonomygenusid)
UNION
SELECT
  ta.taxonomy_attach_id,
  ta.taxonomy_family_id,
  ta.taxonomy_genus_id,
  ta.taxonomy_species_id,
  ta.virtual_path,
  ta.thumbnail_virtual_path,
  ta.sort_order,
  ta.title,
  ta.description,
  ta.content_type,
  ta.category_code,
  ta.is_web_visible,
  ta.copyright_information,
  ta.attach_cooperator_id,
  ta.note,
  ta.created_date,
  ta.created_by,
  ta.modified_date,
  ta.modified_by,
  ta.owned_date,
  ta.owned_by
FROM
    taxonomy_attach AS ta
WHERE
    ta.attach_cooperator_id in (:cooperatorid)



