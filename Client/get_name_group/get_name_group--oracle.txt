select
      ng.name_group_id
      ,ng.group_name
      ,ng.note
      ,ng.url
      ,ng.created_date
      ,ng.created_by
      ,ng.modified_date
      ,ng.modified_by
      ,ng.owned_date
      ,ng.owned_by
FROM name_group ng
WHERE ng.name_group_id in (:namegroupid)

UNION
select
      ng.name_group_id
      ,ng.group_name
      ,ng.note
      ,ng.url
      ,ng.created_date
      ,ng.created_by
      ,ng.modified_date
      ,ng.modified_by
      ,ng.owned_date
      ,ng.owned_by
  FROM name_group ng, accession_inv_name an, inventory i
where ng.name_group_id = an.name_group_id and
      an.inventory_id = i.inventory_id and
      i.accession_id in (:accessionid)

UNION

select
      ng.name_group_id
      ,ng.group_name
      ,ng.note
      ,ng.url
      ,ng.created_date
      ,ng.created_by
      ,ng.modified_date
      ,ng.modified_by
      ,ng.owned_date
      ,ng.owned_by
  FROM name_group ng, accession_inv_name an, inventory i
where ng.name_group_id = an.name_group_id and
      an.inventory_id in (:inventoryid)

