SELECT
  e.exploration_id,
  e.exploration_number,
  e.title,
  e.began_date,
  e.finished_date,
  e.funding_source,
  e.funding_amount,
  e.target_species,
  e.permits,
  e.restrictions,
  e.fiscal_year,
  e.host_cooperator_id,
  e.note,
  e.created_date,
  e.created_by,
  e.modified_date,
  e.modified_by,
  e.owned_date,
  e.owned_by
FROM
    exploration e
WHERE
    e.exploration_id in (:explorationid)


