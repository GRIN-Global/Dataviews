SELECT
  wuc.web_user_cart_id,
  wuc.web_user_id,
  wuc.cart_type_code,
  wuc.expiration_date,
  wu.user_name,
  wc.first_name,
  wc.last_name,
  wuc.created_date,
  wuc.created_by,
  wuc.modified_date,
  wuc.modified_by,
  wuc.owned_date,
  wuc.owned_by
FROM
    web_user_cart wuc
    INNER JOIN web_user wu ON wuc.web_user_id = wu.web_user_id
    LEFT JOIN web_cooperator wc ON wu.web_cooperator_id = wc.web_cooperator_id
    INNER JOIN (
	SELECT web_user_cart_id FROM web_user_cart
            WHERE web_user_cart_id in (:webusercartid)

	UNION SELECT web_user_cart_id FROM web_user_cart_item
            WHERE web_user_cart_item_id in (:webusercartitemid)

	UNION SELECT web_user_cart_id FROM web_user_cart_item
            WHERE accession_id in (:accessionid)

    ) list ON wuc.web_user_cart_id = list.web_user_cart_id
