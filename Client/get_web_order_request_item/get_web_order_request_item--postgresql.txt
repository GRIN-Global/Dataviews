SELECT
  wori.web_order_request_item_id,
  web_cooperator_id,
  web_order_request_id,
  sequence_number,
  accession_id,
  name,
  quantity_shipped,
  unit_of_shipped_code,
  distribution_form_code,
  status_code,
  curator_note,
  user_note,
  (SELECT taxonomy_species_id FROM accession WHERE accession_id = wori.accession_id) AS taxonomy_species_id,
  created_date,
  created_by,
  modified_date,
  modified_by,
  owned_date,
  owned_by
FROM
    web_order_request_item wori
    INNER JOIN (
        SELECT web_order_request_item_id FROM web_order_request_item
            WHERE web_order_request_item_id IN (:weborderrequestitemid)

        UNION SELECT web_order_request_item_id FROM web_order_request_item
            WHERE web_order_request_id IN (:weborderrequestid)

        UNION SELECT web_order_request_item_id FROM web_order_request_item
            WHERE web_cooperator_id IN (:webcooperatorid)

        UNION SELECT web_order_request_item_id FROM web_order_request_item
            WHERE accession_id IN (:accessionid)

        UNION SELECT web_order_request_item_id FROM order_request_item
            WHERE order_request_id IN (:orderrequestid)

        UNION SELECT wori.web_order_request_item_id FROM web_order_request_item wori
            INNER JOIN cooperator c ON c.web_cooperator_id = wori.web_cooperator_id
            WHERE c.cooperator_id IN (:cooperatorid)

        UNION SELECT wori.web_order_request_item_id FROM web_order_request_item wori
            INNER JOIN inventory i ON i.accession_id = wori.accession_id
            WHERE i.inventory_id IN (:inventoryid)

    ) list ON list.web_order_request_item_id = wori.web_order_request_item_id
