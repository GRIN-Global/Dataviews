SELECT
  sdfl.sys_dataview_field_lang_id,
  sdfl.sys_dataview_field_id,
  sdfl.sys_lang_id,
  sdfl.title,
  sdfl.description,
  sdfl.created_date,
  sdfl.created_by,
  sdfl.modified_date,
  sdfl.modified_by,
  sdfl.owned_date,
  sdfl.owned_by
FROM
  sys_dataview_field_lang AS sdfl


