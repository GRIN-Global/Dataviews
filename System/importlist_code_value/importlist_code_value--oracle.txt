SELECT
  cv.value as value_member,
  cv.value as display_member2,
  nvl(cvl.title, cv.value) as display_member
FROM
  code_value cv 
  LEFT JOIN code_value_lang cvl
    ON cvl.code_value_id = cv.code_value_id
	and cvl.sys_lang_id = :langid
where
	cv.group_name = :groupname
order by
	nvl(cvl.title, cv.value)

