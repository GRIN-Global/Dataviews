SELECT
dv.dataview_name,
sdl.title,
sdl.description, 
stf.field_name as pk_field_name,
st.table_name as table_name
FROM
sys_dataview dv left join sys_dataview_lang sdl
on dv.sys_dataview_id = sdl.sys_dataview_id
and sdl.sys_lang_id = __LANGUAGEID__
inner join sys_dataview_field sdvf
	on sdvf.sys_dataview_id = dv.sys_dataview_id
	and sdvf.is_primary_key = 'Y'
inner join sys_table_field stf
	on sdvf.sys_table_field_id = stf.sys_table_field_id
inner join sys_table st
	on stf.sys_table_id = st.sys_table_id
WHERE
	dv.category_code = 'Lookups' 
order by
dv.dataview_name