SELECT sys_table_id
      ,table_name
      ,is_enabled
      ,is_readonly
      ,audits_created 
      ,audits_modified
      ,audits_owned
      ,database_area_code
      ,created_date
      ,created_by
      ,modified_date
      ,modified_by
      ,owned_date
      ,owned_by
  FROM sys_table