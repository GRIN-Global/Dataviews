SELECT
  distinct(sp.sys_permission_id),
  sp.sys_dataview_id,
  sp.sys_table_id,
  sp.permission_tag,
  sp.is_enabled,
  sp.create_permission,
  sp.read_permission,
  sp.update_permission,
  sp.delete_permission,
  spl.sys_lang_id,
  spl.title,
  spl.description
FROM
    sys_permission sp
    LEFT JOIN sys_permission_lang spl
      ON  spl.sys_permission_id = sp.sys_permission_id 
          AND  spl.sys_lang_id = __LANGUAGEID__ 
    LEFT JOIN sys_group_permission_map sgpm
      ON  sgpm.sys_permission_id = sp.sys_permission_id 
    LEFT JOIN sys_group sg
      ON  sg.sys_group_id = sgpm.sys_group_id 

WHERE sg.owned_by = :cooperatorid
