SELECT
  sgpm.sys_group_permission_map_id,
  sgpm.sys_group_id,
  sgpm.sys_permission_id
FROM sys_group_permission_map AS sgpm
  LEFT JOIN sys_group AS sg ON  sgpm.sys_group_id = sg.sys_group_id 
WHERE sg.owned_by = :cooperatorid
