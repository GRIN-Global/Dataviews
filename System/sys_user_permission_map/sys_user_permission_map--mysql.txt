SELECT sys_user_permission_map_id
      ,sys_user_id
      ,sys_permission_id
      ,is_enabled
      ,created_date
      ,created_by
      ,modified_date
      ,modified_by
      ,owned_date
      ,owned_by
  FROM gringlobal.sys_user_permission_map