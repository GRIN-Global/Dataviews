SELECT
  sg.sys_group_id,
  sg.group_tag,
  sg.is_enabled,
  sgl.sys_lang_id,
  sgl.title,
  sgl.description
FROM sys_group sg
  LEFT JOIN sys_group_lang sgl ON  sgl.sys_group_id = sg.sys_group_id AND sgl.sys_lang_id = __LANGUAGEID__ 
WHERE sg.owned_by = :cooperatorid
