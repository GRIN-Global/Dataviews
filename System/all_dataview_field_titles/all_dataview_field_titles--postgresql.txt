select
	sd.dataview_name,
	sdf.field_name,
	sl.title as language_name,
	sl.title as transform_caption,
	sdfl.title,
	sdfl.description
from
	sys_dataview sd left join sys_dataview_field sdf
	on sd.sys_dataview_id = sdf.sys_dataview_id
	left join sys_dataview_field_lang sdfl
	on sdf.sys_dataview_field_id = sdfl.sys_dataview_field_id
	left join sys_lang sl
		on sdfl.sys_lang_id = sl.sys_lang_id
order by
	sd.dataview_name,
	sdf.sort_order