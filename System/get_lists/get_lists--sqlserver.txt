SELECT
  auil.app_user_item_list_id,
  auil.cooperator_id,
  auil.tab_name,
  auil.list_name,
  auil.id_number,
  auil.id_type,
  auil.sort_order,
  auil.title,
  auil.description,
  auil.properties AS properties,
  auil.created_date,
  auil.created_by,
  auil.modified_date,
  auil.modified_by,
  auil.owned_date,
  auil.owned_by
FROM
    app_user_item_list AS auil

WHERE 
    cooperator_id = :cooperatorid
