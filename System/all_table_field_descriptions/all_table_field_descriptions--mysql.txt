select
	st.table_name,
	stf.field_name,
	sl.title as language_name,
	sl.title as transform_caption,
	stfl.title,
	stfl.description
from
	sys_table st left join sys_table_field stf
	on st.sys_table_id = stf.sys_table_id
	left join sys_table_field_lang stfl
	on stf.sys_table_field_id = stfl.sys_table_field_id
	left join sys_lang sl
		on stfl.sys_lang_id = sl.sys_lang_id
order by
	st.table_name,
	stf.field_ordinal