SELECT
  sl.sys_lang_id,
  sl.iso_639_3_tag,
  sl.ietf_tag,
  sl.script_direction,
  sl.title,
  sl.description,
  sl.created_date,
  sl.created_by,
  sl.modified_date,
  sl.modified_by,
  sl.owned_date,
  sl.owned_by
FROM
  sys_lang AS sl


