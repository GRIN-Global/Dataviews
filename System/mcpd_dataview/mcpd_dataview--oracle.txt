select
	s_a.fao_institute_number as INSTCODE,
	coalesce(a.accession_number_part1, '') + coalesce(CONVERT(nvarchar, a.accession_number_part2), '') + coalesce(a.accession_number_part3, '') as ACCENUMB,
	'???' as COLLNUMB,
	tg.genus_name as GENUS,
	t.species_name as SPECIES,
	t.species_authority as SPAUTHOR,
	t.subspecies_name as SUBTAXA,
	t.subspecies_authority as SUBTAUTHOR,
	cr.name as CROPNAME,
	(select top 1 an.plant_name from accession_name an where an.accession_id = a.accession_id order by an.plant_name_rank) as ACCENAME,
	convert(nvarchar, a.initial_received_date, 112) as ACQDATE,
	g.country_code as ORIGCTY,
	as_col.collector_verbatim_locality as COLLSITE,
	
	right('00' + convert(nvarchar, abs(cast(as_col.latitude as integer))), 2)
	+ RIGHT('00' + convert(nvarchar, abs(CAST((as_col.latitude - cast(as_col.latitude as integer)) * 60 as integer))), 2)
	+ RIGHT('00' + convert(nvarchar, abs(((as_col.latitude - CAST(as_col.latitude as integer)) * 60 - CAST((as_col.latitude - cast(as_col.latitude as integer)) * 60        as integer)) * 60)), 2)
	+ case when as_col.latitude > 0 then 'N' else 'S' end
	 as LATITUDE,
	
	+ RIGHT('00' + convert(nvarchar, abs(CAST(as_col.longitude as integer))), 2)
	+ RIGHT('00' + convert(nvarchar, abs(CAST((as_col.longitude - cast(as_col.longitude as integer)) * 60 as integer))), 2)
	+ RIGHT('00' + convert(nvarchar, abs(((as_col.longitude - CAST(as_col.longitude as integer)) * 60 - CAST((as_col.longitude - cast(as_col.longitude as integer)) * 60        as integer)) * 60)), 2)
	+ case when as_col.longitude > 0 then 'E' else 'W' end
	 as LONGITUDE,
	as_col.elevation_meters as ELEVATION,
	convert(nvarchar, as_col.source_date, 112) as COLLDATE,
	(select top 1 s1.fao_institute_number from site s1 where c_a.site_id = s1.site_id) as BREDCODE,
	'???' as SAMPSTAT,
	ap.description as ANCEST,
	as_col.acquisition_source_code as COLLSRC,
	(select top 1 s1.fao_institute_number from site s1 where c_a.site_id = s1.site_id) as DONORCODE,
	coalesce(a.accession_number_part1, '') + coalesce(CONVERT(nvarchar, a.accession_number_part2), '') + coalesce(a.accession_number_part3, '') as DONORNUMB,
	'???' as OTHERNUMB,
	(select top 1 s2.fao_institute_number from site s2 where a.backup_location1_site_id = s2.site_id) as DUPLSITE,
	(select top 1 i1.storage_location_part1 from inventory i1 where i1.accession_id = a.accession_id and i1.form_type_code <> '**') as STORAGE,
	'???' as REMARKS
from
	accession a 
	left join cooperator c_a on a.owned_by = c_a.cooperator_id 
	LEFT join site s_a on c_a.site_id = s_a.site_id
	left join taxonomy_species t on a.taxonomy_species_id = t.taxonomy_species_id
	left join taxonomy_genus tg on t.taxonomy_genus_id = tg.taxonomy_genus_id
	left join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
	left join taxonomy_crop_map tcm on t.taxonomy_species_id = tcm.taxonomy_species_id
	left join crop cr on tcm.crop_id = cr.crop_id
	left join accession_source as_col on a.accession_id = as_col.accession_id and as_col.source_type_code = 'COLLECTED'
	left join geography g on as_col.geography_id = g.geography_id
	left join accession_pedigree ap on a.accession_id = ap.accession_id
	left join accession_source as_don on a.accession_id = as_don.accession_id and as_don.source_type_code = 'DONATED'
	left join accession_source as_dev on a.accession_id = as_dev.accession_id and as_dev.source_type_code = 'DEVELOPED'
order by 2