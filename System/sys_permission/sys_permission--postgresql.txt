SELECT sp.sys_permission_id
      ,sp.sys_dataview_id
      ,sp.sys_table_id
      ,spl.title
      ,spl.description
      ,sp.is_enabled
      ,sp.create_permission
      ,sp.read_permission
      ,sp.update_permission
      ,sp.delete_permission
      ,sp.created_date
      ,sp.created_by
      ,sp.modified_date
      ,sp.modified_by
      ,sp.owned_date
      ,sp.owned_by
  FROM sys_permission sp left join sys_permission_lang spl
on sp.sys_permission_id = spl.sys_permission_id 
and spl.sys_lang_id = __LANGUAGEID__