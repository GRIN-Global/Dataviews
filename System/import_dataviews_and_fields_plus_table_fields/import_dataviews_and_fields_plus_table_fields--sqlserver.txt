SELECT
  sd.dataview_name, 
  sdl.title AS dataview_title, 
  sdl.description AS dataview_description,
  sdf.field_name AS dataview_field_name, 
  sdfl.title AS dataview_field_title,
  sdfl.description AS dataview_field_description,
  st.table_name,
  stf.field_name AS table_field_name,
  stfl.title AS table_field_title,
  stfl.description AS table_field_description
FROM
    sys_dataview AS sd
    INNER JOIN sys_dataview_field AS sdf
      ON  sd.sys_dataview_id = sdf.sys_dataview_id 
    LEFT JOIN sys_dataview_lang AS sdl
      ON  sdl.sys_dataview_id = sd.sys_dataview_id 
        and sdl.sys_lang_id = :langid 
    LEFT JOIN sys_dataview_field_lang AS sdfl
      ON  sdfl.sys_dataview_field_id = sdf.sys_dataview_field_id 
        and sdfl.sys_lang_id = :langid 
    LEFT JOIN sys_table_field AS stf
      ON  sdf.sys_table_field_id = stf.sys_table_field_id 
    LEFT JOIN sys_table AS st
      ON  st.sys_table_id = stf.sys_table_id 
    LEFT JOIN sys_table_field_lang AS stfl
      ON  sdf.sys_table_field_id = stfl.sys_table_field_id 
        and stfl.sys_lang_id = :langid 

WHERE
1 = :viewallrows
