SELECT 
       sd.sys_dataview_id
      ,sd.dataview_name
      ,coalesce(sdl.title,sd.dataview_name) as title
      ,sdl.description
      ,sd.is_enabled 
      ,sd.is_readonly
      ,sd.category_code
      ,sd.database_area_code
      ,sd.database_area_code_sort_order
      ,sd.is_transform
      ,sd.transform_field_for_names
      ,sd.transform_field_for_captions
      ,sd.transform_field_for_values
      ,sd.created_date
      ,sd.created_by
      ,sd.modified_date
      ,sd.modified_by
      ,sd.owned_date
      ,sd.owned_by
FROM gringlobal.dbo.sys_dataview sd 
    LEFT JOIN gringlobal.dbo.sys_dataview_lang sdl 
        ON sd.sys_dataview_id = sdl.sys_dataview_id
        AND sdl.sys_lang_id = __LANGUAGEID__

