select
  sdp.param_name,
  sdp.param_type,
  sdp.sort_order
from
  sys_dataview_param sdp inner join sys_dataview sd
    on sdp.sys_dataview_id = sd.sys_dataview_id
where
  sd.dataview_name = :dataview
order by
  sdp.sort_order
