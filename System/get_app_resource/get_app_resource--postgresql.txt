SELECT
  ar.app_resource_id,
  ar.sys_lang_id,
  ar.app_name,
  ar.form_name,
  ar.app_resource_name,
  ar.description,
  ar.display_member,
  ar.value_member,
  ar.sort_order,
  ar.properties,
  ar.created_date,
  ar.created_by,
  ar.modified_date,
  ar.modified_by,
  ar.owned_date,
  ar.owned_by
FROM
    app_resource AS ar
where 
    coalesce(app_name,'') = coalesce(:appname, app_name, '')
    and sys_lang_id = coalesce(:syslangid, sys_lang_id)
