SELECT sd.sys_dataview_id,
       sd.dataview_name                            AS dataview_name,
       COALESCE(sdl.title, sd.dataview_name)       AS title,
       COALESCE(sdl.description, sd.dataview_name) AS description,
       sd.is_enabled                               AS is_enabled,
       sd.is_readonly                              AS is_readonly,
       COALESCE(ccl.title, sd.category_code)       AS category_code,
       COALESCE(dacl.title, sd.database_area_code) AS database_area_code,
       sd.database_area_code_sort_order,
       stf.field_name                              AS primary_key
FROM   sys_dataview sd
       LEFT JOIN sys_dataview_lang sdl
              ON sdl.sys_dataview_id = sd.sys_dataview_id AND sdl.sys_lang_id = __LANGUAGEID__
       LEFT JOIN sys_dataview_field sdf
              ON sdf.sys_dataview_id = sd.sys_dataview_id AND sdf.is_primary_key = 'Y'
       LEFT JOIN sys_table_field stf
              ON stf.sys_table_field_id = sdf.sys_table_field_id
       LEFT JOIN code_value cc
              ON sd.category_code = cc.value AND cc.group_name = 'DATAVIEW_CATEGORY'
       LEFT JOIN code_value_lang ccl
              ON cc.code_value_id = ccl.code_value_id AND ccl.sys_lang_id = __LANGUAGEID__
       LEFT JOIN code_value dac
              ON sd.database_area_code = dac.value AND dac.group_name = 'DATAVIEW_DATABASE_AREA'
       LEFT JOIN code_value_lang dacl
              ON dac.code_value_id = dacl.code_value_id AND dacl.sys_lang_id = __LANGUAGEID__
WHERE  sd.category_code = 'Client' OR sd.category_code = 'Reports' OR sd.category_code LIKE 'Site%'