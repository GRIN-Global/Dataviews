SELECT DISTINCT(order_request_item.order_request_id)
FROM order_request_item
    INNER JOIN inventory ON order_request_item.inventory_id = inventory.inventory_id
    INNER JOIN citation ON inventory.accession_id = citation.accession_id
WHERE __searchcriteria__