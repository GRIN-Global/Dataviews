SELECT DISTINCT(accession_source_map.cooperator_id) FROM accession_source_map
INNER JOIN accession_source ON accession_source_map.accession_source_id = accession_source.accession_source_id
INNER JOIN inventory ON accession_source.accession_id = inventory.accession_id
INNER JOIN crop_trait_observation ON inventory.inventory_id = crop_trait_observation.inventory_id
INNER JOIN crop_trait ON crop_trait_observation.crop_trait_id = crop_trait.crop_trait_id
INNER JOIN crop_attach ON crop_trait.crop_id = crop_attach.crop_id
WHERE __searchcriteria__