SELECT DISTINCT 
    CASE WHEN i.form_type_code != '**' AND i.inventory_id != i2.inventory_id
        THEN NULL
        ELSE ori.order_request_id
    END AS order_request_id
FROM accession_inv_group
    INNER JOIN accession_inv_group_map aigm ON accession_inv_group.accession_inv_group_id = aigm.accession_inv_group_map_id
    INNER JOIN inventory i ON aigm.inventory_id = i.inventory_id
    INNER JOIN inventory i2 ON i.accession_id = i2.accession_id
    INNER JOIN order_request_item ori ON i2.inventory_id = ori.inventory_id
WHERE __searchcriteria__