SELECT DISTINCT(accession_source_map.cooperator_id) FROM accession_source_map
LEFT JOIN accession_source ON accession_source_map.accession_source_id = accession_source.accession_source_id
LEFT JOIN inventory ON accession_source.accession_id = inventory.accession_id
LEFT JOIN crop_trait_observation ON inventory.inventory_id = crop_trait_observation.inventory_id
LEFT JOIN method ON crop_trait_observation.method_id = method.method_id
WHERE __searchcriteria__