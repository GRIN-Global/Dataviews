SELECT DISTINCT(order_request_item.order_request_id) FROM order_request_item
INNER JOIN crop_trait_observation ON order_request_item.inventory_id = crop_trait_observation.inventory_id
INNER JOIN crop_trait ON crop_trait_observation.crop_trait_id = crop_trait.crop_trait_id
INNER JOIN crop ON crop_trait.crop_id = crop.crop_id
WHERE __searchcriteria__