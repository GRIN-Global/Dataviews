SELECT DISTINCT(inventory.inventory_id) FROM inventory
INNER JOIN web_order_request_item
        ON web_order_request_item.accession_id = inventory.accession_id
INNER JOIN web_order_request
        ON web_order_request.web_order_request_id = web_order_request_item.web_order_request_id 
WHERE __searchcriteria__