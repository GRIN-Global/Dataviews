SELECT DISTINCT 
    CASE i.form_type_code
        WHEN '**' THEN i2.inventory_id
        ELSE i.inventory_id
    END AS inventory_id
FROM accession_inv_group
    LEFT JOIN accession_inv_group_map ON accession_inv_group_map.accession_inv_group_id = accession_inv_group.accession_inv_group_id
    LEFT JOIN inventory i ON accession_inv_group_map.inventory_id = i.inventory_id
    LEFT JOIN inventory i2 ON i.accession_id = i2.accession_id
WHERE __searchcriteria__