SELECT DISTINCT(inventory.inventory_id)
FROM inventory
    INNER JOIN citation ON inventory.accession_id = citation.accession_id
    INNER JOIN literature ON citation.literature_id = literature.literature_id
WHERE __searchcriteria__