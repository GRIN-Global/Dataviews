SELECT DISTINCT 
    CASE i.form_type_code
        WHEN '**' THEN i2.inventory_id
        ELSE i.inventory_id
    END AS inventory_id
FROM accession_inv_voucher 
    LEFT JOIN inventory i ON accession_inv_voucher.inventory_id = i.inventory_id
    LEFT JOIN inventory i2 ON i.accession_id = i2.accession_id
WHERE __searchcriteria__