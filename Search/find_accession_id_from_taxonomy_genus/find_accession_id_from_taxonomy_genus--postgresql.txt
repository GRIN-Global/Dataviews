SELECT DISTINCT(accession.accession_id) FROM accession
LEFT JOIN taxonomy_species ON accession.taxonomy_species_id = taxonomy_species.taxonomy_species_id
LEFT JOIN taxonomy_genus ON taxonomy_species.taxonomy_genus_id = taxonomy_genus.taxonomy_genus_id
WHERE __searchcriteria__