SELECT DISTINCT(order_request_item.order_request_id) FROM order_request_item
LEFT JOIN inventory ON order_request_item.inventory_id = inventory.inventory_id
LEFT JOIN accession ON inventory.accession_id = accession.accession_id
LEFT JOIN taxonomy_species ON accession.taxonomy_species_id = taxonomy_species.taxonomy_species_id
WHERE __searchcriteria__