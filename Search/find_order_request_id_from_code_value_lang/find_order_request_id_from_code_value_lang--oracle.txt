SELECT DISTINCT(order_request_item.order_request_id) FROM order_request_item
LEFT JOIN inventory ON order_request_item.inventory_id = inventory.inventory_id
LEFT JOIN accession_source ON inventory.accession_id = accession_source.accession_id
LEFT JOIN geography ON accession_source.geography_id = geography.geography_id
LEFT JOIN code_value ON geography.country_code = code_value.value
LEFT JOIN code_value_lang ON code_value.code_value_id = code_value_lang.code_value_id 
WHERE __searchcriteria__
