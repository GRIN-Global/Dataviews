SELECT DISTINCT(order_request_item.order_request_id) FROM order_request_item
INNER JOIN crop_trait_observation ON order_request_item.inventory_id = crop_trait_observation.inventory_id
INNER JOIN crop_trait_code_lang ON crop_trait_observation.crop_trait_code_id = crop_trait_code_lang.crop_trait_code_id
WHERE __searchcriteria__