SELECT DISTINCT(inventory.inventory_id) FROM inventory
LEFT JOIN accession ON inventory.accession_id = accession.accession_id
LEFT JOIN taxonomy_species ON accession.taxonomy_species_id = taxonomy_species.taxonomy_species_id
WHERE __searchcriteria__