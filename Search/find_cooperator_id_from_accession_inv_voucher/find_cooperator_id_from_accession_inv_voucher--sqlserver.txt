SELECT DISTINCT(accession_source_map.cooperator_id) 
FROM 
  accession_source_map, accession_source, inventory, accession_inv_voucher
WHERE
  accession_source_map.accession_source_id = accession_source.accession_source_id AND
  accession_inv_voucher.inventory_id = inventory.inventory_id AND
  inventory.accession_id = accession_source.accession_id AND
  __searchcriteria__
