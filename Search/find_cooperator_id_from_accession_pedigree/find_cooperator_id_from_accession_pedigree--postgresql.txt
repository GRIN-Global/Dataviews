SELECT DISTINCT(accession_source_map.cooperator_id) FROM accession_source_map
LEFT JOIN accession_source ON accession_source_map.accession_source_id = accession_source.accession_source_id
LEFT JOIN accession_pedigree ON accession_source.accession_id = accession_pedigree.accession_id
WHERE __searchcriteria__