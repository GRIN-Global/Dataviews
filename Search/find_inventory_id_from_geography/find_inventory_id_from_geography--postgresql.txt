SELECT DISTINCT(inventory.inventory_id) FROM inventory
LEFT JOIN accession_source ON inventory.accession_id = accession_source.accession_id
LEFT JOIN geography ON accession_source.geography_id = geography.geography_id
WHERE __searchcriteria__