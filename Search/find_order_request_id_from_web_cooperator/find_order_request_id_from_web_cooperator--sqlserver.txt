SELECT DISTINCT(order_request.order_request_id) FROM order_request
INNER JOIN web_order_request
        ON web_order_request.web_order_request_id = order_request.web_order_request_id 
INNER JOIN web_cooperator
        ON web_cooperator.web_cooperator_id = web_order_request.web_cooperator_id 
WHERE __searchcriteria__