SELECT DISTINCT(accession_source_map.cooperator_id) FROM accession_source_map
INNER JOIN accession_source
        ON accession_source.accession_source_id = accession_source_map.accession_source_id
INNER JOIN inventory
        ON inventory.accession_id = accession_source.accession_id
INNER JOIN accession_inv_name
        ON accession_inv_name.inventory_id = inventory.inventory_id
WHERE __searchcriteria__