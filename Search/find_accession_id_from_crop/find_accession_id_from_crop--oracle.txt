SELECT DISTINCT(inventory.accession_id) FROM inventory
INNER JOIN crop_trait_observation ON inventory.inventory_id = crop_trait_observation.inventory_id
INNER JOIN crop_trait ON crop_trait_observation.crop_trait_id = crop_trait.crop_trait_id
INNER JOIN crop ON crop_trait.crop_id = crop.crop_id
WHERE __searchcriteria__