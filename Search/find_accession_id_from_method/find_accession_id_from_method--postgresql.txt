SELECT DISTINCT(inventory.accession_id) FROM inventory
LEFT JOIN crop_trait_observation ON inventory.inventory_id = crop_trait_observation.inventory_id
LEFT JOIN method ON crop_trait_observation.method_id = method.method_id
WHERE __searchcriteria__