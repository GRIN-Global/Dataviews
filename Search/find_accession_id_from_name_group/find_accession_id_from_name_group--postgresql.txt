SELECT DISTINCT(inventory.accession_id) 
FROM 
  inventory, accession_inv_name, name_group
WHERE
  inventory.inventory_id = accession_inv_name.inventory_id AND
  accession_inv_name.name_group_id = name_group.name_group_id AND
  __searchcriteria__