SELECT DISTINCT(inventory.inventory_id) FROM inventory
INNER JOIN crop_trait_observation ON inventory.inventory_id = crop_trait_observation.inventory_id
INNER JOIN crop_trait_lang ON crop_trait_observation.crop_trait_id = crop_trait_lang.crop_trait_id
WHERE __searchcriteria__