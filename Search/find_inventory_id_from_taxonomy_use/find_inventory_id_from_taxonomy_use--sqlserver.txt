SELECT DISTINCT(inventory.inventory_id) FROM inventory
LEFT JOIN accession ON inventory.accession_id = accession.accession_id
LEFT JOIN taxonomy_use ON accession.taxonomy_species_id = taxonomy_use.taxonomy_species_id
WHERE __searchcriteria__