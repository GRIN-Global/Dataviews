SELECT
  ROW_NUMBER() OVER (ORDER BY o.order_type_code,oi.status_code,s.site_short_name,c.category_code,geo.country_code,g.genus_name,o.completed_date) AS rownum_id, 
  o.order_type_code,oi.status_code,s.site_short_name,c.category_code,geo.country_code,g.genus_name,
  o.completed_date,count(*) AS count
FROM
    order_request o,order_request_item oi,accession a,inventory inv,taxonomy_genus g,
    taxonomy_species t,cooperator c,site s,geography geo, cooperator c2
WHERE  o.order_request_id = oi.order_request_id  and oi.inventory_id = inv.inventory_id
	and inv.accession_id = a.accession_id and a.taxonomy_species_id = t.taxonomy_species_id 
	and t.taxonomy_genus_id = g.taxonomy_genus_id
	and oi.status_code is not null and o.order_type_code is not null
        and o.final_recipient_cooperator_id = c.cooperator_id
        and c2.cooperator_id = o.owned_by 
	and c2.site_id = s.site_id and c.geography_id = geo.geography_id
	and oi.status_code = 'SHIPPED' and c.category_code is not null
	and g.genus_name is not null and geo.country_code is not null and o.order_type_code is not null
	and o.order_request_id in (:orderrequestid)
GROUP by o.order_type_code,oi.status_code,s.site_short_name,c.category_code,geo.country_code,g.genus_name, 
	o.completed_date 
ORDER by o.order_type_code,oi.status_code,s.site_short_name,c.category_code,geo.country_code,g.genus_name,
	o.completed_date